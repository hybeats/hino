<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hino. История заказов");
?>
<section class="history-page">
  <div class="container-fluid">
    <?$APPLICATION->IncludeComponent("hino:sale.personal.order.list","list",Array(
        "STATUS_COLOR_N"                => "green",
        "STATUS_COLOR_P"                => "yellow",
        "STATUS_COLOR_F"                => "gray",
        "STATUS_COLOR_PSEUDO_CANCELLED" => "red",
        "PATH_TO_DETAIL"                => "order_detail.php?ID=#ID#",
        "PATH_TO_COPY"                  => "basket.php",
        "PATH_TO_CANCEL"                => "order_cancel.php?ID=#ID#",
        "PATH_TO_BASKET"                => "basket.php",
        "PATH_TO_PAYMENT"               => "payment.php",
        "ORDERS_PER_PAGE"               => 15,
        "SET_TITLE"                     => "N",
        "SAVE_IN_SESSION"               => "Y",
        "NAV_TEMPLATE"                  => "",
        "CACHE_TYPE"                    => "A",
        "CACHE_TIME"                    => "3600",
        "CACHE_GROUPS"                  => "Y",
        "HISTORIC_STATUSES"             => "",
        "ACTIVE_DATE_FORMAT"            => "d.m.Y",
        "NAV_TEMPLATE"                  => "hino-pager"
      )
    );?>
  </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
