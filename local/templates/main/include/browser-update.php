<style media="screen">
  .buorg div a, .buorg div a:visited{
    text-indent: 0;
    color: #fff;
    text-decoration: none;
    padding: 8px;
    text-align: center;
    border: none;
    border-radius: 0px;
    font-weight: normal;
    background: #5ab400;
    white-space: nowrap;
    margin: 0 10px;
    display: inline-block;
    max-width: 250px;
  }
  @media only screen and (max-width: 700px){
    .buorg div a, .buorg div a:visited{
      display: block;
      margin: 5px auto;
    }
  }
</style>
<script>
  var $buoop = {
    vs:       {i:10,f:-4,o:-4,s:8,c:-4},
    reminder: 0,
    api:      4,
    noclose:  true
  };
  function $buo_f(){
   var e = document.createElement("script");
   e.src = "//browser-update.org/update.min.js";
   document.body.appendChild(e);
  };
  try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
  catch(e){window.attachEvent("onload", $buo_f)}
</script>
