<!DOCTYPE html>
<html lang="en">
<head>
  <title><?$APPLICATION->ShowTitle();?></title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?$APPLICATION->ShowCSS();?>
  <?$APPLICATION->ShowHeadStrings();?>
  <?$APPLICATION->ShowHeadScripts();?>
  <?$APPLICATION->SetAdditionalCSS("/css/plugins/grid12.css");?>
  <?$APPLICATION->SetAdditionalCSS("/js/plugins/swiper/swiper.min.css");?>
  <?$APPLICATION->SetAdditionalCSS("/js/plugins/jquery-ui/jquery-ui.min.css");?>
  <?$APPLICATION->SetAdditionalCSS("/css/main.css");?>
  <?$APPLICATION->SetAdditionalCSS("/css/media.css");?>
  <?$APPLICATION->SetAdditionalCSS("/css/status.css");?>
  <?$APPLICATION->SetAdditionalCSS("/css/plugins/hino-jquery-ui.css");?>
  <?$APPLICATION->SetAdditionalCSS("/js/vue-components/notifications/vue-toast.min.css");?>

  <?
    $APPLICATION->AddHeadString("<script src=\"/js/plugins/object-assign.js\" charset=\"utf-8\"></script>",true);
    $APPLICATION->AddHeadString("<script src=\"/js/plugins/findIndex.js\" charset=\"utf-8\"></script>",true);
    if($_SERVER["SERVER_NAME"] == "hino"){
      $APPLICATION->AddHeadString("<script src=\"/js/plugins/vue/vue.js\" charset=\"utf-8\"></script>",true);
    }else{
      $APPLICATION->AddHeadString("<script src=\"/js/plugins/vue/vue.min.js\" charset=\"utf-8\"></script>",true);
    }
    $APPLICATION->AddHeadScript("/js/vue-components/modal.js");
    $APPLICATION->AddHeadScript("/js/vue-components/notifications/vue-toast.min.js");
  ?>

  <?$APPLICATION->AddHeadScript('/js/plugins/jquery-3.1.1.min.js');?>
  <?$APPLICATION->AddHeadScript('/js/plugins/swiper/swiper.jquery.min.js');?>
  <?$APPLICATION->AddHeadScript('/js/plugins/swiper/swiper.min.js');?>
  <?$APPLICATION->AddHeadScript('/js/plugins/jquery-ui/jquery-ui.min.js');?>
  <?$APPLICATION->AddHeadScript('/js/plugins/jqueryvalidation/jquery.validate.min.js');?>
  <?$APPLICATION->AddHeadScript('/js/scripts.js');?>
</head>
<body>
  <?$APPLICATION->ShowPanel();?>
  <main class="content">
    <div class="header">
      <div class="container-fluid header__container">
        <div class="row">
          <div class="header__logo-wrap">
            <a href="/"><img class="header__logo" src="/images/logo.jpg" alt="Hino"></a>
          </div>
          <div class="header__text">
            <h2 class="header__title">Хино Моторс Сэйлс</h2>
            <h3 class="header__subtitle">Официальный дистрибьютор Hino Motors Ltd.</h3>
          </div>

          <?if($USER->IsAuthorized()):?>

          <div class="hamburger">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      </div>
      <div class="container-fluid header__navbar navbar">
        <div class="row">
          <div class="col-xs-6 header__logo-clone">
            <div class="header__logo-wrap">
              <a href="/"><img class="header__logo" src="/images/logo.jpg" alt="Hino"></a>
            </div>
            <div class="header__text">
              <h3 class="header__title">Хино Моторс Сэйлс</h3>
            </div>
          </div>
          <div class="col-xs-6 header__wrap">
            <ul class="navbar__list">
              <li class="navbar__item">
                <a href="/user-page/" class="navbar__user"><?echo $USER->GetFullName()?></a>
                <span class="navbar__status navbar__status_<?=HinoUsers::getDealerStatus()?>"></span>
              </li>
              <?if (IsModuleInstalled("im") && CBXFeatures::IsFeatureEnabled('WebMessenger') && (HinoUsers::isAdmin() || HinoUsers::isDealer() || HinoUsers::isSupportGroup())):?>
                <li class="navbar__item">
                  <?
                  $APPLICATION->IncludeComponent(
                    "hino:im.messenger",
                    "hino-chat",
                    Array()
                  );
                  ?>
                </li>
              <?endif;?>
              <?if (HinoUsers::isDealer()) :?>
                <li class="navbar__item">
                  <?$APPLICATION->IncludeComponent(
                    "hino:sale.basket.basket.line",
                    "hino-cart",
                    Array(
                      "HIDE_ON_BASKET_PAGES" => "N",
                      "PATH_TO_BASKET" => "/order/",
                      "SHOW_EMPTY_VALUES" => "Y",
                      "SHOW_NUM_PRODUCTS" => "Y",
                    )
                  );?>
                </li>
              <?endif;?>
              <li class="navbar__item"><a class="navbar__link navbar__text navbar__text_exit" href="/?logout=yes">Выход</a></li>
            </ul>
          </div>
        </div>
      </div>
      <?$APPLICATION->IncludeComponent(
        "bitrix:menu",
        "header-menu",
        Array(
          "ROOT_MENU_TYPE" => "top",
          "MAX_LEVEL" => "1",
          "CHILD_MENU_TYPE" => "top",
          "USE_EXT" => "Y",
          "DELAY" => "N",
          "ALLOW_MULTI_SELECT" => "Y",
          "MENU_CACHE_TYPE" => "N",
          "MENU_CACHE_TIME" => "3600",
          "MENU_CACHE_USE_GROUPS" => "Y",
          "MENU_CACHE_GET_VARS" => ""
          )
        );?>
        <?else:?>
      </div>
    </div>
  <?endif;?>
  </div>
  <div class="main-content" id="main-content">
