      </div>
      <!-- end main-content -->
    </main>
    <footer class="footer">
      <div class="footer__wrap container-fluid">
        <div class="row">
          <div class="col-sm-7 col-xs-12">
            <div class="footer__info">
              <span class="footer__title">Хино Моторс Сэйлс</span>
              <span class="footer__subtitle">Официальный дистрибьютор Hino Motors Ltd.</span>
            </div>
          </div>
          <div class="col-sm-5 col-xs-12">
            <?if($USER->IsAuthorized()):?>
              <span class="footer__supportix">
                <a href="/upload/sp.hino.ru.pdf" target="_blank" class="footer__button-pdf">Скачать инструкцию</a>
              </span>
            <?endif;?>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <span class="footer__rights">© Все права соблюдены.</span>
          </div>
          <div class="col-xs-6">
            <span class="footer__supportix">Разработка и поддержка сайта: <a href="http://supportix.ru/"><img src="/images/supportix-logo.png" alt="SUPPORTix"></a></span>
          </div>
        </div>
      </div>
    </footer>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter45634887 = new Ya.Metrika({
                        id:45634887,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/45634887" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <?$APPLICATION->IncludeFile("include/browser-update.php")?>
  </body>
</html>
