<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div id="bx-notifier-panel" class="bx-notifier-panel">
	<a href="javascript:void(0)" class="bx-notifier-indicator bx-notifier-message" title="<?=GetMessage('IM_MESSENGER_OPEN_MESSENGER_2');?>">
		<span class="bx-notifier-indicator-icon"></span>
		<span class="bx-notifier-indicator-count">0</span>
	</a>
</div>
<script type="text/javascript">
<?=CIMMessenger::GetTemplateJS(Array(), $arResult)?>
</script>
