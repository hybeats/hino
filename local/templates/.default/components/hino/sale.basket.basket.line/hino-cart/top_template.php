<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<div class="bx-hdr-profile navbar-basket">
	<div class="bx-basket-block navbar-basket__wrap">
		<?if (!$arResult["DISABLE_USE_BASKET"]): ?>
			<a class="navbar-basket__link" href="<?=$arParams['PATH_TO_BASKET']?>"><?echo $arResult['NUM_PRODUCTS'];?></a>
		<?endif;?>
	</div>
</div>
