<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->AddHeadScript("/js/plugins/jquery-ui/datepicker-ru.js");
?>
<script type="text/x-template" id="employees-template">
  <div>
    <div v-if="arEmployees" v-for="employee in arEmployees" class="user-page__management">
      <p class="user-page__text user-page__text_name"><strong>{{employee.LAST_NAME}} {{employee.NAME}} {{employee.SECOND_NAME}}</strong></p>
      <p class="user-page__text"><strong>E-mail: </strong>{{employee.EMAIL}}</p>
      <p v-if="employee.PERSONAL_PHONE" class="user-page__text"><strong>Тел.: </strong>{{employee.PERSONAL_PHONE}}</p>
      <p v-if="employee.UF_DELIVERY_ADDRESS && deliveryAddress[employee.UF_DELIVERY_ADDRESS]" class="user-page__text"><strong>Адрес доставки: </strong>{{deliveryAddress[employee.UF_DELIVERY_ADDRESS]}}</p>
      <p v-if="employee.UF_PROXY_NUMBER" class="user-page__text"><strong>№ доверенности: </strong>{{employee.UF_PROXY_NUMBER}}</p>
      <p v-if="employee.UF_PROXY_DATE_START" class="user-page__text"><strong>Дата выдачи доверенности: </strong>{{employee.UF_PROXY_DATE_START}}</p>
      <p v-if="employee.UF_PROXY_DATE_END" class="user-page__text"><strong>Окончание действия доверенности: </strong>{{employee.UF_PROXY_DATE_END}}</p>
      <?if(HinoUsers::isAdmin()):?>
        <button type="button" @click.prevent="edit(employee.ID)" class="user-page__btn">Редактировать</button>
      <?endif;?>
    </div>
  </div>
</script>

<section class="user-page">
  <div class="user-page__wrap main-wrap container-fluid">
    <div class="row">
      <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 clearfix">
        <div class="user-page__logo">
          <img class="user-page__logo" src="<?=$arResult["LOGO"]?>" alt="<?=$arResult["LEGAL"]["NAME"]?>">
        </div>
        <div class="user-page__info toggle-content js-active" data-tab="user-info" data-content="legal">
          <strong class="user-page__title"><?=$arResult["LEGAL"]["NAME"]?></strong>
          <div class="user-page__contacts">
            <p class="user-page__text"><strong>Адрес: </strong><?=$arResult["LEGAL"]["ADDRESS"]?></p>
            <p class="user-page__text"><strong>Тел.: </strong><?=$arResult["LEGAL"]["PHONE"]?></p>
            <p class="user-page__text"><strong>ИНН: </strong><?=$arResult["LEGAL"]["INN"]["VALUE"]?></p>
            <p class="user-page__text"><strong>КПП: </strong><?=$arResult["LEGAL"]["KPP"]["VALUE"]?></p>
          </div>
          <div class="user-page__management">
            <p class="user-page__subtitle">Ваш персональный менеджер</p>
            <?if (!empty($arResult["PERSONAL_MANAGER"])):?>
              <p class="user-page__text">
                <strong><?echo $arResult["PERSONAL_MANAGER"]["LAST_NAME"]." ".$arResult["PERSONAL_MANAGER"]["NAME"]." ".$arResult["PERSONAL_MANAGER"]["SECOND_NAME"]?></strong>
              <p>
              <p class="user-page__text"><strong>E-mail: </strong><?=$arResult["PERSONAL_MANAGER"]["EMAIL"]?></p>
              <p class="user-page__text"><strong>Тел.: </strong><?=$arResult["PERSONAL_MANAGER"]["PERSONAL_PHONE"]?></p>
            <?else:?>
              <p class="user-page__text">Не назначен</p>
            <?endif;?>
          </div>
        </div>
        <div class="user-page__info toggle-content" data-tab="user-info" data-content="agreement">
          <strong class="user-page__title"><?=$arResult["LEGAL"]["NAME"]?></strong>
          <?foreach ($arResult["AGREEMENTS"] as $agreement): ?>
          <div class="user-page__management">
            <p class="user-page__text">
              <strong><?=$agreement["UF_NAME"]?></strong>
            </p>
            <?if(!empty($agreement["UF_DATANACHALADEYSTV"])):?>
              <p class="user-page__text"><strong>Дата начала действия: </strong>
                <?echo FormatDate("d.m.Y", MakeTimeStamp($agreement["UF_DATANACHALADEYSTV"]))?>
              </p>
            <?endif;?>
            <?if(!empty($agreement["UF_DATANACHALADEYSTV"])):?>
              <p class="user-page__text"><strong>Дата окончания действия: </strong>
                <?echo FormatDate("d.m.Y", MakeTimeStamp($agreement["UF_DATANACHALADEYSTV"]))?>
              </p>
            <?endif;?>
          </div>
          <?endforeach; ?>
        </div>
        <div class="user-page__info toggle-content" data-tab="user-info" data-content="employees" id="employees"
          data-employees="<?=htmlspecialchars(json_encode($arResult["EMPLOYEES"]), ENT_QUOTES, 'UTF-8')?>"
          data-delivery="<?=htmlspecialchars(json_encode($arResult["DELIVERY_ADDRESS"]), ENT_QUOTES, 'UTF-8')?>">
          <strong class="user-page__title"><?=$arResult["LEGAL"]["NAME"]?></strong>
          <div id="employees-list"></div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
        <div class="user-page-nav">
          <ul class="user-page-nav__list" data-tab="user-info">
            <li class="user-page-nav__item user-page-nav__item_legal toggle-trigger js-active" data-toggle="legal">Юр. информация</li>
            <li class="user-page-nav__item user-page-nav__item_arg toggle-trigger" data-toggle="agreement">Договоры</li>
            <li class="user-page-nav__item user-page-nav__item_empl toggle-trigger" data-toggle="employees">Сотрудники</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 user-page__download">
        <span class="user-page__text user-page__text_download">Шаблоны документов</span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="user-page__documents">
          <div class="swiper-container">
            <div class="swiper-wrapper">
            <?foreach ($arResult["DOCS_TEMPLATES"] as $key => $value):?>
              <div class="swiper-slide">
                <a href="<?=$value?>" class="user-page__text user-page__text_documents"><?=$key?></a>
              </div>
            <?endforeach;?>
            </div>
          </div>
          <div class="user-page__prev"></div>
          <div class="user-page__next"></div>
        </div>
      </div>
    </div>
  </div>
</section>
