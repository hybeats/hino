$( function() {
  Vue.component('datepicker', {
    template: '<input type="text" class="input-default" :value="value">',
    props: ['value'],
    mounted: function() {
      $(this.$el).datepicker({
        dateFormat: "dd.mm.yy",
        onClose: this.onClose
      })
    },
    watch: {
      value: function (newVal) {
        $(this.el).datepicker('setDate', newVal);
      }
    },
    methods: {
      onClose: function(date) {
        this.$emit('input', date);
      }
    }
  });

  var HinoModalEditEmployee = Vue.extend({
    template:
      '<hino-modal mod="employees" :show="option.show" :showCancel="option.showCancel" :title="option.title" v-on:submitModal="submit" v-on:closeModal="close">' +
        '<label class="hino-popup__label">'+
          '<span class="hino-popup__title">№ доверенности</span>' +
          '<input type="text" class="input-default" v-model="value.proxyNumber">'+
        '</label>'+
        '<label class="hino-popup__label">'+
          '<span class="hino-popup__title">Дата выдачи доверенности</span>' +
          '<datepicker @input="updateds" v-model="value.proxyDateStart"></datepicker>'+
        '</label>'+
        '<label class="hino-popup__label">'+
          '<span class="hino-popup__title">Окончание действия доверенности</span>' +
          '<datepicker @input="submitde" v-model="value.proxyDateEnd"></datepicker>'+
        '</label>'+
        '<label class="hino-popup__label" v-if="deliveryAddress">'+
          '<span class="hino-popup__title">Адрес доставки</span>' +
          '<label class="hino-popup__radio" v-for="(name, id) in deliveryAddress"><input type="radio" v-model="value.address" name="adress" :value="id">{{name}}</label>'+
        '</label>'+
      '</hino-modal>',
    data: function(){
      return {
        option: {
          show:         false,
          showCancel:   true,
          title:        '',
        },
        deliveryAddress: {},
        userID: 0,
        value: {
          proxyNumber: '',
          proxyDateStart: '',
          proxyDateEnd: '',
          address: ''
        },
        callback: false
      }
    },
    methods: {
      updateds: function(date){
        this.value.proxyDateStart = date
      },
      submitde: function(date){
        this.value.proxyDateEnd = date
      },
      submit: function(){
        this.option.show = false;
        HinoHelper.fetch(
          '/ajax/dealer/index.php',
          {
            action:         'updatedealeruser',
            userID:         this.userID,
            proxyNumber:    this.value.proxyNumber,
            proxyDateStart: this.value.proxyDateStart,
            proxyDateEnd:   this.value.proxyDateEnd,
            address:        this.value.address
          },
          function(){
            if(typeof this.callback == 'function') {
              this.callback({
                proxyNumber:    this.value.proxyNumber,
                proxyDateStart: this.value.proxyDateStart,
                proxyDateEnd:   this.value.proxyDateEnd,
                address:        this.value.address
              })
            }
          }.bind(this)
        )
      },
      close: function(){
        this.option.show = false;
      }
    }
  });

  new Vue({
    el: '#employees-list',
    template: '#employees-template',
    data:{
      modal: false,
      arEmployees: false,
      deliveryAddress: false
    },
    mounted: function(){
      this.arEmployees = $('#employees').data('employees');
      this.deliveryAddress = $('#employees').data('delivery');
    },
    methods: {
      edit: function(id){
        if(typeof this.arEmployees[id] != 'undefined') {
          $('body').addClass('body_modal-open');

          if (!this.modal) {
            this.modal = new HinoModalEditEmployee({
              el: document.createElement('div')
            });
            document.body.appendChild(this.modal.$el);
          }

          Object.assign(
            this.modal,
            {
              option: {
                title: 'Редактирование пользователя',
                show:  true
              },
              deliveryAddress: this.deliveryAddress,
              userID: id,
              value: {
                proxyNumber: this.arEmployees[id]['UF_PROXY_NUMBER'],
                proxyDateStart: this.arEmployees[id]['UF_PROXY_DATE_START'],
                proxyDateEnd: this.arEmployees[id]['UF_PROXY_DATE_END'],
                address: this.arEmployees[id]['UF_DELIVERY_ADDRESS'],
              },
              callback: function(data){
                Object.assign(this.arEmployees[id], {
                  'UF_PROXY_NUMBER': data.proxyNumber,
                  'UF_PROXY_DATE_START': data.proxyDateStart,
                  'UF_PROXY_DATE_END': data.proxyDateEnd,
                  'UF_DELIVERY_ADDRESS': data.address
                });
              }.bind(this)
            }
          );
        }
      }
    }
  });
});
