<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="user-page">
  <div class="user-page__wrap main-wrap container-fluid">
    <div class="row">
      <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 clearfix">
        <div class="user-page__logo">
          <img class="user-page__logo" src="/images/no-logo.jpg" alt="ООО “Hino dealer”">
        </div>
        <div class="user-page__info">
          <strong class="user-page__title"><?=$arResult["NAME"]?></strong>
          <div class="user-page__contacts">
            <p class="user-page__text"><strong>E-mail: </strong><?=$arResult["EMAIL"]?></p>
            <p class="user-page__text"><strong>Тел.: </strong><?=$arResult["PHONE"]?></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
        <?if(HinoUsers::isAdmin()):?>
          <div class="user-page-nav">
            <ul class="user-page-nav__list user-page-nav__list_admin">
              <li class="user-page-nav__item">
                <a href="/dealers-list/">Список дилеров</a>
              </li>
              <li class="user-page-nav__item">
                <a href="/history/">Список заказов</a>
              </li>
              <li class="user-page-nav__item" id="admin_jpy">
                <a href="#" v-on:click.prevent="edit" data-jpy="<?=$arResult["JPY"]?>">Коэффициент JPY</a>
              </li>
            </ul>
          </div>
        <?endif;?>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 user-page__download">
        <span class="user-page__text user-page__text_download">Шаблоны документов</span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="user-page__documents">
          <div class="swiper-container">
            <div class="swiper-wrapper">
            <?foreach ($arResult["DOCS_TEMPLATES"] as $key => $value):?>
              <div class="swiper-slide">
                <a href="<?=$value?>" class="user-page__text user-page__text_documents"><?=$key?></a>
              </div>
            <?endforeach;?>
            </div>
          </div>
          <div class="user-page__prev"></div>
          <div class="user-page__next"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="user-page__popup import-popup">

  </div>
</section>
