$( function() {

  var HinoModalEditJPY = Vue.extend({
    template:
      '<hino-modal :show="option.show" :showCancel="option.showCancel" :title="option.title" v-on:submitModal="submit" v-on:closeModal="close">' +
        '<label class="hino-popup__label">'+
          '<input type="text" class="input-default" v-model="coefficientJPY">'+
        '</label>'+
      '</hino-modal>',
    data: function(){
      return {
        option: {
          show:         false,
          showCancel:   true,
          title:        '',
        },
        coefficientJPY: false
      }
    },
    methods:{
      submit: function(){
        if(this.coefficientJPY == "") return false;
        
        this.option.show = false;
        HinoHelper.fetch(
          '/ajax/dealer/index.php',
          {
            action:      'updateordercoefficientjpy',
            coefficient: parseFloat(this.coefficientJPY)
          },
          function(){
            $('#admin_jpy').find('a').data('jpy', this.coefficientJPY);
          }.bind(this)
        )
      },
      close: function(){
        this.option.show = false;
      }
    }
  });

  new Vue({
    el: '#admin_jpy',
    data:{
      modal: false
    },
    methods: {
      edit: function(){
        $('body').addClass('body_modal-open');

        if (!this.modal) {
          this.modal = new HinoModalEditJPY({
            el: document.createElement('div')
          });
          document.body.appendChild(this.modal.$el);
        }
        var el      = $(event.target);

        Object.assign(
          this.modal,
          {
            option: {
              title: 'Коэффициент перевода JPY',
              show:  true
            },
            coefficientJPY: el.data('jpy')
          }
        );
      }
    }
  });
});
