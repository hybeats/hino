$( function() {
  $( "#history_form_date_from, #history_form_date_to" )
    .datepicker({
      dateFormat: "dd.mm.yy"
    })
    .attr('readonly','readonly');

  var HinoModalEditOrder = Vue.extend({
    template:
      '<hino-modal :show="option.show" :showCancel="option.showCancel" :title="option.title" v-on:submitModal="submit" v-on:closeModal="close">' +
        '<div class="hino-popup__select-wrap">'+
          '<span class="hino-popup__select-title">Перевозчик</span>' +
          '<select class="select-default hino-popup__select" v-model="value.company" name="company">' +
            '<option v-for="(name, id) in companyAr" :value="id">{{name}}</option>'+
          '</select>'+
        '</div>'+
        '<label class="hino-popup__label"><input class="input-default" type="text" v-model="value.ttn" placeholder="ТТН"></label>'+
      '</hino-modal>',
    mounted: function(){
      this.companyAr = HinoCompanyAR;
    },
    data: function(){
      return {
        option: {
          show:         false,
          showCancel:   true,
          title:        '',
        },
        companyAr: [],
        value: {
          order_id: false,
          company:  false,
          ttn:      ''
        }
      }
    },
    methods:{
      submit: function(){
        this.option.show = false;
        HinoHelper.fetch(
          '/ajax/order/index.php',
          {
            action:   'updateorder',
            order_id: this.value.order_id,
            company:  this.value.company,
            ttn:      this.value.ttn
          },
          function(){
            var rowOrder = $('#order_' + this.value.order_id);
            rowOrder.find('.history-table__col_company').text(HinoCompanyAR[this.value.company]);
            rowOrder.find('.history-table__col_ttn').text(this.value.ttn);
          }.bind(this)
        )
      },
      close: function(){
        this.option.show = false;
      }
    }
  });

  var HinoModalDetailOrder = Vue.extend({
    template: '#detail-order',
    data: function(){
      return {
        option: {
          show:         false,
          showCancel:   false,
          title:        '',
        },
        mod: 'orderdetail',
        orderItemList: [],
        order: false
      }
    },
    methods:{
      close: function(){
        this.option.show = false;
      },
      submit: function(){
        this.option.show = false;
      }
    }
  });


  new Vue({
    el: '#history-page-table',
    data:{
      modal: false,
      modalDetail: false
    },
    methods: {
      showDetailOrder: function(event){
        var id = $(event.target).data('id');
        if(!id) return false;

        HinoHelper.fetch(
          '/ajax/order/index.php',
          {action: 'getorderitems', id: id},
          function(data){
            if(typeof data.result != 'undefined'){
              $('body').addClass('body_modal-open');

              if (!this.modalDetail) {
                this.modalDetail = new HinoModalDetailOrder({
                  el: document.createElement('div')
                });
                document.body.appendChild(this.modalDetail.$el);
              }

              Object.assign(
                this.modalDetail,
                {
                  option: {
                    title: 'Заказ №' + data.result.order.name,
                    show:  true
                  },
                  order:         data.result.order,
                  orderItemList: data.result.order_item_list
                }
              );
            }
          }.bind(this)
        );
      },
      download: function(name1c){
        if(typeof name1c == 'undefined' || name1c == ''){
          HinoHelper.toasts.showToast('Ошибка ID заказа', {theme: 'error'});
          return false;
        }
        HinoHelper.fetch(
          '/ajax/order/download_zip.php',
          {
            action: 'check',
            name1c: name1c
          },
          function(data){
            if(typeof data.error == 'undefined' &&  typeof data.result != 'undefined'){
              var link = document.createElement('a');
              link.href = '/ajax/order/download_zip.php?action=download&name=' + data.result.name;
              link.download = 'download.zip';

              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
              delete link;
            }
          }.bind(this)
        );
      },
      edit: function(event){
        $('body').addClass('body_modal-open');

        if (!this.modal) {
          this.modal = new HinoModalEditOrder({
            el: document.createElement('div')
          });
          document.body.appendChild(this.modal.$el);
        }
        var el      = $(event.target),
            id      = el.data('id'),
            company = el.data('company'),
            ttn     = el.data('ttn');

        Object.assign(
          this.modal,
          {
            option: {
              title: 'Редактировать заказ №' + id,
              show:  true
            },
            value: {
              order_id: id,
              company:  company,
              ttn:      ttn
            }
          }
        );
      }
    }
  });

  new Vue({
    el: '#history_form_filter',
    mounted: function(){
      this.managerAr       = HinoHistoryForm.managers;
      this.dealerSelected  = HinoHistoryForm.dealerSelected;
      this.managerSelected = HinoHistoryForm.managerSelected;
    },
    watch: {
      dealerSelected: function (dealerId) {
        var arManagerName = [];

        if(dealerId > 0) {
          this.managerAr = HinoHistoryForm.managers.filter(function(manager){
            if (HinoHistoryForm.dealers[dealerId].EMPLOYEE.indexOf(manager.ID) != -1) {
              arManagerName.push(manager.NAME);
              return true;
            } else {
              return false;
            }
          });
        } else {
          this.managerAr = HinoHistoryForm.managers;
        }

        if (arManagerName.indexOf(this.managerSelected) == -1 && dealerId != 0) {
          this.managerSelected = '0';
        }
      }
    },
    data: {
      managerAr: [],
      dealerSelected: false,
      managerSelected: '0'
    }
  })

});
