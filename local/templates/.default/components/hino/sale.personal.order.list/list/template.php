<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$APPLICATION->AddHeadScript("/js/plugins/jquery-ui/datepicker-ru.js");?>
<?
if(HinoUsers::isAdmin()  || HinoUsers::isSupportGroup()) {
  $APPLICATION->AddHeadString('<script type="text/javascript">var HinoCompanyAR = '.CUtil::PhpToJSObject($arResult["COMPANY"]).';</script>');
}

if(HinoUsers::isAdmin() || HinoUsers::isServiceGroup() || HinoUsers::isSupportGroup()) {
  $hinoHistoryForm = array(
    'dealers'         => $arResult["DEALERS"],
    'managers'        => $arResult["MANAGERS"],
    'dealerSelected'  => $_SESSION["spo_filter_user_id"]? $_SESSION["spo_filter_user_id"]: 0,
    'managerSelected' => $_SESSION["spo_filter_manager"]? $_SESSION["spo_filter_manager"]: 0
  );

  $APPLICATION->AddHeadString('<script type="text/javascript">var HinoHistoryForm = '.CUtil::PhpToJSObject($hinoHistoryForm).';</script>');
}
?>
<script type="text/x-template" id="detail-order">
  <hino-modal :show="option.show" :showCancel="option.showCancel" :mod="mod" :title="option.title" v-on:closeModal="close" v-on:submitModal="submit">
    <div class="order-page">
      <div class="order-page__table parts-table">
        <div class="parts-table__header">
          <div class="parts-table__head parts-table__head_name">№</div>
          <div class="parts-table__head parts-table__head_number">Артикул</div>
          <div class="parts-table__head parts-table__head_name">Наименование</div>
          <div class="parts-table__head parts-table__head_count">Количество</div>
          <div class="parts-table__head parts-table__head_grade">Moving<br/>Grade</div>
          <div class="parts-table__head parts-table__head_code">Код<br/>продукта</div>
          <div class="parts-table__head parts-table__head_weight">Вес</div>
          <div class="parts-table__head parts-table__head_cost">Стоимость</div>
          <div class="parts-table__head parts-table__head_result-cost">Общая<br/>стоимость</div>
        </div>
        <div class="parts-table__row" v-for="(part, index) in orderItemList">
          <div class="parts-table__col parts-table__col_name">{{index + 1}}</div>
          <div class="parts-table__col parts-table__col_number">{{part.article}}</div>
          <div class="parts-table__col parts-table__col_name" v-html="part.name"></div>
          <div class="parts-table__col parts-table__col_count">{{part.count}} шт.</div>
          <div class="parts-table__col parts-table__col_grade">{{part.grade}}</div>
          <div class="parts-table__col parts-table__col_code">{{part.code}}</div>
          <div class="parts-table__col parts-table__col_weight">{{part.weight}}</div>
          <div class="parts-table__col parts-table__col_cost">{{part.price.formatPrice()}} руб.</div>
          <div class="parts-table__col parts-table__col_result-cost">{{part.fullprice.formatPrice()}} руб.</div>
        </div>
        <div class="parts-table__row_result" v-if="order">
          <div class="parts-table__col parts-table__col_name parts-table__col_empty"></div>
          <div class="parts-table__col parts-table__col_number parts-table__col_empty"></div>
          <div class="parts-table__col parts-table__col_name parts-table__col_empty"></div>
          <div class="parts-table__col parts-table__col_count parts-table__col_empty"></div>
          <div class="parts-table__col parts-table__col_grade parts-table__col_empty"></div>
          <div class="parts-table__col parts-table__col_grade parts-table__col_empty"><b>Итого:</b></div>
          <div class="parts-table__col parts-table__col_weight">{{order.weight}}</div>
          <div class="parts-table__col parts-table__col_cost">{{order.cost.formatPrice()}} руб.</div>
          <div class="parts-table__col parts-table__col_result-cost">{{order.resultcost.formatPrice()}} руб.</div>
        </div>
      </div>
      <div class="order-comment" v-if="order.comments">
        <div class="order-comment__text">
          <div class="row">
            <div class="col-lg-12"><h2 class="order-page__title">Комментарий</h2></div>
          </div>
          <div class="row">
            <div class="col-xs-10">
              {{order.comments}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </hino-modal>
</script>

<div class="main-wrap history-page__wrap">
  <form id="history_form" class="history-page__form history-form" method="get">
    <div class="row history-form__row">
      <div class="col-md-3 col-xs-12 history-form__col">
        <label class="history-form__label" for="history_form_number">
          Номер заказа
          <input id="history_form_number" class="input-default history-form__input" type="text" name="filter_id" value="<?=$_SESSION["spo_filter_id"]?>" placeholder="Номер заказа">
        </label>
      </div>
      <div class="col-md-3 col-xs-6 history-form__col">
        <label class="history-form__label" for="history_form_date_from">
          Дата от
          <input id="history_form_date_from" class="input-default history-form__input" type="text" name="filter_date_from" value="<?=$_SESSION["spo_filter_date_from"]?>" placeholder="Дата">
        </label>
      </div>
      <!-- <div class="col-md-3 col-xs-6 history-form__col">
        <label class="history-form__label" for="history_form_date_to">
          Дата до
          <input id="history_form_date_to" class="input-default history-form__input" type="text" name="filter_date_to" value="<?=$_SESSION["spo_filter_date_to"]?>" placeholder="Дата">
        </label>
      </div> -->
      <div class="col-md-3 col-xs-6 history-form__col">
        <label class="history-form__label" for="history_form_product_article">
          Наличие в заказе
          <input id="history_form_product_article" class="input-default history-form__input" type="text" name="filter_product_article" value="<?=$_SESSION["spo_filter_product_article"]?>" placeholder="Артикул">
        </label>
      </div>
      <div class="col-md-3 col-xs-12 history-form__col">
        <label class="history-form__label">
          Склад
          <select class="select-default history-form__select" name="filter_store">
            <option value="0">Выберите склад</option>
            <?foreach ($arResult["STORE"] as $id => $name):?>
              <option value="<?=$id?>" <?if($id == $_SESSION["spo_filter_store"]):?> selected<?endif;?>><?=$name?></option>
            <?endforeach;?>
          </select>
        </label>
      </div>
    </div>
    <div class="row history-form__row" id="history_form_filter">
      <div class="col-md-6 col-xs-12 history-form__col">
        <div class="history-form__label">
          Cтатус заказа
          <div class="history-form__checkbox-wrap">
            <?foreach($arResult["INFO"]["STATUS"] as $status):?>
              <input class="history-form__checkbox checkbox-default"
                type="checkbox"
                id="status_<?=$status["ID"]?>"
                name="filter_status[]"
                <?if(in_array($status["ID"], $_SESSION["spo_filter_status"])):?> checked<?endif;?>
                value="<?=$status["ID"]?>">
              <label for="status_<?=$status["ID"]?>" class="history-form__checkbox-label">
                <?=$status["NAME"]?>
              </label>
            <?endforeach;?>
          </div>
        </div>
      </div>
      <?if(!empty($arResult["DEALERS"])):?>
        <div class="col-md-3 col-xs-6 history-form__col">
          <label class="history-form__label" for="history_form_user_id">
            Дилер
            <select v-model="dealerSelected" class="select-default history-form__select" name="filter_user_id" id="history_form_user_id">
              <option value="0">Выберите дилера</option>
              <?foreach ($arResult["DEALERS"] as $id => $dealer):?>
                <option value="<?=$id?>"><?=$dealer['NAME']?></option>
              <?endforeach;?>
            </select>
          </label>
        </div>
        <div class="col-md-3 col-xs-6 history-form__col">
          <label class="history-form__label" for="history_form_manager">
            Менеджер
            <select v-model="managerSelected" class="select-default history-form__select" name="filter_manager" id="history_form_manager">
              <option value="0">Выберите менеджера</option>
              <option v-for="manager in managerAr" :value="manager.NAME">{{manager.NAME}}</option>
            </select>
          </label>
        </div>
      <?endif;?>
    </div>
    <div class="row history-form__row">
      <div class="col-md-2 col-sm-3 col-xs-6 history-form__col">
        <input class="history-form__submit" type="submit" name="filter" value="Отфильтровать">
      </div>
      <div class="col-md-2 col-sm-3 col-xs-6 history-form__col">
        <a class="history-form__cancel"href="?del_filter=Y">Сбросить</a>
      </div>
    </div>
  </form>
  <?if(!empty($arResult["ORDERS"])):?>
    <div class="row">
      <div class="col-lg-12">
        <div class="history-page__table table history-table" id="history-page-table">
          <div class="history-table__header table__header">
            <div class="history-table__head table__head history-table__head_id">Номер заказа</div>
            <div class="history-table__head table__head history-table__head_num">Номер УПД</div>
            <?if(HinoUsers::isAdmin() || HinoUsers::isServiceGroup() || HinoUsers::isSupportGroup()):?>
              <div class="history-table__head table__head history-table__head_dealer">Дилер</div>
            <?else:?>
              <div class="history-table__head table__head history-table__head_dealer">Менеджер</div>
            <?endif;?>
            <div class="history-table__head table__head history-table__head_status">Статус</div>
            <div class="history-table__head table__head history-table__head_type">Тип заказа</div>
            <div class="history-table__head table__head history-table__head_store">Склад</div>
            <div class="history-table__head table__head history-table__head_date">Дата заказа</div>
            <div class="history-table__head table__head history-table__head_info">Общая<br/>стоимость</div>
            <div class="history-table__head table__head history-table__head_info">Перевозчик</div>
            <div class="history-table__head table__head history-table__head_info">ТТН</div>
            <div class="history-table__head table__head history-table__head_download">Документы</div>
            <?if(HinoUsers::isAdmin() || HinoUsers::isSupportGroup()):?>
              <div class="history-table__head table__head history-table__head_info"></div>
            <?endif;?>
          </div>
          <?foreach($arResult["ORDERS"] as $order):?>
            <?$status_class = ($order["ORDER"]["CANCELED"] == "Y")? "df": strtolower($arResult["INFO"]["STATUS"][$order["ORDER"]["STATUS_ID"]]["ID"]);?>
            <div id="order_<?=$order["ORDER"]["ID"]?>" class="history-table__row table__row status_<?=$status_class?>">
              <div class="history-table__col table__col history-table__col_id" v-on:click="showDetailOrder" data-id="<?=$order["ORDER"]["ID"]?>">
                <?=$order["ORDER"]["ID"];?>
              </div>
              <div class="history-table__col table__col history-table__col_num">
                <?
                  if($idUPD = HinoOrders::getIdUPD($order["ORDER"]["ID"])){
                    echo $idUPD;
                  }else{
                    echo '-';
                  }
                ?>
              </div>
              <?$managerFIO = $order["ORDER"]["MANAGER_NAME"];?>
              <?if(HinoUsers::isAdmin() || HinoUsers::isServiceGroup() || HinoUsers::isSupportGroup()):?>
                <div class="history-table__col table__col history-table__col_dealer"><?=$arResult["DEALERS"][$order["ORDER"]["USER_ID"]]["NAME"]?> <?=($managerFIO? "<br/>(".$managerFIO.")": "")?></div>
              <?else:?>
                <div class="history-table__col table__col history-table__col_dealer"><?=$managerFIO?></div>
              <?endif;?>
              <div class="history-table__col table__col history-table__col_status">
                <span class="history-table__status-icon"></span>
                <?=($order["ORDER"]["CANCELED"] == "Y"? "Отменен": $arResult["INFO"]["STATUS"][$order["ORDER"]["STATUS_ID"]]["NAME"])?>
              </div>
              <div class="history-table__col table__col history-table__col_type"><?=$order["SHIPMENT"][0]["DELIVERY_NAME"]?></div>
              <div class="history-table__col table__col history-table__col_store">
                <?/*=($order["ORDER"]["STORE_ID"] == 0? "Япония": $arResult["STORE"][$order["ORDER"]["STORE_ID"]])*/?>
                <?=($order["ORDER"]["STORE_ID"] === "0"? "Япония": $arResult["STORE"][$order["ORDER"]["STORE_ID"]])?>
              </div>
              <div class="history-table__col table__col history-table__col_date"><?=$order["ORDER"]["DATE_INSERT"];?></div>
              <div class="history-table__col table__col history-table__col_info"><?=$order["ORDER"]["FORMATED_PRICE"];?></div>
              <div class="history-table__col table__col history-table__col_company">
                <?if($order["SHIPMENT"][0]["COMPANY_ID"]):?>
                  <?=$arResult["COMPANY"][$order["SHIPMENT"][0]["COMPANY_ID"]]?>
                <?else:?>
                  &ndash;
                <?endif;?>
              </div>
              <div class="history-table__col table__col history-table__col_ttn">
                <?if($order["SHIPMENT"][0]["DELIVERY_DOC_NUM"]):?>
                  <?=$order["SHIPMENT"][0]["DELIVERY_DOC_NUM"]?>
                <?else:?>
                  &ndash;
                <?endif;?>
              </div>
              <div class="history-table__col table__col history-table__col_download">
                <?if($order["SHIPMENT"][0]["STATUS_ID"] == "SR" || $order["SHIPMENT"][0]["STATUS_ID"] == "DF"):?>
                  <a href="#" v-on:click.prevent="download('<?=$order["SHIPMENT"][0]["ID_1C"]?>')" class="history-table__link">Скачать</a>
                <?endif;?>
              </div>
              <?if(HinoUsers::isAdmin() || HinoUsers::isSupportGroup()):?>
                <div class="history-table__col table__col history-table__col_info">
                  <?if($order["SHIPMENT"][0]["STATUS_ID"] == "SR" || $order["SHIPMENT"][0]["STATUS_ID"] == "DF"):?>
                    <button
                      class="history-table__button history-table__button_edit"
                      data-company="<?=$order["SHIPMENT"][0]["COMPANY_ID"]?>"
                      data-ttn="<?=$order["SHIPMENT"][0]["DELIVERY_DOC_NUM"]?>"
                      data-id="<?=$order["ORDER"]["ID"]?>"
                      v-on:click="edit"
                      type="button">
                    </button>
                  <?endif;?>
                </div>
              <?endif;?>
            </div>
          <?endforeach;?>
        </div>
      </div>
    </div>
  <?else:?>
    <div class="row">
      <div class="col-lg-12">
        <div class="history-page__empty-list">
          <?=GetMessage("SPOL_TPL_EMPTY_ORDER_LIST")?>
        </div>
      </div>
    </div>
  <?endif;?>
</div>

<?=$arResult["NAV_STRING"];?>
