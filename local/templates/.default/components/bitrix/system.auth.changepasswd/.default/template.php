<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<section class="auth-page">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 auth-page__wrap">
        <form class="auth-page__form auth-form" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_FORM"]?>">
          <input type="hidden" name="AUTH_FORM" value="Y">
          <input type="hidden" name="TYPE" value="CHANGE_PWD">
          <strong class="auth-form__title"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></strong>

          <?if (strlen($arResult["BACKURL"]) > 0):?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
          <?endif?>
          <?foreach ($arResult["POST"] as $key => $value):?>
          <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
          <?endforeach?>
          <div class="auth-form__wrap">
            <div class="auth-form__row auth-form__row_text">
              <?ShowMessage($arParams["~AUTH_RESULT"]);?>
            </div>
            <div class="auth-form__row">
              <input class="auth-form__input bx-auth-input" type="text" name="USER_LOGIN" placeholder="<?=GetMessage("AUTH_LOGIN")?>" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" />
            </div>
            <div class="auth-form__row">
              <input class="auth-form__input bx-auth-input" type="text" name="USER_CHECKWORD" placeholder="<?=GetMessage("AUTH_CHECKWORD")?>" maxlength="255" value="<?=$arResult["USER_CHECKWORD"]?>" />
            </div>
            <div class="auth-form__row">
              <input class="auth-form__input bx-auth-input" type="password" name="USER_PASSWORD" placeholder="<?=GetMessage("AUTH_NEW_PASSWORD_REQ")?>" maxlength="50" autocomplete="off" value="<?=$arResult["USER_PASSWORD"]?>" />
            </div>
            <div class="auth-form__row">
              <input class="auth-form__input bx-auth-input" type="password" name="USER_CONFIRM_PASSWORD" placeholder="<?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?>" maxlength="50" autocomplete="off" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" />
            </div>
            <div class="auth-form__row">
              <input class="auth-form__submit" type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>" />
            </div>
            <div class="auth-form__row auth-form__row_text">
              <?=$arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?>
            </div>
            <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
              <a class="auth-form__forgot" href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_AUTH")?></a>
            <?endif?>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  document.bform.USER_LOGIN.focus();
</script>
