<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="main-wrap">
	<div class="container-fluid news-detail">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h1 class="news-detail__title title-h1"><?=$arResult["NAME"]?></h1>
				<span class="news-detail__date news-date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
				<div class="news-detail__picture">
					<img class="news-detail__image" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>">
				</div>
				<div class="news-detail__text news-text"><?=$arResult["DETAIL_TEXT"]?></div>
			</div>
		</div>
	</div>
</div>
