<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<div class="header-menu">
	<div class="header-menu__wrap main-wrap">
		<div class="header-menu__mobile-header mobile-header">
			<div class="mobile-header__info">
				<div class="mobile-header__userbar">
					<span class="mobile-header__user navbar__user"><?echo CUser::GetFullName();?></span>
					<span class="mobile-header__status navbar__status navbar__status_unblocked"></span>
				</div>
			</div>
			<div class="mobile-header__logo">
				<img class="mobile-header__img" src="/images/logo-mobile-menu.png" alt="Hino">
			</div>
		</div>
		<ul class="header-menu__list">
		<?
		foreach($arResult as $arItem):
			if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
				continue;
		?>
			<?if($arItem["SELECTED"]):?>
				<li class="header-menu__item"><a href="<?=$arItem["LINK"]?>" class="header-menu__link header-menu__link_selected header-menu__link_<?=$arItem["PARAMS"]["MODIFIER"]?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li class="header-menu__item"><a href="<?=$arItem["LINK"]?>" class="header-menu__link header-menu__link_<?=$arItem["PARAMS"]["MODIFIER"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>
		<?endforeach?>
		</ul>
	</div>
</div>
<?endif?>
