<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach ($arResult["ITEMS"] as $key => $arItem):?>

<?if($key % 3 == 0 || $key == 0):?> <!--Открываем новый row для следующих 3х элементов-->
<div class="news-page__list news-list row">
<?endif;?>

	<div class="news-list__item col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
      <img class="news-list__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
    </a>
		<span class="news-list__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news-list__title"><?=$arItem["NAME"]?></a>
		<div class="news-list__text"><?=$arItem["PREVIEW_TEXT"]?></div>
	</div>

<?if($key % 3 == 2 || $key == 2):?> <!--Зыкрываем row для последних 3х элементов-->
</div>
<?endif;?>

<?endforeach; ?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?=$arResult["NAV_STRING"]?>
		</div>
	</div>
<?endif;?>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<form id="news-subscribe" class="news-page__subscribe subscribe-form">
			<input type="hidden" name="action" value="subscribe">
			<input type="hidden" name="sub_status" value="">
			<input type="hidden" name="sub_exists" value="">
			<label class="subscribe-form__label" for="">Подпишитесь на рассылку</label>
			<input class="subscribe-form__submit" type="submit" name="subscribe_submit" value="Подписаться">
			<p class="subscribe-form__result"></p>
		</form>
	</div>
</div>
