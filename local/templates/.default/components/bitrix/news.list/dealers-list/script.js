$( function() {
  var HinoModalEditDealer = Vue.extend({
    template:
      '<hino-modal :mod="option.mod" :show="option.show" :showCancel="option.showCancel" :title="option.title" v-on:submitModal="submit" v-on:closeModal="close">' +
        '<div class="hino-popup__label">'+
          '<span class="hino-popup__title">Менеджер</span>' +
          '<select class="select-default hino-popup__select" v-model="value.manager" name="manager">' +
            '<option v-for="(name, id) in arManager" :value="id">{{name}}</option>'+
          '</select>'+
        '</div>'+
        '<div class="hino-popup__label">'+
          '<span class="hino-popup__title">Приоритет складов</span>' +
          '<draggable :list="value.storeDealerList" class="hino-popup__drag">'+
            '<div class="hino-popup__drag-item" v-for="id in value.storeDealerList">{{arStore[id]}}</div>'+
          '</draggable>'+
        '</div>'+
      '</hino-modal>',
    mounted: function(){
      this.arStore   = arHinoStore;
      this.arManager = arHinoManager;
    },
    data: function(){
      return {
        option: {
          show:       false,
          title:      ''
        },
        arStore: [],
        arManager: [],
        value: {
          dealer_id:       false,
          manager:         false,
          storeDealerList: []
        }
      }
    },
    methods:{
      submit: function(){
        this.option.show = false;

        HinoHelper.fetch(
          '/ajax/dealer/index.php',
          {
            action:    'updatedealer',
            id_dealer: this.value.dealer_id,
            store:     JSON.stringify(this.value.storeDealerList),
            manager:   this.value.manager
          },
          function(){
            var rowControls = $('#controls_' + this.value.dealer_id);
            rowControls.find('.dealers-controls__item_edit').data('manager', this.value.manager);
            arHinoStoreDealerSort[this.value.dealer_id] = this.value.storeDealerList;
          }.bind(this)
        )
      },
      close: function(){
        this.option.show = false;
      }
    }
  });

  new Vue({
    el: '#dealers-list',
    data:{
      urlAjax: '/ajax/dealer/index.php',
      modal:   false
    },
    mounted: function(){
      this.arHinoStoreDealerSort = arHinoStoreDealerSort;
    },
    methods: {
      dealerBlock: function(event){
        var el = $(event.target),
            id = el.data('id'),
            locked = parseInt(el.data('locked'));

        HinoConfirm.open(
          {
            title: 'Блокировка дилера',
            text:  locked == 0? 'Заблокировать дилера?': 'Разблокировать дилера?'
          },
          function(){
            HinoHelper.fetch(
              this.urlAjax,
              {
                action:    'blockdealer',
                id_dealer: id,
                locked:    +!locked
              },
              function(data){
                el
                  .toggleClass('dealers-controls__item_status-block', !!data.result.locked)
                  .data('locked', data.result.locked);
              }
            );
          }.bind(this)
        );
      },
      dealerEdit: function(event){
        $('body').addClass('body_modal-open');

        if (!this.modal) {
          this.modal = new HinoModalEditDealer({
            el: document.createElement('div')
          });
          document.body.appendChild(this.modal.$el);
        }
        var el = $(event.target),
            id = el.data('id');

        Object.assign(
          this.modal,
          {
            option: {
              showCancel: true,
              title: 'Редактировать дилера ' + el.data('name'),
              show:  true,
              mod:   'dealeredit'
            },
            value: {
              dealer_id:       id,
              manager:         el.data('manager'),
              storeDealerList: arHinoStoreDealerSort[id].slice(0)
            }
          }
        );
      }
    }
  });
});
