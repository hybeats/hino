<?
use Bitrix\Catalog\StoreTable,
    Bitrix\Main\UserGroupTable,
    Bitrix\Main\UserTable;

//список складов
$resStore = StoreTable::getList();
while($obStore = $resStore->Fetch()){
  $arResult["STORE"][$obStore["ID"]] = $obStore["TITLE"];
}

//список менеджеров HINO
$resUser = UserTable::getList(Array(
  "select" => array("ID", "NAME", "LAST_NAME"),
  "filter" => array(
    "UserGroupTable:USER.GROUP_ID" => SUPPORT_HINO
  )
));

while($obUser = $resUser->Fetch()){
  $arResult["MANAGER"][$obUser["ID"]] = $obUser["LAST_NAME"]." ".$obUser["NAME"];
}

//отсортировванные склады для всех дилеров
foreach ($arResult["ITEMS"] as $arItem){
  $arResult["SORT_STORE"][$arItem["ID"]] = HinoUsers::getDealerListStoreByID($arItem["ID"]);
}
