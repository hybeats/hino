<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->AddHeadString('<script type="text/javascript">var arHinoStore = '.CUtil::PhpToJSObject($arResult["STORE"]).';</script>');
$APPLICATION->AddHeadString('<script type="text/javascript">var arHinoStore = '.CUtil::PhpToJSObject($arResult["STORE"]).';</script>');
$APPLICATION->AddHeadString('<script type="text/javascript">var arHinoManager = '.CUtil::PhpToJSObject($arResult["MANAGER"]).';</script>');
$APPLICATION->AddHeadString('<script type="text/javascript">var arHinoStoreDealerSort = '.CUtil::PhpToJSObject($arResult["SORT_STORE"]).';</script>');

$APPLICATION->AddHeadString('<script src="/js/plugins/sortable/Sortable.min.js" charset="utf-8"></script>');
$APPLICATION->AddHeadString('<script src="/js/plugins/sortable/vuedraggable.min.js" charset="utf-8"></script>');
?>
<section class="dealers-list">
  <div class="container-fluid">
    <div class="main-wrap dealers-list__wrap">
      <div class="row">
        <div class="col-md-12">
          <h1 class="title-h1">Список дилеров</h1>
          <div class="dealers-list__parent" id="dealers-list">
          <?foreach ($arResult["ITEMS"] as $arItem): ?>
            <div class="dealers-list__item">
              <div class="dealers-list__logo-wrap">
                <img
                  width="80"
                  height="80"
                  class ="dealers-list__logo"
                  src="<?echo ($arItem["PREVIEW_PICTURE"]["SRC"])? $arItem["PREVIEW_PICTURE"]["SRC"]: "/images/no-logo.jpg"?>"
                  alt="<?=$arItem["NAME"]?>"
                />
              </div>
              <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="dealers-list__name"><?=$arItem["NAME"]?></a>
              <div class="dealers-list__controls dealers-controls">
                <div class="dealers-controls__wrap" id="controls_<?=$arItem["ID"]?>">
                  <span
                    class="dealers-controls__item dealers-controls__item_edit"
                    v-on:click.prevent="dealerEdit"
                    data-name="<?=$arItem["NAME"]?>"
                    data-manager="<?=$arItem["PROPERTIES"]["MANAGER"]["VALUE"]? $arItem["PROPERTIES"]["MANAGER"]["VALUE"] :MANAGER_DEFAULT;?>"
                    data-id="<?=$arItem["ID"]?>">
                  </span>
                  <span
                    class="dealers-controls__item dealers-controls__item_status <?if($arItem["PROPERTIES"]["STATUS"]["VALUE_XML_ID"] == "locked"):?>dealers-controls__item_status-block<?endif;?>"
                    data-locked="<?=intval($arItem["PROPERTIES"]["STATUS"]["VALUE_XML_ID"] == "locked")?>"
                    data-id="<?=$arItem["ID"]?>"
                    v-on:click.prevent="dealerBlock">
                  </span>
                </div>
              </div>
            </div>
          <?endforeach; ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <?=$arResult["NAV_STRING"]?>
        </div>
      </div>
    </div>
  </div>
<section>
