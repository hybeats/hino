<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
// номер текущей страницы
$curPage = $arResult["NAV_RESULT"]->NavPageNomer;
// всего страниц - номер последней страницы
$totalPages = $arResult["NAV_RESULT"]->NavPageCount;
?>
<div class="main-news__list news-list row">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="news-list__item col-lg-4 col-md-4 col-sm-4">
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
      <img class="news-list__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
    </a>
		<span class="news-list__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news-list__title"><?=$arItem["NAME"]?></a>
		<div class="news-list__text"><?=$arItem["PREVIEW_TEXT"]?></div>
	</div>
<?endforeach;?>
<?if($curPage < $totalPages):?>
	<div class="button-wrap row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<a href="<?=$arParams["AJAX_URL"]?>?PAGEN_1=<?=$curPage+1?>" class="main-news__button js-load">Загрузить еще</a>
		</div>
	</div>
<?endif;?>
</div>
