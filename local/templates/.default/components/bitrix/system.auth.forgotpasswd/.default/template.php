<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<section class="auth-page">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 auth-page__wrap">
        <form name="bform" class="auth-page__form auth-form" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
          <?if (strlen($arResult["BACKURL"]) > 0):?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
          <?endif?>
          <input type="hidden" name="AUTH_FORM" value="Y">
          <input type="hidden" name="TYPE" value="SEND_PWD">
          <strong class="auth-form__title"><?=GetMessage("AUTH_GET_CHECK_STRING")?></strong>

          <div class="auth-form__wrap">
            <div class="auth-form__row auth-form__row_text">
              <?=GetMessage("AUTH_FORGOT_PASSWORD_1")?>
              <?ShowMessage($arParams["~AUTH_RESULT"]);?>
            </div>
            <div class="auth-form__row">
              <input class="auth-form__input bx-auth-input" type="text" placeholder="<?=GetMessage("AUTH_LOGIN")?>" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
            </div>
            <div class="auth-form__row">
              <input class="auth-form__input bx-auth-input" type="text" placeholder="<?=GetMessage("AUTH_EMAIL")?>" name="USER_EMAIL" maxlength="255" />
            </div>
            <div class="auth-form__row">
              <input class="auth-form__submit" type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
            </div>


            <a class="auth-form__forgot" href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_AUTH")?></a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  document.bform.USER_LOGIN.focus();
</script>
