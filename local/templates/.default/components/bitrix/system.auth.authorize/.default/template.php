<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<section class="auth-page">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-12 auth-page__wrap">
        <form class="auth-page__form auth-form" name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
          <strong class="auth-form__title"><?=GetMessage("AUTH_FORM_TITLE")?></strong>
          <input type="hidden" name="AUTH_FORM" value="Y" />
          <input type="hidden" name="TYPE" value="AUTH" />
          <?if (strlen($arResult["BACKURL"]) > 0):?>
          <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
          <?endif?>
          <?foreach ($arResult["POST"] as $key => $value):?>
          <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
          <?endforeach?>
          <div class="auth-form__wrap">
            <div class="auth-form__row auth-form__row_text">
              <?ShowMessage($arResult['ERROR_MESSAGE']);?>
              <?ShowMessage($arParams["~AUTH_RESULT"]);?>
            </div>
            <div class="auth-form__row">
              <input class="auth-form__input bx-auth-input" type="text" name="USER_LOGIN" maxlength="255" placeholder="<?=GetMessage("AUTH_LOGIN")?>" value="<?=$arResult["LAST_LOGIN"]?>" />
            </div>
            <div class="auth-form__row">
              <input class="auth-form__input bx-auth-input" type="password" name="USER_PASSWORD"  placeholder="<?=GetMessage("AUTH_PASSWORD")?>" maxlength="255" autocomplete="off" />
            </div>
            <div class="auth-form__row">
              <input class="auth-form__submit" type="submit" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" />
            </div>
            <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
              <a class="auth-form__forgot" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
            <?endif?>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  <?if (strlen($arResult["LAST_LOGIN"])>0):?>
  try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
  <?else:?>
  try{document.form_auth.USER_LOGIN.focus();}catch(e){}
  <?endif?>
</script>
