<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';

CModule::IncludeModule("hino.pricelist");

use Bitrix\Main\Localization\Loc;
use Hino\PriceList;
use Bitrix\Main\Web\Json;

Loc::loadMessages(__FILE__);

if(!$USER->IsAdmin()){
  $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

@set_time_limit(0);


if($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["remove"]=="Y" && $USER->GetID() == 1){
  require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
  $priceList = new PriceList();
  $result = $priceList->removeAllPart();
  echo Json::encode(
    array(
      "result" => $result
    )
  );
  require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");
}

if($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["import"]=="Y"){
  require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");

  $arResult = array();

  $filename  = trim(str_replace("\\", "/", trim($_REQUEST["path"])), "/");
  $FILE_NAME = $_SERVER["DOCUMENT_ROOT"]."/".$filename;

  if (($handle_f = fopen($FILE_NAME, "r")) !== FALSE){
    $priceList = new PriceList();

    if(isset($_REQUEST['ftell'])){
      fseek($handle_f, $_REQUEST['ftell']);
    }

    $stringItem = 0;
    $loadItem   = isset($_REQUEST['load_item'])? intval($_REQUEST['load_item']): 0;

    // построчное считывание и анализ строк из файла
    while ( ($data_f = fgetcsv($handle_f, 10000, ";")) !== false) {
      $priceList->addPart(array(
        "NEWESTPRT" => str_replace("-", "", $data_f[0]),
        "DESCR"     => $data_f[1],
        "SUNITPR"   => $data_f[2],
        "WEIGHT"    => $data_f[3],
        "LENGTHCM"  => $data_f[4],
        "WIDTHCM"   => $data_f[5],
        "HEIGHTCM"  => $data_f[6]
      ));

      $stringItem++;
      $loadItem++;

      if($stringItem == 500){
        echo Json::encode(
          array(
            "text"      => Loc::getMessage("HINO_PRICELIST_FILE_LOAD", array("#COUNT#" => $loadItem)),
            "load_item" => $loadItem,
            "ftell"     => ftell($handle_f),
            "action"    => "next"
          )
        );
        fclose($handle_f);
        exit;
      }
    }
    fclose($handle_f);
  }else{
    $arResult["text"] = Loc::getMessage("HINO_PRICELIST_FILE_ERROR");
  }
  echo Json::encode(array_merge($arResult, array(
    "action" => "stop",
    "text"   => Loc::getMessage("HINO_PRICELIST_FILE_LOAD_FINAL"),
  )));
  require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");
}
?>


<?
// публтчная часть
$APPLICATION->SetTitle(Loc::getMessage("HINO_PRICELIST_LOAD_TITLE"));
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';
?>

<div class="adm-info-message-wrap">
  <div class="adm-info-message" style="width: 100%; box-sizing: border-box;">
    <div class="adm-info-message-title" id="hino_import_result_div">
    </div>
  </div>
</div>

<?
$arTabs = array(
  array(
    "DIV"   => "load",
    "TAB"   => Loc::getMessage("HINO_PRICELIST_TAB"),
    "TITLE" => Loc::getMessage("HINO_PRICELIST_TAB_TITLE"),
  )
);
if($USER->GetID() == 1){
  $arTabs[] = array(
    "DIV"   => "remove",
    "TAB"   => Loc::getMessage("HINO_PRICELIST_TAB_REMOVE"),
    "TITLE" => Loc::getMessage("HINO_PRICELIST_TAB_TITLE_REMOVE"),
  );
}

$tabControl = new CAdminTabControl("tabControl", $arTabs, true, true);
$tabControl->Begin();
$tabControl->BeginNextTab();
?>
<div class="adm-info-message-wrap adm-info-message-red">
  <div class="adm-info-message" style="width: 100%; box-sizing: border-box;">
    <div class="adm-info-message-title">
      Внимание! Формат таблицы Exel перед сохранением в CSV(разделитель точка запятая, без заголовков)<br/><br/>
      <img src="/local/modules/hino.pricelist/admin/img/mess-img.jpg" alt="">
    </div>
    <div class="adm-info-message-icon"></div>
  </div>
</div>

<script language="JavaScript" type="text/javascript">
var hinoImportPriceList = {
  startButton: false,
  running: false,
  ftell: 0,
  loadItem: 0,
  doNext: function(){
    if(this.running){
      var _this = this;
      BX.ajax.post(
        '<?=$APPLICATION->GetCurPage()?>',
        {
          path:      document.getElementById('URL_DATA_FILE').value,
          ftell:     _this.ftell,
          load_item: _this.loadItem,
          import:    'Y'
        },
        function(result){
          var result = JSON.parse(result);
          document.getElementById('hino_import_result_div').innerHTML = result.text;

          if(result.action == 'next'){
            _this.ftell    = result.ftell;
            _this.loadItem = result.load_item;
            _this.doNext();
          }else{
            _this.endImport();
          }
        }
      );
    }
  },
  startImport: function(){
    BX.showWait();
    this.running = this.startButton.disabled = true;
    this.doNext();
  },
  endImport: function(){
    BX.closeWait();
    this.running = this.startButton.disabled = false;
  },
  init: function(){
    this.startButton = document.getElementById('start_button');
  },
  reset: function(){
    this.ftell = 0;
    this.loadItem = 0;
  }

};
</script>

<form method="POST" action="<?=$APPLICATION->GetCurPage()?>?lang=<?echo htmlspecialcharsbx(LANG)?>" onload="hinoImportPriceList.init()" name="form_load_price" id="form_load_price">
  <tr class="adm-detail-required-field">
    <td width="40%"><?echo Loc::GetMessage("HINO_PRICELIST_URL_DATA_FILE")?>:</td>
    <td width="60%">
      <input type="text" id="URL_DATA_FILE" name="URL_DATA_FILE" size="30" onchange="hinoImportPriceList.reset()" value="<?=htmlspecialcharsbx($URL_DATA_FILE)?>">
      <input type="button" value="<?echo Loc::GetMessage("HINO_PRICELIST_OPEN")?>" OnClick="BtnClick()">
      <?
      CAdminFileDialog::ShowScript
      (
        Array(
          "event"            => "BtnClick",
          "arResultDest"     => array("FORM_NAME" => "form_load_price", "FORM_ELEMENT_NAME" => "URL_DATA_FILE"),
          "arPath"           => array("SITE" => SITE_ID, "PATH" =>"/upload"),
          "select"           => 'F',// F - file only, D - folder only
          "operation"        => 'O',
          "showUploadTab"    => true,
          "showAddToMenuTab" => false,
          "fileFilter"       => 'csv',
          "allowAllFiles"    => true,
          "SaveConfig"       => true,
        )
      );
      ?>
    </td>
  </tr>
  <tr class="adm-detail-required-field">
    <td>
      <input type="button" id="start_button" value="<?echo Loc::GetMessage("HINO_PRICELIST_START_IMPORT")?>" OnClick="hinoImportPriceList.startImport();" class="adm-btn-save">
    </td>
  </tr>
  <?$tabControl->EndTab();?>
</form>

<?if($USER->GetID() == 1):?>
<script type="text/javascript">
  hinoImportPriceList.remove = function(){
    BX.showWait();
    BX.ajax.post(
      '<?=$APPLICATION->GetCurPage()?>',
      {
        remove:    'Y'
      },
      function(result){
        BX.closeWait();
      }
    );
  };
</script>
  <?$tabControl->BeginNextTab();?>
  <div class="adm-info-message-wrap adm-info-message-red">
    <div class="adm-info-message" style="width: 100%; box-sizing: border-box;">
      <div class="adm-info-message-title">
        Внимание! Удалит полностью все данные
      </div>
      <div class="adm-info-message-icon"></div>
    </div>
  </div>
  <input type="button" id="remove_button" value="<?echo Loc::GetMessage("HINO_PRICELIST_REMOVE")?>" OnClick="hinoImportPriceList.remove();" class="adm-btn-save">

<?endif;?>

<?$tabControl->End();?>


<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';?>
