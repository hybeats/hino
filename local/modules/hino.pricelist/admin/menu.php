<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$menu = array(
  array(
    'parent_menu' => 'global_menu_content',
    'sort'        => 400,
    'text'        => Loc::getMessage('HINO_PRICELIST_MENU_TITLE'),
    'title'       => Loc::getMessage('HINO_PRICELIST_MENU_TITLE'),
    'section'     => 'hino',
    'items_id'    => 'menu_hino',
    'url'         => 'hino_pricelist_index.php?lang=' . LANGUAGE_ID,
    'items'       => array(),
  ),
);

return $menu;
