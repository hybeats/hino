<?php
$MESS['HINO_PRICELIST_LOAD_TITLE'] = 'Импорт прайс листа';
$MESS['HINO_PRICELIST_TAB'] = 'Импорт';
$MESS['HINO_PRICELIST_TAB_TITLE'] = 'Импорт CSV';
$MESS['HINO_PRICELIST_REMOVE'] = 'Удалить все записи';

$MESS['HINO_PRICELIST_START_IMPORT'] = 'Импортировать';
$MESS['HINO_PRICELIST_URL_DATA_FILE'] = 'Путь до файла';
$MESS['HINO_PRICELIST_OPEN'] = 'Открыть';
$MESS['ACCESS_DENIED'] = 'Нет прав';

$MESS['HINO_PRICELIST_FILE_ERROR'] = 'Ошибка открытия файла';
$MESS['HINO_PRICELIST_FILE_LOAD'] = 'Загружено #COUNT# записей';
$MESS['HINO_PRICELIST_FILE_LOAD_FINAL'] = 'Файл загружен!!!';

$MESS['HINO_PRICELIST_TAB_REMOVE']       = 'Удалить';
$MESS['HINO_PRICELIST_TAB_TITLE_REMOVE'] = 'Удаление всех запчастей';
