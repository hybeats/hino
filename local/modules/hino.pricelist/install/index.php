<?php
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class hino_pricelist extends CModule
{
  public function __construct()
  {
    $arModuleVersion = array();

    include __DIR__ . '/version.php';

    if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
    {
      $this->MODULE_VERSION      = $arModuleVersion['VERSION'];
      $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
    }

    $this->MODULE_ID           = 'hino.pricelist';
    $this->MODULE_NAME         = Loc::getMessage('HINO_PRICELIST_MODULE_NAME');
    $this->MODULE_DESCRIPTION  = Loc::getMessage('HINO_PRICELIST_MODULE_DESCRIPTION');
    $this->MODULE_GROUP_RIGHTS = 'N';
    $this->PARTNER_NAME        = Loc::getMessage('HINO_PRICELIST_MODULE_PARTNER_NAME');
    $this->PARTNER_URI         = 'http://supportix.ru';
  }

  public function doInstall()
  {
    ModuleManager::registerModule($this->MODULE_ID);

    $this->installFiles();
  }

  public function doUninstall()
  {
    $this->uninstallFiles();
    ModuleManager::unRegisterModule($this->MODULE_ID);
  }

  function installFiles()
  {
    copyDirFiles(
      $_SERVER['DOCUMENT_ROOT'] . '/local/modules/hino.pricelist/install/admin',
      $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin',
      true, true
    );
  }

  function uninstallFiles()
  {
    deleteDirFiles(
      $_SERVER['DOCUMENT_ROOT'] . '/local/modules/hino.pricelist/install/admin',
      $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin'
    );

    return true;
  }
}
