<?php
namespace Hino;

\Bitrix\Main\Loader::includeModule('highloadblock');

use Bitrix\Main\Localization\Loc;
use Bitrix\Highloadblock\HighloadBlockTable;

Loc::loadMessages(__FILE__);


class PriceList
{
  private $hlBlockID = 2;

  function __construct(){

    $hlblock = HighloadBlockTable::getById($this->hlBlockID)->fetch();
    $this->tableName = $hlblock['TABLE_NAME'];

    $entity  = HighloadBlockTable::compileEntity($hlblock);

    $this->entityDataClass = $entity->getDataClass();
  }

  public function addPart($data)
  {
    $entity_data_class = $this->entityDataClass;

    $result = $entity_data_class::add(array(
      "UF_NEWESTPRT" => $data["NEWESTPRT"],
      "UF_DESCR"     => $data["DESCR"],
      "UF_SUNITPR"   => $data["SUNITPR"],
      "UF_WEIGHT"    => $data["WEIGHT"],
      "UF_LENGTHCM"  => $data["LENGTHCM"],
      "UF_WIDTHCM"   => $data["WIDTHCM"],
      "UF_HEIGHTCM"  => $data["HEIGHTCM"]
    ));
  }

  public function removeAllPart($data){
    global $DB;

    $res = $DB->Query("TRUNCATE TABLE `".$this->tableName."`", false, $err_mess.__LINE__);
    return $res->result;
  }

  public function searchFirsPart($article){
    $entity_data_class = $this->entityDataClass;

    $sTableID = 'tbl_'.$entity_table_name;
    $rsData = $entity_data_class::getList(
      array(
        "select" => array('*'),
        "filter" => array("UF_NEWESTPRT" => trim($article))
      )
    );

    $rsData = new \CDBResult($rsData, $sTableID);
    if($arRes = $rsData->Fetch()){
      return $arRes;
    }else {
      return false;
    }
  }
}
