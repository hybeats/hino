$(document).ready(function() {
  //user page documents slider
  new Swiper ('.swiper-container', {
    direction: 'horizontal',
    slidesPerView: 5,
    prevButton: ".user-page__prev",
    nextButton: ".user-page__next",
    breakpoints: {
      1000: {
        slidesPerView: 4
      },
      640: {
        slidesPerView: 2
      },
      480: {
        slidesPerView: 1
      }
    }
  });
});
