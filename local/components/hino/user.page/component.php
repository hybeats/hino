<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Iblock;

$APPLICATION->SetAdditionalCSS($this->GetPath()."/css/style.css");
$APPLICATION->AddHeadScript($this->GetPath()."/js/script.js");

CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
$hu = new HinoUsers();
$hd = new HinoContent();
$userID = $arParams["USER_ID"];
$arUser = $hu->getUserArray($userID);
$arGroups = CUser::GetUserGroup($userID);

if (in_array(DEALERS_GROUP_ID, $arGroups) || $arParams["DEALER_ID"]) {
  $arDealer = $hd->getDealerByParams($userID, $arParams["DEALER_ID"]);
  if(!empty($arDealer) && (HinoUsers::isAdmin() || in_array($userID, $arDealer["PROPS"]["EMPLOYEE"]["VALUE"]))){
    $arResult["LOGO"] = ($arDealer["FIELDS"]["PREVIEW_PICTURE"])? CFile::GetPath($arDealer["FIELDS"]["PREVIEW_PICTURE"]): "/images/no-logo.jpg";
    $arResult["LEGAL"]["NAME"] = $arDealer["FIELDS"]["NAME"];
    $arResult["LEGAL"]["ADDRESS"] = $arDealer["FIELDS"]["PROPERTY_3"];
    $arResult["LEGAL"]["PHONE"] = $arDealer["FIELDS"]["PROPERTY_6"];
    $arResult["LEGAL"]["INN"] = $arDealer["PROPS"]["INN"];
    $arResult["LEGAL"]["KPP"] = $arDealer["PROPS"]["KPP"];

    if ($arDealer["PROPS"]["DELIVERY_ADDRESS_1"]["VALUE"]) {
      $arResult["DELIVERY_ADDRESS"]["DELIVERY_ADDRESS_1"] = $arDealer["PROPS"]["DELIVERY_ADDRESS_1"]["VALUE"];
    }
    if ($arDealer["PROPS"]["DELIVERY_ADDRESS_2"]["VALUE"]) {
      $arResult["DELIVERY_ADDRESS"]["DELIVERY_ADDRESS_2"] = $arDealer["PROPS"]["DELIVERY_ADDRESS_2"]["VALUE"];
    }

    // получение данных по персональному менеджеру
    $Manager = CUser::GetByID($arDealer["PROPS"]["MANAGER"]["VALUE"]);
    $arManager = $Manager->Fetch();
    $arResult["PERSONAL_MANAGER"] = $arManager;

    $arResult["AGREEMENTS"] = $hd->getDocsByDealerCode($arDealer["FIELDS"]["CODE"]);
    $arResult["EMPLOYEES"] = $hu->getUsersByIdFilter($arDealer["PROPS"]["EMPLOYEE"]["VALUE"]);
  }else{
    $this->abortResultCache();
    Iblock\Component\Tools::process404("", true, true, true);
  }
} else {
  $arResult["NAME"] = $arUser["LAST_NAME"]." ".$arUser["NAME"]." ".$arUser["SECOND_NAME"];
  $arResult["EMAIL"] = $arUser["EMAIL"];
  $arResult["PHONE"] = $arUser["PERSONAL_PHONE"];
}
$arResult["DOCS_TEMPLATES"] = $hd->getDocsTemplates(IBLOCK_DOCS_TEMPLATES);

$this->IncludeComponentTemplate();
?>
