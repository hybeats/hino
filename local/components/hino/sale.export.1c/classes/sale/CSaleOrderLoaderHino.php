<?
set_time_limit(100);

CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");

class CSaleOrderLoaderHino extends CSaleOrderLoader
{
  function writeError($mess) {
    $mess = date("Y-m-d H:i:s")."\n".$mess;
    $mess .= "\n--------------------------------------------------------------";
    Bitrix\Main\Diag\Debug::writeToFile($mess, '', "/_logs/integration1c/".date("j_F_Y")."_import.log");
  }

  function updateDealer($arAgentInfo, $mode)
  {
    $PROPERTY_ID = 5; // id свойства Регион REGION
    $property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID" => IBLOCK_DEALERS, "PROPERTY_ID" => $PROPERTY_ID));
    while($enum_fields = $property_enums->GetNext())
    {
      $regionList[$enum_fields["VALUE"]] = $enum_fields["ID"];
    }

    if (array_key_exists($arAgentInfo["ORDER_PROPS"]["REGION"], $regionList)) {
      $idRegion = $regionList[$arAgentInfo["ORDER_PROPS"]["REGION"]];
    } else {
      $ibpenum = new CIBlockPropertyEnum;
      $idRegion = $ibpenum->Add(
        Array(
          "PROPERTY_ID" => $PROPERTY_ID,
          "VALUE"       => $arAgentInfo["ORDER_PROPS"]["REGION"]
        )
      );
    }

    $el = new CIBlockElement;

    $propDealer = array(
      "INN"                => $arAgentInfo["ORDER_PROPS"]["INN"],
      "KPP"                => $arAgentInfo["ORDER_PROPS"]["KPP"],
      "F_ADDRESS_FULL"     => $arAgentInfo["ORDER_PROPS"]["F_ADDRESS_FULL"],
      "ADDRESS_FULL"       => $arAgentInfo["ORDER_PROPS"]["ADDRESS_FULL"],
      "REGION"             => $idRegion,
      "PHONE"              => $arAgentInfo["ORDER_PROPS"]["PHONE"],
      "EMAIL"              => $arAgentInfo["ORDER_PROPS"]["EMAIL"],
      "CONTRAGENT_ID"      => $arAgentInfo["USER_ID"],
      "DELIVERY_ADDRESS_1" => $arAgentInfo["AGENT"]["DELIVERY_ADDRESS_1"]["PRESENTATION"],
      "DELIVERY_ADDRESS_2" => $arAgentInfo["AGENT"]["DELIVERY_ADDRESS_2"]["PRESENTATION"]
    );

    if($mode == "add") {
      $propDealer["STATUS"] = array("VALUE" => 9); //статус Активен
      $dealerID = $el->Add(
        array(
          "IBLOCK_ID"       => IBLOCK_DEALERS,
          "NAME"            => ($arAgentInfo["ORDER_PROPS"]["OFICIAL_NAME"])? $arAgentInfo["ORDER_PROPS"]["OFICIAL_NAME"]: $arAgentInfo["ORDER_PROPS"]["NAME"],
          "CODE"            => $arAgentInfo["ORDER_PROPS"]["ID"],
          "PROPERTY_VALUES" => $propDealer
        )
      );

      if(!$dealerID) {
        $this->logMessage("Новый дилер Error: ".$el->LAST_ERROR);
      }
    } elseif ($mode == "update") {

      $res = CIBlockElement::GetList(
        array(),
        array("IBLOCK_ID" => IBLOCK_DEALERS, "ACTIVE" => "Y", "CODE" => $arAgentInfo["ORDER_PROPS"]["ID"]),
        false,
        false,
        array("ID")
      );

      $dealerOb = $res->GetNext();
      if($dealerID = $dealerOb["ID"]) {
        CIBlockElement::SetPropertyValuesEx($dealerID, false, $propDealer);

        if(!$resultUpdate) {
          $this->logMessage("Обновление дилер Error: ".$el->LAST_ERROR);
        }
      }
    }
  }

  function getUserByProperty($arOrder)
  {
    if(!empty($arOrder["AGENT"]) && strlen($arOrder["AGENT"]["ID"]) > 0)
    {
      $arOrder["PERSON_TYPE_ID"] = 0;
      $arOrder["USER_ID"] = 0;
      $arErrors = array();
      $dbUProp = CSaleOrderUserProps::GetList(array(), array("XML_ID" => $arOrder["AGENT"]["ID"]), false, false, array("ID", "NAME", "USER_ID", "PERSON_TYPE_ID", "XML_ID", "VERSION_1C"));
      if ($arUProp = $dbUProp->Fetch()) {
        $arOrder["USER_ID"] = $arUProp["USER_ID"];
        $arOrder["PERSON_TYPE_ID"] = $arUProp["PERSON_TYPE_ID"];
        $arOrder["USER_PROFILE_ID"] = $arUProp["ID"];
        $arOrder["USER_PROFILE_VERSION"] = $arUProp["VERSION_1C"];

        $dbUPropValue = CSaleOrderUserPropsValue::GetList(array(), array("USER_PROPS_ID" => $arUProp["ID"]));
        while ($arUPropValue = $dbUPropValue->Fetch()) {
          $arOrder["USER_PROPS"][$arUPropValue["ORDER_PROPS_ID"]] = $arUPropValue["VALUE"];
        }
      } else {
        if (strlen($arOrder["AGENT"]["ID"]) > 0) {
          $arAI = explode("#", $arOrder["AGENT"]["ID"]);
          if (IntVal($arAI[0]) > 0) {
            $dbUser = CUser::GetByID($arAI[0]);
            if ($arU = $dbUser->Fetch()) {
              if (htmlspecialcharsback(substr(htmlspecialcharsbx($arU["ID"] . "#" . $arU["LOGIN"] . "#" . $arU["LAST_NAME"] . " " . $arU["NAME"] . " " . $arU["SECOND_NAME"]), 0, 80)) == $arOrder["AGENT"]["ID"]) {
                $arOrder["USER_ID"] = $arU["ID"];
              }
            }
          }
        }

        if (IntVal($arOrder["USER_ID"]) <= 0) {
          //create new user
          $arUser = array(
              "NAME" => $arOrder["AGENT"]["ITEM_NAME"],
              "EMAIL" => $arOrder["AGENT"]["CONTACT"]["MAIL_NEW"],
          );

          if (strlen($arUser["NAME"]) <= 0)
            $arUser["NAME"] = $arOrder["AGENT"]["CONTACT"]["CONTACT_PERSON"];

          $emServer = $_SERVER["SERVER_NAME"];
          if(strpos($_SERVER["SERVER_NAME"], ".") === false)
            $emServer .= ".bx";

          if (strlen($arUser["EMAIL"]) <= 0)
            $arUser["EMAIL"] = "buyer" . time() . GetRandomCode(2) . "@" . $emServer;

          $arOrder["USER_ID"] = CSaleUser::DoAutoRegisterUser($arUser["EMAIL"], $arUser["NAME"], $this->getSiteId(), $arErrors, array("XML_ID"=>$arOrder["AGENT"]["ID"]));
          $this->updateDealer($arOrder, "add");
        }
      }
    }
    return $arOrder;
  }

  function collectDocumentInfo($value)
  {
    $bNeedFull = false;
    $arOrder = array();

    $arOrder["ORDER_ID"] = false;

    $arOrder["OPERATION"] = $value["#"][GetMessage("CC_BSC1_OPERATION")][0]["#"];

    self::setOperationType($value["#"][GetMessage("CC_BSC1_OPERATION")][0]["#"]);

    $arOrder["OPERATION_TYPE"] = self::$operationType ;

    $this->documentType = self::setDocumentType(self::$operationType);

    if(in_array(
        $this->documentType,
        array(
            'shipment',
            'payment',
            'order'
        ))
    )
    {
      $arOrder["ID"] = $value["#"][GetMessage("CC_BSC1_NUMBER")][0]["#"];
      $arOrder["XML_1C_DOCUMENT_ID"] = $value["#"][GetMessage("CC_BSC1_ID")][0]["#"];

      switch ($this->documentType)
      {
        case 'payment':

          $paymentResult = $this->getDocumentId('Payment', $arOrder["ID"], $arOrder["XML_1C_DOCUMENT_ID"]);

          if(isset($paymentResult['ID']))
            $arOrder['ID'] = $paymentResult['ID'];
          if(isset($paymentResult['ORDER_ID']))
            $arOrder['PAYMENT_ORDER_ID'] = $paymentResult['ORDER_ID'];

          $arOrder["ORDER_ID_ORIG"] = $value["#"][GetMessage("CC_BSC1_NUMBER_BASE")][0]["#"];
          $arOrder["ORDER_ID"] = $value["#"][GetMessage("CC_BSC1_NUMBER_BASE")][0]["#"];

          $arOrder["ORDER_ID"] = $this->getOrderIdByDocument($arOrder["ORDER_ID"]);

          break;
        case 'shipment':

          $shipmentResult = $this->getDocumentId('Shipment', $arOrder["ID"], $arOrder["XML_1C_DOCUMENT_ID"]);

          if(isset($shipmentResult['ID']))
            $arOrder['ID'] = $shipmentResult['ID'];
          if(isset($shipmentResult['ORDER_ID']))
            $arOrder['SHIPMENT_ORDER_ID'] = $shipmentResult['ORDER_ID'];

          $arOrder["ORDER_ID_ORIG"] = $value["#"][GetMessage("CC_BSC1_NUMBER_BASE")][0]["#"];
          $arOrder["ORDER_ID"] = $value["#"][GetMessage("CC_BSC1_NUMBER_BASE")][0]["#"];

          $arOrder["ORDER_ID"] = $this->getOrderIdByDocument($arOrder["ORDER_ID"]);

          break;
        case 'order':

          $orderResult = $this->getDocumentId('Order', $arOrder["ID"], $arOrder["XML_1C_DOCUMENT_ID"]);
          if(isset($orderResult['ID']))
            $arOrder['ID'] = $orderResult['ID'];

          //if ($accountNumberPrefix != "")
          //  $arOrder["ID"] = substr($arOrder["ID"], strlen($accountNumberPrefix));
          break;
      }

      $arOrder["AMOUNT"] = $value["#"][GetMessage("CC_BSC1_SUMM")][0]["#"];
      $arOrder["AMOUNT"] = $this->ToFloat($arOrder["AMOUNT"]);

      $arOrder["COMMENT"] = $value["#"][GetMessage("CC_BSC1_COMMENT")][0]["#"];

      $arOrder["CANCELED"] = $value["#"][GetMessage("CC_BSC1_CANCELED")][0]["#"];
      $arOrder["VERSION_1C"] = $value["#"][GetMessage("CC_BSC1_VERSION_1C")][0]["#"];
      $arOrder["ID_1C"] = $value["#"][GetMessage("CC_BSC1_ID_1C")][0]["#"];
      $arOrder["DATE"] = $value["#"][GetMessage("CC_BSC1_1C_DATE")][0]["#"];

      $arOrder["OPERATION"] = $value["#"][GetMessage("CC_BSC1_OPERATION")][0]["#"];
      $arOrder["TRAITS"] = array();

      switch ($this->documentType)
      {
        case 'order':
          //if (strlen($arOrder["ID"]) <= 0 && strlen($arOrder["ID_1C"]) > 0)
            $bNeedFull = true;
          break;
        case 'shipment':
          $bNeedFull = true;
          break;
      }

      if (is_array($value["#"][GetMessage("CC_BSC1_REK_VALUES")][0]["#"][GetMessage("CC_BSC1_REK_VALUE")]) && !empty($value["#"][GetMessage("CC_BSC1_REK_VALUES")][0]["#"][GetMessage("CC_BSC1_REK_VALUE")])) {
        foreach ($value["#"][GetMessage("CC_BSC1_REK_VALUES")][0]["#"][GetMessage("CC_BSC1_REK_VALUE")] as $val) {
          $arOrder["TRAITS"][$val["#"][GetMessage("CC_BSC1_NAME")][0]["#"]] = $val["#"][GetMessage("CC_BSC1_VALUE")][0]["#"];
        }
      }

      $taxValue = 0;
      $taxName = "";
      $arOrder["items"] = array();
      $basketItems = array();
      if (is_array($value["#"][GetMessage("CC_BSC1_ITEMS")][0]["#"]) && is_array($value["#"][GetMessage("CC_BSC1_ITEMS")][0]["#"][GetMessage("CC_BSC1_ITEM")])) {
        foreach ($value["#"][GetMessage("CC_BSC1_ITEMS")][0]["#"][GetMessage("CC_BSC1_ITEM")] as $val) {
          $val = $val["#"];
          if(is_array($val))
          {
            $productID = $val[GetMessage("CC_BSC1_ID")][0]["#"];
            $discountValue = $this->ToFloat($val[GetMessage("CC_BSC1_DISCOUNTS")][0]["#"][GetMessage("CC_BSC1_DISCOUNT")][0]["#"][GetMessage("CC_BSC1_DISCOUNT_PERCENT")][0]["#"]);

            $discountPrice = "";
            $priceAll = $this->ToFloat($val[GetMessage("CC_BSC1_SUMM")][0]["#"]);
            $priceone = $this->ToFloat($val[GetMessage("CC_BSC1_PRICE_PER_UNIT")][0]["#"]);
            if (DoubleVal($priceone) <= 0)
              $priceone = $this->ToFloat($val[GetMessage("CC_BSC1_PRICE_ONE")][0]["#"]);

            $quantity = $this->ToFloat($val[GetMessage("CC_BSC1_QUANTITY")][0]["#"]);
            if (doubleval($quantity) > 0) {
              $price = roundEx($priceone*(1 - $discountValue/100), 4);
              $priceone = roundEx($priceone, 4);

              if ($priceone != $price)
                $discountPrice = DoubleVal($priceone - $price);


              //DISCOUNTS!
              $basketItems = Array(
                  "NAME" => $val[GetMessage("CC_BSC1_NAME")][0]["#"],
                  "PRICE" => $price,
                  "PRICE_ONE" => $priceone,
                  "QUANTITY" => $quantity,
                  "DISCOUNT_PRICE" => $discountPrice,
                  "PRICE_DELIVERY_ONE" => $priceAll - $price*$quantity,
              );


              if (is_array($val[GetMessage("CC_BSC1_ITEM_UNIT")]) && is_array($val[GetMessage("CC_BSC1_ITEM_UNIT")][0]["#"])) {
                $basketItems["MEASURE_CODE"] = $val[GetMessage("CC_BSC1_ITEM_UNIT")][0]["#"][GetMessage("CC_BSC1_ITEM_UNIT_CODE")][0]["#"];
                $basketItems["MEASURE_NAME"] = $val[GetMessage("CC_BSC1_ITEM_UNIT")][0]["#"][GetMessage("CC_BSC1_ITEM_UNIT_NAME")][0]["#"];
              }

              if (is_array($val[GetMessage("CC_BSC1_REK_VALUES")][0]["#"][GetMessage("CC_BSC1_REK_VALUE")])) {
                foreach ($val[GetMessage("CC_BSC1_REK_VALUES")][0]["#"][GetMessage("CC_BSC1_REK_VALUE")] as $val1) {
                  if ($val1["#"][GetMessage("CC_BSC1_NAME")][0]["#"] == GetMessage("CC_BSC1_ITEM_TYPE"))
                  {
                    $basketItems["TYPE"] = $val1["#"][GetMessage("CC_BSC1_VALUE")][0]["#"];
                  }
                  elseif (strpos($val1["#"][GetMessage("CC_BSC1_NAME")][0]["#"], GetMessage("CC_BSC1_PROP_BASKET")."#") === 0)
                  {
                    $markerPosition = strpos($val1["#"][GetMessage("CC_BSC1_NAME")][0]["#"], GetMessage("CC_BSC1_PROP_BASKET")."#");
                    $idBasketProperty = substr($val1["#"][GetMessage("CC_BSC1_NAME")][0]["#"], $markerPosition + strlen(GetMessage("CC_BSC1_PROP_BASKET")."#"));
                    $basketItems["ATTRIBUTES"][$idBasketProperty] = $val1["#"][GetMessage("CC_BSC1_VALUE")][0]["#"];
                  }

                }
              }

              if (strlen($value["#"][GetMessage("CC_BSC1_TAXES")][0]["#"][GetMessage("CC_BSC1_TAX")][0]["#"][GetMessage("CC_BSC1_NAME")][0]["#"]) > 0) {
                $taxValueTmp = $val[GetMessage("CC_BSC1_TAXES")][0]["#"][GetMessage("CC_BSC1_TAX")][0]["#"][GetMessage("CC_BSC1_TAX_VALUE")][0]["#"];
                $basketItems["VAT_RATE"] = $taxValueTmp / 100;

                if (IntVal($taxValueTmp) > IntVal($taxValue)) {
                  $taxName = $val[GetMessage("CC_BSC1_TAXES")][0]["#"][GetMessage("CC_BSC1_TAX")][0]["#"][GetMessage("CC_BSC1_NAME")][0]["#"];
                  $taxValue = $taxValueTmp;
                }
              }
            }

            if (self::getVersionSchema() >= self::PARTIAL_VERSION)
              $arOrder["items"][][$productID] = $basketItems;
            else
              $arOrder["items"][$productID] = $basketItems;
          }
        }
      }

      if(IntVal($taxValue)>0)
      {
        $price = $this->ToFloat($value["#"][GetMessage("CC_BSC1_TAXES")][0]["#"][GetMessage("CC_BSC1_TAX")][0]["#"][GetMessage("CC_BSC1_SUMM")][0]["#"]);
        $arOrder["TAX"] = Array(
            "NAME" => $taxName,
            "VALUE" =>$taxValue,
            "IS_IN_PRICE" => ($value["#"][GetMessage("CC_BSC1_TAXES")][0]["#"][GetMessage("CC_BSC1_TAX")][0]["#"][GetMessage("CC_BSC1_IN_PRICE")][0]["#"]=="true"?"Y":"N"),
            "VALUE_MONEY" => $price,
        );
      }

      if($bNeedFull)
      {
        IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/sale/general/export.php");
        $arOrder["DATE"] = $value["#"][GetMessage("CC_BSC1_1C_DATE")][0]["#"];
        $arOrder["TIME"] = $value["#"][GetMessage("CC_BSC1_1C_TIME")][0]["#"];

        if(!empty($value["#"][GetMessage("SALE_EXPORT_CONTRAGENTS")][0]["#"]))
        {
          $arAgentInfo = $this->collectAgentInfo($value["#"][GetMessage("SALE_EXPORT_CONTRAGENTS")][0]["#"][GetMessage("SALE_EXPORT_CONTRAGENT")][0]["#"]);
          $arOrder["AGENT"] = $arAgentInfo["AGENT"];
          $arOrder["ORDER_PROPS"] = $arAgentInfo["ORDER_PROPS"];

          if(strlen($arOrder["TRAITS"][GetMessage("SALE_EXPORT_DELIVERY_ADDRESS")]) > 0)
          {
            if(!empty($arOrder["AGENT"]["REGISTRATION_ADDRESS"]))
              $arOrder["AGENT"]["REGISTRATION_ADDRESS"]["PRESENTATION"] = $arOrder["TRAITS"][GetMessage("SALE_EXPORT_DELIVERY_ADDRESS")];
            if(!empty($arOrder["AGENT"]["ADDRESS"]))
              $arOrder["AGENT"]["ADDRESS"]["PRESENTATION"] = $arOrder["TRAITS"][GetMessage("SALE_EXPORT_DELIVERY_ADDRESS")];
          }
        }
      }
    }
    return $arOrder;
  }

  function collectAgentInfo($data = array())
  {
    if(empty($data))
      return false;
    IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/sale/general/export.php");

    $result = array();
    $schema = array("ID", "VERSION", "ITEM_NAME", "OFICIAL_NAME", "FULL_NAME", "INN", "KPP", "OKPO_CODE", "EGRPO", "OKVED", "OKDP", "OKOPF", "OKFC", "OKPO",
        "REGISTRATION_ADDRESS" => array("PRESENTATION", "POST_CODE", "COUNTRY", "REGION", "STATE", "SMALL_CITY", "CITY", "STREET", "HOUSE", "BUILDING", "FLAT"),
        "UR_ADDRESS" => array("PRESENTATION", "POST_CODE", "COUNTRY", "REGION", "STATE", "SMALL_CITY", "CITY", "STREET", "HOUSE", "BUILDING", "FLAT"),
        "ADDRESS" => array("PRESENTATION", "POST_CODE", "COUNTRY", "REGION", "STATE", "SMALL_CITY", "CITY", "STREET", "HOUSE", "BUILDING", "FLAT"),
        "CONTACTS" => array("CONTACT" => array("WORK_PHONE_NEW", "MAIL_NEW")),
        "REPRESENTATIVES" => array("REPRESENTATIVE" => array("CONTACT_PERSON")),
        "DELIVERY_ADDRESS_1" => array("PRESENTATION"),
        "DELIVERY_ADDRESS_2" => array("PRESENTATION"),
    );

    foreach($schema as $k => $v)
    {
      if(is_array($v))
      {
        if(isset($data[GetMessage("SALE_EXPORT_".$k)]) && !empty($data[GetMessage("SALE_EXPORT_".$k)][0]["#"]))
        {
          $adr = $data[GetMessage("SALE_EXPORT_".$k)][0]["#"];
          foreach($v as $kk => $vv)
          {
            if(is_array($vv))
            {
              if(isset($adr[GetMessage("SALE_EXPORT_".$kk)]) && !empty($adr[GetMessage("SALE_EXPORT_".$kk)][0]["#"]) > 0)
              {
                foreach($vv as $vvv)
                {
                  foreach($adr[GetMessage("SALE_EXPORT_".$kk)] as $val)
                  {
                    if($val["#"][GetMessage("SALE_EXPORT_TYPE")][0]["#"] == GetMessage("SALE_EXPORT_".$vvv)
                        && strlen($val["#"][GetMessage("SALE_EXPORT_VALUE")][0]["#"]) > 0
                    )
                      $result["AGENT"][$kk][$vvv] = $val["#"][GetMessage("SALE_EXPORT_VALUE")][0]["#"];
                    elseif(empty($val["#"][GetMessage("SALE_EXPORT_TYPE")][0]["#"]) && $val["#"][GetMessage("SALE_EXPORT_RELATION")][0]["#"] == GetMessage("SALE_EXPORT_CONTACT_PERSON"))
                      $result["AGENT"]["CONTACT"][$vvv] = $val["#"][GetMessage("SALE_EXPORT_ITEM_NAME")][0]["#"];

                  }
                }
              }
            }
            else
            {
              if(isset($adr[GetMessage("SALE_EXPORT_".$vv)]) && strlen($adr[GetMessage("SALE_EXPORT_".$vv)][0]["#"]) > 0)
              {
                $result["AGENT"][$k][$vv] = $adr[GetMessage("SALE_EXPORT_".$vv)][0]["#"];
              }
              else
              {
                if(!empty($adr[GetMessage("SALE_EXPORT_ADDRESS_FIELD")]))
                {
                  foreach($adr[GetMessage("SALE_EXPORT_ADDRESS_FIELD")] as $val)
                  {
                    if($val["#"][GetMessage("SALE_EXPORT_TYPE")][0]["#"] == GetMessage("SALE_EXPORT_".$vv)
                        && strlen($val["#"][GetMessage("SALE_EXPORT_VALUE")][0]["#"]) > 0
                    )
                      $result["AGENT"][$k][$vv] = $val["#"][GetMessage("SALE_EXPORT_VALUE")][0]["#"];
                  }
                }
              }
            }
          }
        }
      }
      else
      {
        if(isset($data[GetMessage("SALE_EXPORT_".$v)]) && strlen($data[GetMessage("SALE_EXPORT_".$v)][0]["#"]) > 0)
          $result["AGENT"][$v] = $data[GetMessage("SALE_EXPORT_".$v)][0]["#"];
      }
    }

    $result["AGENT"]["AGENT_NAME"] = $result["AGENT"]["ITEM_NAME"];
    $result["AGENT"]["CONTACT"]["EMAIL"] = $result["AGENT"]["CONTACT"]["MAIL_NEW"];
    $result["AGENT"]["CONTACT"]["PHONE"] = $result["AGENT"]["CONTACT"]["WORK_PHONE_NEW"];
    $result["AGENT"]["OKPO"] = $result["AGENT"]["OKPO_CODE"];


    $result["ORDER_PROPS"] = array();
    foreach($result["AGENT"] as $k => $v)
    {
      if(!is_array($v) && !empty($v))
        $result["ORDER_PROPS"][$k] = $v;
      else
      {
        if($k == "CONTACT")
        {
          $result["ORDER_PROPS"]["EMAIL"] = $v["MAIL_NEW"];
          $result["ORDER_PROPS"]["PHONE"] = $v["WORK_PHONE_NEW"];
        }
        elseif($k == "REPRESENTATIVE")
        {
          $result["ORDER_PROPS"]["CONTACT_PERSON"] = $v["CONTACT_PERSON"];
        }
        elseif($k == "REGISTRATION_ADDRESS" || $k == "UR_ADDRESS")
        {
          $result["ORDER_PROPS"]["ADDRESS_FULL"] = $v["PRESENTATION"];
          $result["ORDER_PROPS"]["INDEX"] = $v["POST_CODE"];
          foreach($v as $k1 => $v1)
          {
            if(strlen($v1) > 0 && empty($result["ORDER_PROPS"][$k1]))
              $result["ORDER_PROPS"][$k1] = $v1;
          }
        }
        elseif($k == "ADDRESS")
        {
          $result["ORDER_PROPS"]["F_ADDRESS_FULL"] = $v["PRESENTATION"];
          $result["ORDER_PROPS"]["F_INDEX"] = $v["POST_CODE"];
          foreach($v as $k1 => $v1)
          {
            if(strlen($v1) > 0 && empty($result["ORDER_PROPS"]["F_".$k1]))
              $result["ORDER_PROPS"]["F_".$k1] = $v1;
          }
        }
      }
    }

    if(strlen($result["AGENT"]["OFICIAL_NAME"]) > 0 && strlen($result["AGENT"]["INN"]) > 0)
      $result["AGENT"]["TYPE"] = "UR";
    elseif(strlen($result["AGENT"]["INN"]) > 0)
      $result["AGENT"]["TYPE"] = "IP";
    else
      $result["AGENT"]["TYPE"] = "FIZ";

    return $result;
  }

  function nodeHandler(CDataXML $value)
  {
    $value = $value->GetArray();
    $this->strErrorDocument = '';

    if(!empty($value[GetMessage("CC_BSC1_DOCUMENT")]))
    {
      $value = $value[GetMessage("CC_BSC1_DOCUMENT")];

      //логируем все входящие документы XML
      self::writeError(print_r($value, true));

      $arDocument = $this->collectDocumentInfo($value);

      if(!empty($arDocument))
      {
        // читаем склад из XML
        $arDocument["STORE"] = array(
          "XML_ID" => $value["#"]["Склады"][0]["#"]["Склад"][0]["#"]["Ид"][0]["#"],
          "NAME"   => $value["#"]["Склады"][0]["#"]["Склад"][0]["#"]["Наименование"][0]["#"]
        );

        $this->logMessage("StartExchange:");
        $this->logMessage("VersionSchema: ".self::getVersionSchema());

        if (self::getVersionSchema() >= self::PARTIAL_VERSION)
        {
          if(\Bitrix\Main\Config\Option::get('catalog', 'default_use_store_control', 'N')=='Y' || \Bitrix\Main\Config\Option::get('catalog', 'enable_reservation', 'N')=='Y')
            $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_USE_STORE_SALE");
          else
          {
            if(\Bitrix\Main\Config\Option::get("main", "~sale_converted_15", 'N') <> 'Y')
              $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_CONVERT_SALE");
            else
            {
              if(\Bitrix\Main\Config\Option::get("sale", "allow_deduction_on_delivery", "N") == 'Y')
                $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SALE_ALLOW_DEDUCTION_ON_DELIVERY_ERROR");
              else
              {
                if(!self::checkPSOnStatusPaymentOrder())
                  $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_PS_ON_STATUS_PAYMENT_ORDER_ERROR");
                else
                {
                  $this->logMessage("OperationType: ".$arDocument['OPERATION_TYPE']);

                  switch($arDocument['OPERATION_TYPE'])
                  {
                    case 'order_operation':

                      /** @var Bitrix\Sale\Order $order */
                      if(strlen($arDocument["XML_1C_DOCUMENT_ID"])>0)
                      {
                        $this->setVersion1C($arDocument["VERSION_1C"]);
                        $this->setXMLDocumentID($arDocument["XML_1C_DOCUMENT_ID"]);

                        $this->logMessage("Document.XML_1C_DOCUMENT_ID: ".$arDocument['XML_1C_DOCUMENT_ID']);
                        $this->logMessage("Document.VERSION_1C: ".$arDocument['VERSION_1C']);

                        if(intval($arDocument["ID"])>0)
                        {
                          $this->logMessage("UpdateOrder:");
                          $this->logMessage("ID: ".$arDocument['ID']);

                          $this->updateOrderWithoutShipmentsPayments($arDocument);
                          if(strlen($this->strErrorDocument)<=0)
                          {
                            $order = \Bitrix\Sale\Order::load($arDocument["ID"]);

                            $this->updateEntityCompatible1C($order, $arDocument);

                            $order->setField('UPDATED_1C', 'Y');
                            $order->setField('VERSION_1C', $this->getVersion1C());
                            $order->setField('ID_1C', $this->getXMLDocumentID());
                            $r = $order->save();
                            if (!$r->isSuccess())
                              $this->strErrorDocument .= array_shift($r->getErrors())->getMessage();
                          }
                        }
                        elseif(\Bitrix\Main\Config\Option::get("sale", "1C_IMPORT_NEW_ORDERS", "Y") == "Y")
                        {
                          $this->logMessage("NewOrder:");

                          $arOrder = $this->addOrderWithoutShipmentsPayments($arDocument);

                          if(intval($arOrder['ID'])>0)
                          {
                            $order = \Bitrix\Sale\Order::load($arOrder["ID"]);
                            if(strlen($this->strErrorDocument)<=0)
                            {
                              $this->createEntityCompatible1C($order, $arDocument);

                              $order->setField('EXTERNAL_ORDER','Y');
                              $order->setField('UPDATED_1C','Y');
                              $order->setField('VERSION_1C', $this->getVersion1C());
                              $order->setField('ID_1C', $this->getXMLDocumentID());

                              if(strlen($arDocument["DATE"])>0)
                                $order->setField('DATE_INSERT', new Bitrix\Main\Type\DateTime(CDatabase::FormatDate($arDocument["DATE"]." ".$arDocument["TIME"], "YYYY-MM-DD HH:MI:SS", CLang::GetDateFormat("FULL", LANG))));
                              $r = $order->save();
                              if(!$r->isSuccess())
                                $this->strErrorDocument .= array_shift($r->getErrors())->getMessage();
                            }
                          }
                          else
                            $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_ORDER_ERROR_2", Array('#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID']));
                        }
                      }
                      else
                      {
                        $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_ORDER_ERROR_1");
                      }
                      break;
                    case 'pay_system_b_operation':
                    case 'pay_system_c_operation':
                    case 'pay_system_a_operation':

                      /** @var Bitrix\Sale\Order $order */
                      if(isset($arDocument['PAYMENT_ORDER_ID']) && strlen($arDocument['ORDER_ID'])<=0)
                        $arDocument['ORDER_ID'] = $arDocument['PAYMENT_ORDER_ID'];

                      if(strlen($arDocument["XML_1C_DOCUMENT_ID"])>0)
                      {
                        $this->setVersion1C($arDocument["VERSION_1C"]);
                        $this->setXMLDocumentID($arDocument["XML_1C_DOCUMENT_ID"]);

                        $this->logMessage("Document.XML_1C_DOCUMENT_ID: ".$arDocument['XML_1C_DOCUMENT_ID']);
                        $this->logMessage("Document.VERSION_1C: ".$arDocument['VERSION_1C']);

                        if($arDocument['ORDER_ID'] !== false)
                        {
                          if($order = \Bitrix\Sale\Order::load($arDocument['ORDER_ID']))
                          {
                            if ($order->getField("STATUS_ID") != "F")
                            {
                              if($arDocument['CANCELED'] == "true")
                              {
                                $paymentCollection = $order->getPaymentCollection();

                                if(strlen($arDocument["ID"])>0 && ($payment = $paymentCollection->getItemById($arDocument["ID"])))
                                {
                                  $deletePayment = $this->deleteDocumentPayment($payment);
                                  if(!$deletePayment->isSuccess())
                                    $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_ORDER_ERROR_4", Array('#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID'])).array_shift($deletePayment->getErrors())->getMessage();
                                }
                                else
                                  $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_PAYMENT_ERROR_9", Array( '#ORDER_ID#'=>$arDocument['ORDER_ID'], '#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID']));
                              }
                              else
                              {
                                if(strlen($arDocument["ID"])>0)
                                {
                                  $paymentCollection = $order->getPaymentCollection();

                                  if($payment = $paymentCollection->getItemById($arDocument["ID"]))
                                  {
                                    $this->beforePaidCompatible1C($order);

                                    $this->updatePaymentFromDocument($arDocument, $payment);

                                    if(strlen($this->strErrorDocument)<=0)
                                    {
                                      $this->Paid($payment, $arDocument);

                                      $this->afterPaidCompatible1C($order);

                                      if(strlen($this->strErrorDocument)<=0)
                                      {
                                        $payment->setField('UPDATED_1C','Y');
                                        $payment->setField('VERSION_1C', $this->getVersion1C());
                                        $payment->setField('ID_1C',$this->getXMLDocumentID());
                                      }
                                    }
                                  }
                                  else
                                    $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_PAYMENT_ERROR_3", Array("#ID#" => $arDocument["ID"], '#ORDER_ID#'=>$arDocument['ORDER_ID'], '#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID']));
                                }
                                elseif (\Bitrix\Main\Config\Option::get("sale", "1C_IMPORT_NEW_PAYMENT", "Y") == 'Y') // create new payment (ofline 1C))
                                {
                                  $this->beforePaidCompatible1C($order);

                                  $payment = $this->addPaymentFromDocumentByOrder($arDocument, $order);
                                  if(strlen($this->strErrorDocument)<=0 && !is_null($payment))
                                  {
                                    $this->Paid($payment, $arDocument);

                                    $this->afterPaidCompatible1C($order);

                                    if(strlen($this->strErrorDocument)<=0)
                                    {
                                      $payment->setField('EXTERNAL_PAYMENT','Y');
                                      $payment->setField('VERSION_1C', $this->getVersion1C());
                                      $payment->setField('ID_1C',$this->getXMLDocumentID());
                                    }
                                  }
                                }
                              }
                            }
                            else
                              $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_PAYMENT_ERROR_10", Array('#ORDER_ID#'=>$order->getId(), '#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID']));
                          }
                          else
                            $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_PAYMENT_ERROR_8",array('#ORDER_ID#'=>$order->getId(), '#XML_1C_DOCUMENT_ID#'=>$this->getXMLDocumentID()));
                        }
                        else
                          $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_PAYMENT_ERROR_5",array('#XML_1C_DOCUMENT_ID#'=>$this->getXMLDocumentID()));
                      }
                      else
                        $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_PAYMENT_ERROR_6");

                      if(strlen($this->strErrorDocument)<=0)
                      {
                        $order->setField('UPDATED_1C', 'Y');

                        $r = $order->save();
                        if(!$r->isSuccess())
                          $this->strErrorDocument .= array_shift($r->getErrors())->getMessage();
                      }

                      break;
                    case 'shipment_operation':

                      if(isset($arDocument['SHIPMENT_ORDER_ID']) && strlen($arDocument['ORDER_ID'])<=0)
                        $arDocument['ORDER_ID'] = $arDocument['SHIPMENT_ORDER_ID'];

                      if(strlen($arDocument["XML_1C_DOCUMENT_ID"])>0)
                      {
                        $this->setVersion1C($arDocument["VERSION_1C"]);
                        $this->setXMLDocumentID($arDocument["XML_1C_DOCUMENT_ID"]);
                        $this->setOrderIdOriginal($arDocument["ORDER_ID_ORIG"]);

                        $this->logMessage("Document.XML_1C_DOCUMENT_ID: ".$arDocument['XML_1C_DOCUMENT_ID']);
                        $this->logMessage("Document.VERSION_1C: ".$arDocument['VERSION_1C']);
                        $this->logMessage("Document.ORDER_ID_ORIG: ".$arDocument['ORDER_ID_ORIG']);

                        if($arDocument['ORDER_ID'] !== false)
                        {
                          /** @var Bitrix\Sale\Order $order */
                          if($order = \Bitrix\Sale\Order::load($arDocument['ORDER_ID']))
                          {
                            if ($order->getField("STATUS_ID") != "F")
                            {
                              if($arDocument["CANCELED"] == "true")
                              {
                                if (strlen($arDocument["ID"])>0 && ($shipment = $order->getShipmentCollection()->getItemById($arDocument['ID'])))
                                {
                                  $deleteShipment = $this->deleteDocumentShipment($shipment);
                                  if(!$deleteShipment->isSuccess())
                                    $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_ORDER_ERROR_4", Array('#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID'])).array_shift($deleteShipment->getErrors())->getMessage();
                                }
                                else
                                  $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_16", Array( '#ORDER_ID#'=>$arDocument['ORDER_ID'], '#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID']));
                              }
                              else
                              {
                                if(strlen($arDocument["ID"])>0)
                                {
                                  if ($shipment = $order->getShipmentCollection()->getItemById($arDocument['ID']))
                                  {
                                    /** @var Bitrix\Sale\Shipment $shipment */
                                    if (!$shipment->isSystem())
                                    {
                                      if (!$shipment->isShipped())
                                      {
                                        $this->deleteShipmentItemsByDocument($arDocument, $shipment);

                                        $this->updateShipmentQuantityFromDocument($arDocument, $shipment);

                                        if(strlen($this->strErrorDocument)<=0)
                                        {
                                          $this->Ship($shipment, $arDocument);

                                          $this->afterShippedCompatible1C($order);

                                          if(strlen($this->strErrorDocument)<=0)
                                          {
                                            $shipment->setField('UPDATED_1C','Y');
                                            $shipment->setField('VERSION_1C', $this->getVersion1C());
                                            $shipment->setField('ID_1C',$this->getXMLDocumentID());
                                          }
                                        }
                                      }
                                      else
                                        $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_2", Array("#ID#" => $arDocument["ID"],'#ORDER_ID#'=>$arDocument['ORDER_ID'],'#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID']));
                                    }
                                    else
                                      $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_14", Array("#ID#" => $arDocument["ID"],'#ORDER_ID#'=>$arDocument['ORDER_ID'],'#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID']));
                                  }
                                  else
                                    $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_3", Array("#ID#" => $arDocument["ID"],'#ORDER_ID#'=>$arDocument['ORDER_ID'],'#XML_1C_DOCUMENT_ID#'=>$arDocument['XML_1C_DOCUMENT_ID']));
                                }
                                elseif(\Bitrix\Main\Config\Option::get("sale", "1C_IMPORT_NEW_SHIPMENT", 'Y')=='Y')
                                {
                                  $shipment = $this->addShipmentFromDocumentByOrder($arDocument, $order);

                                  if(strlen($this->strErrorDocument)<=0)
                                  {
                                    $this->Ship($shipment, $arDocument);

                                    $this->afterShippedCompatible1C($order);

                                    if(strlen($this->strErrorDocument)<=0)
                                    {
                                      $shipment->setField('VERSION_1C',$this->getVersion1C());
                                      $shipment->setField('ID_1C', $this->getXMLDocumentID());
                                      $shipment->setField('EXTERNAL_DELIVERY','Y');
                                      $shipment->setField('UPDATED_1C','Y');
                                    }
                                  }
                                }
                              }
                            }
                            else
                              $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_18", Array('#ORDER_ID#'=>$order->getId(), '#XML_1C_DOCUMENT_ID#'=>$this->getXMLDocumentID()));
                          }
                          else
                            $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_15",array('#ORDER_ID#'=>$order->getId(),'#XML_1C_DOCUMENT_ID#'=>$this->getXMLDocumentID()));
                        }
                        elseif(\Bitrix\Main\Config\Option::get("sale", "1C_IMPORT_NEW_ORDER_NEW_SHIPMENT", "Y") == 'Y') // create new shipment (ofline 1C))
                        {
                          if($arDocument["CANCELED"] != "true")
                          {
                            /** @var Bitrix\Sale\Order $order */
                            $arOrder = $this->addOrderWithoutShipmentsPayments($arDocument);
                            if($arOrder['ID']>0)
                            {
                              $order = \Bitrix\Sale\Order::load($arOrder['ID']);
                              $shipment = $this->addShipmentFromDocumentByOrder($arDocument, $order);

                              if(strlen($this->strErrorDocument)<=0)
                              {
                                $this->Ship($shipment, $arDocument);

                                if(strlen($this->strErrorDocument)<=0)
                                {
                                  $shipment->setField('VERSION_1C', $this->getVersion1C());
                                  $shipment->setField('ID_1C', $this->getXMLDocumentID());
                                  $shipment->setField('EXTERNAL_DELIVERY', 'Y');
                                  $shipment->setField('UPDATED_1C', 'Y');

                                  $order->setField('VERSION_1C', $this->getVersion1C());
                                  $order->setField('ID_1C', $this->getOrderIdOriginal());
                                  $order->setField('EXTERNAL_ORDER', 'Y');

                                }
                              }
                            }
                            else
                              $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_7", Array('#XML_1C_DOCUMENT_ID#'=>$this->getXMLDocumentID()));
                          }
                          else
                            $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_17", Array('#XML_1C_DOCUMENT_ID#'=>$this->getXMLDocumentID()));
                        }
                        else
                          $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_5", Array("#ID#" => $arDocument["ID"],'#XML_1C_DOCUMENT_ID#'=>$this->getXMLDocumentID()));
                      }
                      else
                        $this->strErrorDocument .= "\n".GetMessage("CC_BSC1_SHIPMENT_ERROR_6", Array("#ID#" => $arDocument["ID"]));

                      if(strlen($this->strErrorDocument)<=0)
                      {
                        if($order->isShipped())
                        {
                          if(strlen($this->arParams["FINAL_STATUS_ON_DELIVERY"])>0 &&
                              $order->getField("STATUS_ID") != "F" &&
                              $order->getField("STATUS_ID") != $this->arParams["FINAL_STATUS_ON_DELIVERY"]
                          )
                          {
                            $order->setField("STATUS_ID", $this->arParams["FINAL_STATUS_ON_DELIVERY"]);
                          }
                        }

                        $order->setField('UPDATED_1C', 'Y');

                        $r=$order->save();
                        if (!$r->isSuccess())
                          $this->strErrorDocument .= array_shift($r->getErrorMessages());
                      }

                      break;
                  }
                }
              }
            }
          }
          $this->logMessage("FinalExchange \r\n\r\n");
        }
        else
        {
          self::oldSaveOrder($arDocument);
        }
      }
      $this->strError .= $this->strErrorDocument;
    }
    elseif(\Bitrix\Main\Config\Option::get("sale", "1C_IMPORT_NEW_ORDERS", "Y") == "Y")
    {

      $value = $value[GetMessage("CC_BSC1_AGENT")]["#"];
      $arAgentInfo = $this->collectAgentInfo($value);

      if(!empty($arAgentInfo["AGENT"]))
      {
        $mode = false;
        $arErrors = array();
        $dbUProp = CSaleOrderUserProps::GetList(array(), array("XML_ID" => $arAgentInfo["AGENT"]["ID"]), false, false, array("ID", "NAME", "USER_ID", "PERSON_TYPE_ID", "XML_ID", "VERSION_1C"));

        if($arUProp = $dbUProp->Fetch())
        {
          if($arUProp["VERSION_1C"] != $arAgentInfo["AGENT"]["VERSION"])
          {
            $mode = "update";
            $arAgentInfo["PROFILE_ID"] = $arUProp["ID"];
            $arAgentInfo["PERSON_TYPE_ID"] = $arUProp["PERSON_TYPE_ID"];
            $arAgentInfo["USER_ID"] = $arUProp["USER_ID"];

            $this->updateDealer($arAgentInfo, $mode);
          }
        }
        else
        {
          $arUser = array(
            "NAME" => $arAgentInfo["AGENT"]["ITEM_NAME"],
            "EMAIL" => $arAgentInfo["AGENT"]["CONTACT"]["MAIL_NEW"],
          );

          if(strlen($arUser["NAME"]) <= 0)
            $arUser["NAME"] = $arAgentInfo["AGENT"]["CONTACT"]["CONTACT_PERSON"];

          $emServer = $_SERVER["SERVER_NAME"];
          if(strpos($_SERVER["SERVER_NAME"], ".") === false)
            $emServer .= ".bx";
          if(strlen($arUser["EMAIL"]) <= 0)
            $arUser["EMAIL"] = "buyer".time().GetRandomCode(2)."@".$emServer;
          $arAgentInfo["USER_ID"] = CSaleUser::DoAutoRegisterUser($arUser["EMAIL"], $arUser["NAME"], $this->arParams["SITE_NEW_ORDERS"], $arErrors, array("XML_ID"=>$arAgentInfo["AGENT"]["ID"]));


          if(IntVal($arAgentInfo["USER_ID"]) > 0)
          {
            $mode = "add";
            $this->updateDealer($arAgentInfo, $mode);
          }
          else
          {
            $this->strError .= "\n".GetMessage("CC_BSC1_AGENT_USER_PROBLEM", Array("#ID#" => $arAgentInfo["AGENT"]["ID"]));
            if(!empty($arErrors))
            {
              foreach($arErrors as $v)
              {
                $this->strError .= "\n".$v["TEXT"];
              }
            }
          }
        }

        if($mode)
        {
          if(empty($arPersonTypesIDs))
          {
            $dbPT = CSalePersonType::GetList(array(), array("ACTIVE" => "Y", "LIDS" => $this->arParams["SITE_NEW_ORDERS"]));
            while($arPT = $dbPT->Fetch())
            {
              $arPersonTypesIDs[] = $arPT["ID"];
            }
          }

          if(empty($arExportInfo))
          {
            $dbExport = CSaleExport::GetList(array(), array("PERSON_TYPE_ID" => $arPersonTypesIDs));
            while($arExport = $dbExport->Fetch())
            {
              $arExportInfo[$arExport["PERSON_TYPE_ID"]] = unserialize($arExport["VARS"]);
            }
          }

          if(IntVal($arAgentInfo["PERSON_TYPE_ID"]) <= 0)
          {
            foreach($arExportInfo as $pt => $value)
            {
              if(($value["IS_FIZ"] == "Y" && $arAgentInfo["AGENT"]["TYPE"] == "FIZ")
                  || ($value["IS_FIZ"] == "N" && $arAgentInfo["AGENT"]["TYPE"] != "FIZ")
              )
                $arAgentInfo["PERSON_TYPE_ID"] = $pt;
            }
          }

          if(IntVal($arAgentInfo["PERSON_TYPE_ID"]) > 0)
          {
            $arAgentInfo["ORDER_PROPS_VALUE"] = array();
            $arAgentInfo["PROFILE_PROPS_VALUE"] = array();

            $arAgent = $arExportInfo[$arAgentInfo["PERSON_TYPE_ID"]];

            foreach($arAgent as $k => $v)
            {
              if(strlen($v["VALUE"]) <= 0 || $v["TYPE"] != "PROPERTY")
                unset($arAgent[$k]);
            }

            foreach($arAgent as $k => $v)
            {
              if(!empty($arAgentInfo["ORDER_PROPS"][$k]))
                $arAgentInfo["ORDER_PROPS_VALUE"][$v["VALUE"]] = $arAgentInfo["ORDER_PROPS"][$k];
            }

            if (IntVal($arAgentInfo["PROFILE_ID"]) > 0)
            {
              CSaleOrderUserProps::Update($arUProp["ID"], array("VERSION_1C" => $arAgentInfo["AGENT"]["VERSION"], "NAME" => $arAgentInfo["AGENT"]["AGENT_NAME"], "USER_ID" => $arAgentInfo["USER_ID"]));
              $dbUPV = CSaleOrderUserPropsValue::GetList(array(), array("USER_PROPS_ID" => $arAgentInfo["PROFILE_ID"]));
              while($arUPV = $dbUPV->Fetch())
              {
                $arAgentInfo["PROFILE_PROPS_VALUE"][$arUPV["ORDER_PROPS_ID"]] = array("ID" => $arUPV["ID"], "VALUE" => $arUPV["VALUE"]);
              }
            }

            if(empty($arOrderProps[$arAgentInfo["PERSON_TYPE_ID"]]))
            {
              $dbOrderProperties = CSaleOrderProps::GetList(
                  array("SORT" => "ASC"),
                  array(
                      "PERSON_TYPE_ID" => $arAgentInfo["PERSON_TYPE_ID"],
                      "ACTIVE" => "Y",
                      "UTIL" => "N",
                      "USER_PROPS" => "Y",
                  ),
                  false,
                  false,
                  array("ID", "TYPE", "NAME", "CODE", "USER_PROPS", "SORT", "MULTIPLE")
              );
              while ($arOrderProperties = $dbOrderProperties->Fetch())
              {
                $arOrderProps[$arAgentInfo["PERSON_TYPE_ID"]][] = $arOrderProperties;
              }
            }

            foreach($arOrderProps[$arAgentInfo["PERSON_TYPE_ID"]] as $arOrderProperties)
            {
              $curVal = $arAgentInfo["ORDER_PROPS_VALUE"][$arOrderProperties["ID"]];

              if (strlen($curVal) > 0)
              {
                if (IntVal($arAgentInfo["PROFILE_ID"]) <= 0)
                {
                  $arFields = array(
                      "NAME" => $arAgentInfo["AGENT"]["AGENT_NAME"],
                      "USER_ID" => $arAgentInfo["USER_ID"],
                      "PERSON_TYPE_ID" => $arAgentInfo["PERSON_TYPE_ID"],
                      "XML_ID" => $arAgentInfo["AGENT"]["ID"],
                      "VERSION_1C" => $arAgentInfo["AGENT"]["VERSION"],
                  );
                  $arAgentInfo["PROFILE_ID"] = CSaleOrderUserProps::Add($arFields);
                }
                if(IntVal($arAgentInfo["PROFILE_ID"]) > 0)
                {
                  $arFields = array(
                      "USER_PROPS_ID" => $arAgentInfo["PROFILE_ID"],
                      "ORDER_PROPS_ID" => $arOrderProperties["ID"],
                      "NAME" => $arOrderProperties["NAME"],
                      "VALUE" => $curVal
                  );
                  if(empty($arAgentInfo["PROFILE_PROPS_VALUE"][$arOrderProperties["ID"]]))
                  {
                    CSaleOrderUserPropsValue::Add($arFields);
                  }
                  elseif($arAgentInfo["PROFILE_PROPS_VALUE"][$arOrderProperties["ID"]]["VALUE"] != $curVal)
                  {
                    CSaleOrderUserPropsValue::Update($arAgentInfo["PROFILE_PROPS_VALUE"][$arOrderProperties["ID"]]["ID"], $arFields);
                  }
                }
              }
            }
          }
          else
          {
            $this->strError .= "\n".GetMessage("CC_BSC1_AGENT_PERSON_TYPE_PROBLEM", Array("#ID#" => $arAgentInfo["AGENT"]["ID"]));
          }
        }
      }
      else
      {
        $this->strError .= "\n".GetMessage("CC_BSC1_AGENT_NO_AGENT_ID");
      }
    }
  }

  function oldSaveOrder($arOrder)
  {
    $arOrder["ID_1C"] = trim($arOrder["ID_1C"]);

    //перезаписываем ID_1C отгрузки/реализации
    if($arOrder["OPERATION_TYPE"] == "shipment_operation"){
      $orderList = \Bitrix\Sale\Order::getList(
        array(
          'select' => array('ID'),
          'filter' => array(
            'LOGIC' => 'OR',
            array( 'ID' => $arOrder['ORDER_ID_ORIG'] ),
            array( 'XML_ID' => $arOrder['ORDER_ID_ORIG'] )
          )
        )
      );

      $orderTmp = $orderList->Fetch();

      if(!empty($orderTmp)){
        $order = \Bitrix\Sale\Order::load($orderTmp['ID']);
        $shipmentCollection = $order->getShipmentCollection();

        foreach ($shipmentCollection as $shipment){
          if (!$shipment->isSystem()){
            $shipmentItemCollection = $shipment->getShipmentItemCollection();
            if ($shipmentItemCollection->getPrice() == 0) {
              break;
            }

            $shipment->setField("ID_1C", $arOrder["ID_1C"]);

            if($shipment->getField("STATUS_ID") == "DN"){
              $shipment->setField("STATUS_ID", "SR");
            }

            $shipment->save();
          }
        }
      }
      return;
    }

    global $APPLICATION, $USER;

    if (strlen($arOrder["ID"]) <= 0 && strlen($arOrder["ID_1C"]) > 0)//try to search order from 1C
    {
      $dbOrder = CSaleOrder::GetList(array("ID" => "DESC"), array("ID_1C" => $arOrder["ID_1C"]), false, false, array("ID", "ID_1C"));
      if ($orderInfo = $dbOrder->Fetch()) {
        $arOrder["ID"] = $orderInfo["ID"];
      }
    }
    if(strlen($arOrder["ID"]) > 0) // exists site order
    {
      $dbOrder = CSaleOrder::GetList(array(), array("ID" => $arOrder["ID"]), false, false, array("ID", "LID", "PERSON_TYPE_ID", "PAYED", "DATE_PAYED", "CANCELED", "DATE_CANCELED", "REASON_CANCELED", "STATUS_ID", "DATE_STATUS", "PAY_VOUCHER_NUM", "PAY_VOUCHER_DATE", "PRICE_DELIVERY", "ALLOW_DELIVERY", "DATE_ALLOW_DELIVERY", "PRICE", "CURRENCY", "DISCOUNT_VALUE", "USER_ID", "PAY_SYSTEM_ID", "DELIVERY_ID", "DATE_INSERT", "DATE_INSERT_FORMAT", "DATE_UPDATE", "USER_DESCRIPTION", "ADDITIONAL_INFO", "COMMENTS", "TAX_VALUE", "DELIVERY_DOC_NUM", "DELIVERY_DOC_DATE", "STORE_ID", "ACCOUNT_NUMBER", "VERSION", "VERSION_1C", "ID_1C"));
      if($orderInfo = $dbOrder->Fetch())
      {
        if($arOrder["VERSION_1C"] != $orderInfo["VERSION_1C"] || (strlen($orderInfo["VERSION_1C"]) <= 0 || strlen($arOrder["VERSION_1C"]) <= 0)) // skip update if the same version
        {
          $arOrderFields = array();
          $orderId = $orderInfo["ID"];
          CSaleOrderChange::AddRecord($orderId, "ORDER_1C_IMPORT");
          if($arOrder["ID_1C"] != $orderInfo["ID_1C"])
            $arOrderFields["ID_1C"] = $arOrder["ID_1C"];

          $arOrderFields["VERSION_1C"] = $arOrder["VERSION_1C"];

          if($orderInfo["PAYED"] != "Y" && $orderInfo["STATUS_ID"] != "F")
          {
            $dbOrderTax = CSaleOrderTax::GetList(
                array(),
                array("ORDER_ID" => $orderId),
                false,
                false,
                array("ID", "TAX_NAME", "VALUE", "VALUE_MONEY", "CODE", "IS_IN_PRICE")
            );
            $bTaxFound = false;
            if($arOrderTax = $dbOrderTax->Fetch())
            {
              $bTaxFound = true;

              if(IntVal($arOrderTax["VALUE_MONEY"]) != IntVal($arOrder["TAX"]["VALUE_MONEY"]) || IntVal($arOrderTax["VALUE"]) != IntVal($arOrder["TAX"]["VALUE"]) || ($arOrderTax["IS_IN_PRICE"] != $arOrder["TAX"]["IS_IN_PRICE"]))
              {
                if(IntVal($arOrder["TAX"]["VALUE"])>0)
                {
                  $arFields = Array(
                      "TAX_NAME" => $arOrder["TAX"]["NAME"],
                      "ORDER_ID" => $orderId,
                      "VALUE" => $arOrder["TAX"]["VALUE"],
                      "IS_PERCENT" => "Y",
                      "IS_IN_PRICE" => $arOrder["TAX"]["IS_IN_PRICE"],
                      "VALUE_MONEY" => $arOrder["TAX"]["VALUE_MONEY"],
                      "CODE" => "VAT1C",
                      "APPLY_ORDER" => "100"
                  );
                  CSaleOrderTax::Update($arOrderTax["ID"], $arFields);
                  $arOrderFields["TAX_VALUE"] = $arOrder["TAX"]["VALUE_MONEY"];
                }
                else
                {
                  CSaleOrderTax::Delete($arOrderTax["ID"]);
                  $arOrderFields["TAX_VALUE"] = 0;
                }
              }
            }

            if(!$bTaxFound)
            {
              if(IntVal($arOrder["TAX"]["VALUE"])>0)
              {
                $arFields = Array(
                    "TAX_NAME" => $arOrder["TAX"]["NAME"],
                    "ORDER_ID" => $orderId,
                    "VALUE" => $arOrder["TAX"]["VALUE"],
                    "IS_PERCENT" => "Y",
                    "IS_IN_PRICE" => $arOrder["TAX"]["IS_IN_PRICE"],
                    "VALUE_MONEY" => $arOrder["TAX"]["VALUE_MONEY"],
                    "CODE" => 'VAT1C',
                    "APPLY_ORDER" => '100',
                );
                CSaleOrderTax::Add($arFields);
                $arOrderFields["TAX_VALUE"] = $arOrder["TAX"]["VALUE_MONEY"];
              }
            }

            $arShoppingCart = array();
            $bNeedUpdate = false;
            $dbBasket = CSaleBasket::GetList(
                array("NAME" => "ASC"),
                array("ORDER_ID" => $orderId),
                false,
                false,
                array(
                    "ID",
                    "QUANTITY",
                    "CANCEL_CALLBACK_FUNC",
                    "MODULE",
                    "PRODUCT_ID",
                    "PRODUCT_PROVIDER_CLASS",
                    "RESERVED",
                    "RESERVE_QUANTITY",
                    "TYPE",
                    "SET_PARENT_ID",
                    "PRICE",
                    "VAT_RATE",
                    "DISCOUNT_PRICE",
                    "PRODUCT_XML_ID",
                )
            );

            HinoReservationPart::deleteOrder($orderId);
            $res = CCatalogStore::GetList(array("SORT"=>"ASC"), array("ACTIVE" => "Y"), false, false, array("ID", "XML_ID"));
            while($arRes = $res->GetNext()){
              $arStock[$arRes["XML_ID"]] = $arRes["ID"];
            }

            $storeID = $arStock[$arOrder["STORE"]["XML_ID"]];

            switch ($arOrder["TRAITS"][GetMessage("CC_BSC1_TYPE")]) {
              case 'Обычный':
                $deliveryID = 6;
                break;
              case 'Обычный ВО':
              case 'Обычный BO':
                if($storeID == 1){
                  $storeID = 3;
                }
                if($storeID == 2){
                  $storeID = 4;
                }
                $deliveryID = 6;
                break;
              case 'Срочный':
                $deliveryID = 2;
                break;
              case 'Срочный BO':
              case 'Срочный BO':
                if($storeID == 1){
                  $storeID = 3;
                }
                if($storeID == 2){
                  $storeID = 4;
                }
                $deliveryID = 2;
                break;
              case 'Сверхсрочный':
                if($storeID == 1 || $storeID == 2){
                  $deliveryID = 2;
                }else{
                  $deliveryID = 10;
                }
                break;
              case 'Сверхсрочный ВО':
              case 'Сверхсрочный BO':
                if($storeID == 1){
                  $storeID = 3;
                }
                if($storeID == 2){
                  $storeID = 4;
                }
                $deliveryID = 10;
                break;
            }

            $fuser_id = \Bitrix\Sale\Fuser::getIdByUserId($orderInfo["USER_ID"]);

            while ($arBasket = $dbBasket->Fetch())
            {
              $arFields = Array();
              if(!empty($arOrder["items"][$arBasket["PRODUCT_XML_ID"]]))
              {
                if($arBasket["QUANTITY"] != $arOrder["items"][$arBasket["PRODUCT_XML_ID"]]["QUANTITY"])
                  $arFields["QUANTITY"] = $arOrder["items"][$arBasket["PRODUCT_XML_ID"]]["QUANTITY"];
                if($arBasket["PRICE"] != $arOrder["items"][$arBasket["PRODUCT_XML_ID"]]["PRICE"])
                  $arFields["PRICE"] = $arOrder["items"][$arBasket["PRODUCT_XML_ID"]]["PRICE"];
                if($arBasket["VAT_RATE"] != $arOrder["items"][$arBasket["PRODUCT_XML_ID"]]["VAT_RATE"])
                  $arFields["VAT_RATE"] = $arOrder["items"][$arBasket["PRODUCT_XML_ID"]]["VAT_RATE"];
                if($arBasket["DISCOUNT_PRICE"] != $arOrder["items"][$arBasket["PRODUCT_XML_ID"]]["DISCOUNT_PRICE"])
                  $arFields["DISCOUNT_PRICE"] = $arOrder["items"][$arBasket["PRODUCT_XML_ID"]]["DISCOUNT_PRICE"];

                if(count($arFields)>0)
                {
                  $arFields["ID"] = $arBasket["ID"];
                  if(DoubleVal($arFields["QUANTITY"]) <= 0)
                    $arFields["QUANTITY"] = $arBasket["QUANTITY"];
                  $bNeedUpdate = true;
                  $arShoppingCart[] = $arFields;
                }
                else
                {
                  $arShoppingCart[] = $arBasket;
                }

                $arOrder["items"][$arBasket["PRODUCT_XML_ID"]]["CHECKED"] = "Y";
              }
              else
              {
                if($arOrder['CANCELED'] != "true" && $arOrder["TRAITS"][GetMessage("CC_BSC1_CANCELED")] != "true" && $orderInfo["CANCELED"] == "N")
                {
                  $bNeedUpdate = true;
                  //CSaleBasket::Delete($arBasket["ID"]);
                }
              }
            }

            $priceDelivery = 0;
            if(!empty($arOrder["items"]))
            {
              foreach ($arOrder["items"] as $itemID => $arItem)
              {
                $priceDelivery += $arItem["PRICE_DELIVERY_ONE"];
                $product = HinoProduct::getProductByXmlId($itemID);

                $hr = new HinoReservationPart($product["ID"]);
                $arFieldsReserv = array(
                  "PRODUCT_ID"  => $product["ID"],
                  "FUSER_ID"    => $fuser_id,
                  "STORE_ID"    => $storeID,
                  "DELIVERY_ID" => $deliveryID,
                  "QUANTITY"    => $arItem["QUANTITY"],
                  "ORDER_ID"    => $orderId,
                  "RESERVED"    => (strlen($arOrder["TRAITS"][GetMessage("CC_BSC1_1C_DELIVERY_DATE")]) > 1)? "N": "Y"
                );
                $hr->insertReservation($arFieldsReserv);

                if ($arItem["CHECKED"] != "Y")
                {
                  if ($arItem["TYPE"] == GetMessage("CC_BSC1_ITEM"))
                  {
                    if ($arBasketFields = $this->prepareProduct4Basket($itemID, $arItem, $orderId, $orderInfo))
                    {
                      $arShoppingCart[] = $arBasketFields;
                      $bNeedUpdate = true;
                    }
                  }
                }
              }
            }

            if($bNeedUpdate)
            {
              $arErrors = array();

              if(!CSaleBasket::DoSaveOrderBasket($orderId, $orderInfo["LID"], $orderInfo["USER_ID"], $arShoppingCart, $arErrors))
              {
                $e = $APPLICATION->GetException();
                if(is_object($e))
                  $this->strError .= "\r\n ".GetMessage("CC_BSC1_ORDER_ERROR_3", Array('#XML_1C_DOCUMENT_ID#'=>$arOrder["ID"])).$e->GetString();
              }

            }

            if(intval($orderId) > 0 && intval($deliveryID) > 0){
              HinoReservationPart::updateOrderFields(
                $orderId,
                array(
                  "STORE_ID"    => $storeID,
                  "DELIVERY_ID" => $deliveryID
                )
              );
            }

            if(DoubleVal($arOrder["AMOUNT"]) > 0 && $arOrder["AMOUNT"] != $orderInfo["PRICE"])
              $arOrderFields["PRICE"] = $arOrder["AMOUNT"];
            if(DoubleVal($orderInfo["DISCOUNT_VALUE"]) > 0)
              $arOrderFields["DISCOUNT_VALUE"] = 0;
            if(strlen($arOrder["COMMENT"]) > 0 && $arOrder["COMMENT"] != $orderInfo["COMMENTS"])
              $arOrderFields["COMMENTS"] = $arOrder["COMMENT"];
            $arOrderFields["UPDATED_1C"] = "Y";
            if(!empty($arOrderFields))
              CSaleOrder::Update($orderId, $arOrderFields);


            $order = \Bitrix\Sale\Order::load($orderInfo["ID"]);
            $shipmentCollection = $order->getShipmentCollection();

            foreach ($shipmentCollection as $shipment){
              if (!$shipment->isSystem()){
                $shipment->setStoreId($storeID);

                $deliveryObj = Bitrix\Sale\Delivery\Services\Manager::getObjectById($deliveryID);
                $shipment->setFields(array(
                  'DELIVERY_ID'         => $deliveryObj->getId(),
                  'DELIVERY_NAME'       => $deliveryObj->getName(),
                  'CURRENCY'            => $deliveryObj->getCurrency(),
                  'PRICE_DELIVERY'      => $priceDelivery,
                  'BASE_PRICE_DELIVERY' => $priceDelivery
                ));
              }
            }

            $propertyCollection = $order->getPaymentCollection();
            $payment = $propertyCollection{0};
            $payment->setFields(array(
              "SUM" => $arOrder["AMOUNT"]
            ));

            $order->setField('PRICE', $arOrder["AMOUNT"]);
            $order->save();
          }
          else
          {
            $this->strError .= "\n".GetMessage("CC_BSC1_FINAL_NOT_EDIT", Array("#ID#" => $orderId));
            return;
          }
        }

        $arAditFields = Array();
        if($arOrder['CANCELED'] == "true" || $arOrder["TRAITS"][GetMessage("CC_BSC1_CANCELED")] == "true" || $arOrder["TRAITS"][GetMessage("CC_BSC1_CANCEL")] == "true")
        {
          if($orderInfo["CANCELED"] == "N")
          {
            CSaleOrder::CancelOrder($orderInfo["ID"], "Y", $arOrder["COMMENT"]);
            HinoReservationPart::updateOrderFields( $orderInfo["ID"], array("RESERVED" => "N"));

            $arAditFields["UPDATED_1C"] = "Y";
          }
        }
        else
        {
          if($arOrder["TRAITS"][GetMessage("CC_BSC1_CANCELED")] != "true")
          {
            if($orderInfo["CANCELED"] == "Y")
            {
              CSaleOrder::CancelOrder($orderInfo["ID"], "N", $arOrder["COMMENT"]);
              HinoReservationPart::updateOrderFields( $orderInfo["ID"], array("RESERVED" => "N"));
              $arAditFields["UPDATED_1C"] = "Y";
            }
          }

          if(strlen($arOrder["TRAITS"][GetMessage("CC_BSC1_1C_PAYED_DATE")])>1)
          {
            if($orderInfo["PAYED"]=="N")
              CSaleOrder::PayOrder($orderInfo["ID"], "Y");
            $arAditFields["PAY_VOUCHER_DATE"] = CDatabase::FormatDate(str_replace("T", " ", $arOrder["TRAITS"][GetMessage("CC_BSC1_1C_PAYED_DATE")]), "YYYY-MM-DD HH:MI:SS", CLang::GetDateFormat("FULL", LANG));
            if(strlen($arOrder["TRAITS"][GetMessage("CC_BSC1_1C_PAYED_NUM")])>0)
              $arAditFields["PAY_VOUCHER_NUM"] = $arOrder["TRAITS"][GetMessage("CC_BSC1_1C_PAYED_NUM")];
            $arAditFields["UPDATED_1C"] = "Y";
          }

          if(strlen($arOrder["TRAITS"][GetMessage("CC_BSC1_1C_DELIVERY_DATE")])>1)
          {
            if($orderInfo["ALLOW_DELIVERY"]=="N")
              CSaleOrder::DeliverOrder($orderInfo["ID"], "Y");

            $arAditFields["DATE_ALLOW_DELIVERY"] = CDatabase::FormatDate(str_replace("T", " ", $arOrder["TRAITS"][GetMessage("CC_BSC1_1C_DELIVERY_DATE")]), "YYYY-MM-DD HH:MI:SS", CLang::GetDateFormat("FULL", LANG));
            $arAditFields["DELIVERY_DOC_DATE"] = $arAditFields["DATE_ALLOW_DELIVERY"];

            if($arOrder["TRAITS"][GetMessage("CC_BSC1_1C_STATUS_ID")] == "F"){
              //отгружаем заказ
              CSaleOrder::DeductOrder($orderInfo["ID"], "Y");

              $order = \Bitrix\Sale\Order::load($orderInfo["ID"]);
              $shipmentCollection = $order->getShipmentCollection();

              foreach ($shipmentCollection as $shipment){
                if (!$shipment->isSystem()){

                  $shipmentItemCollection = $shipment->getShipmentItemCollection();
                  if ($shipmentItemCollection->getPrice() == 0) {
                    break;
                  }

                  $shipment->setField("STATUS_ID", "DF");
                  $shipment->save();
                }
              }
            }

            /*if(strlen($arOrder["TRAITS"][GetMessage("CC_BSC1_1C_DELIVERY_NUM")])>0)
              $arAditFields["DELIVERY_DOC_NUM"] = $arOrder["TRAITS"][GetMessage("CC_BSC1_1C_DELIVERY_NUM")];
            */
            $arAditFields["UPDATED_1C"] = "Y";
          }
        }


        if($this->arParams["CHANGE_STATUS_FROM_1C"] && strlen($arOrder["TRAITS"][GetMessage("CC_BSC1_1C_STATUS_ID")])>0)
        {
          if($orderInfo["STATUS_ID"] != $arOrder["TRAITS"][GetMessage("CC_BSC1_1C_STATUS_ID")])
          {
            CSaleOrder::StatusOrder($orderInfo["ID"], $arOrder["TRAITS"][GetMessage("CC_BSC1_1C_STATUS_ID")]);
            $arAditFields["UPDATED_1C"] = "Y";
          }
        }

        if(count($arAditFields)>0)
          CSaleOrder::Update($orderInfo["ID"], $arAditFields);

      }
      else
        $this->strError .= "\n".GetMessage("CC_BSC1_ORDER_NOT_FOUND", Array("#ID#" => $arOrder["ID"]));
    }
    elseif($this->arParams["IMPORT_NEW_ORDERS"] == "Y") // create new order (ofline 1C)
    {
      if(!empty($arOrder["AGENT"]) && strlen($arOrder["AGENT"]["ID"]) > 0)
      {
        $arOrder["PERSON_TYPE_ID"] = 0;
        $arOrder["USER_ID"] = 0;
        $arErrors = array();
        $dbUProp = CSaleOrderUserProps::GetList(array(), array("XML_ID" => $arOrder["AGENT"]["ID"]), false, false, array("ID", "NAME", "USER_ID", "PERSON_TYPE_ID", "XML_ID", "VERSION_1C"));
        if($arUProp = $dbUProp->Fetch())
        {
          $arOrder["USER_ID"] = $arUProp["USER_ID"];
          $arOrder["PERSON_TYPE_ID"] = $arUProp["PERSON_TYPE_ID"];
          $arOrder["USER_PROFILE_ID"] = $arUProp["ID"];
          $arOrder["USER_PROFILE_VERSION"] = $arUProp["VERSION_1C"];

          $dbUPropValue = CSaleOrderUserPropsValue::GetList(array(), array("USER_PROPS_ID" => $arUProp["ID"]));
          while($arUPropValue = $dbUPropValue->Fetch())
          {
            $arOrder["USER_PROPS"][$arUPropValue["ORDER_PROPS_ID"]] = $arUPropValue["VALUE"];
          }
        }
        else
        {
          if(strlen($arOrder["AGENT"]["ID"]) > 0)
          {
            $arAI = explode("#", $arOrder["AGENT"]["ID"]);
            if(IntVal($arAI[0]) > 0)
            {
              $dbUser = CUser::GetByID($arAI[0]);
              if($arU = $dbUser->Fetch())
              {
                if(htmlspecialcharsback(substr(htmlspecialcharsbx($arU["ID"]."#".$arU["LOGIN"]."#".$arU["LAST_NAME"]." ".$arU["NAME"]." ".$arU["SECOND_NAME"]), 0, 80)) == $arOrder["AGENT"]["ID"])
                {
                  $arOrder["USER_ID"] = $arU["ID"];
                }
              }
            }
          }

          if(IntVal($arOrder["USER_ID"]) <= 0)
          {
            //create new user
            $arUser = array(
                "NAME"  => $arOrder["AGENT"]["ITEM_NAME"],
                "EMAIL" => $arOrder["AGENT"]["CONTACT"]["MAIL_NEW"],
            );

            if (strlen($arUser["NAME"]) <= 0)
              $arUser["NAME"] = $arOrder["AGENT"]["CONTACT"]["CONTACT_PERSON"];

            $emServer = $_SERVER["SERVER_NAME"];
            if(strpos($_SERVER["SERVER_NAME"], ".") === false)
              $emServer .= ".bx";

            if (strlen($arUser["EMAIL"]) <= 0)
              $arUser["EMAIL"] = "buyer".time().GetRandomCode(2)."@".$emServer;

            $arOrder["USER_ID"] = CSaleUser::DoAutoRegisterUser($arUser["EMAIL"], $arUser["NAME"], $this->arParams["SITE_NEW_ORDERS"], $arErrors, array("XML_ID"=>$arOrder["AGENT"]["ID"]));

            $this->updateDealer($arOrder, "add");
          }
        }

        if(empty($arPersonTypesIDs))
        {
          $dbPT = CSalePersonType::GetList(array(), array("ACTIVE" => "Y", "LIDS" => $this->arParams["SITE_NEW_ORDERS"]));
          while($arPT = $dbPT->Fetch())
          {
            $arPersonTypesIDs[] = $arPT["ID"];
          }
        }

        if(empty($arExportInfo))
        {
          $dbExport = CSaleExport::GetList(array(), array("PERSON_TYPE_ID" => $arPersonTypesIDs));
          while($arExport = $dbExport->Fetch())
          {
            $arExportInfo[$arExport["PERSON_TYPE_ID"]] = unserialize($arExport["VARS"]);
          }
        }

        if(IntVal($arOrder["PERSON_TYPE_ID"]) <= 0)
        {
          foreach($arExportInfo as $pt => $value)
          {
            if(
            (($value["IS_FIZ"] == "Y" && $arOrder["AGENT"]["TYPE"] == "FIZ")
                || ($value["IS_FIZ"] == "N" && $arOrder["AGENT"]["TYPE"] != "FIZ"))
            )
              $arOrder["PERSON_TYPE_ID"] = $pt;
          }
        }

        if(IntVal($arOrder["PERSON_TYPE_ID"]) > 0)
        {
          $arAgent = $arExportInfo[$arOrder["PERSON_TYPE_ID"]];
          foreach($arAgent as $k => $v)
          {
            if(empty($v) ||
                (
                    (empty($v["VALUE"]) || $v["TYPE"] != "PROPERTY") &&
                    (empty($arOrder["USER_PROPS"])
                        || (is_array($v) && is_string($v["VALUE"]) && empty($arOrder["USER_PROPS"][$v["VALUE"]]))
                    )
                )
            )
              unset($arAgent[$k]);
          }

          if(IntVal($arOrder["USER_ID"]) > 0)
          {
            $orderFields = array(
                "SITE_ID" => $this->arParams["SITE_NEW_ORDERS"],
                "PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"],
                "PAYED" => "N",
                "CANCELED" => "N",
                "STATUS_ID" => $arOrder["TRAITS"][GetMessage("CC_BSC1_1C_STATUS_ID")],
                "PRICE" => $arOrder["AMOUNT"],
                "CURRENCY" => CSaleLang::GetLangCurrency($this->arParams["SITE_NEW_ORDERS"]),
                "USER_ID" => $arOrder["USER_ID"],
                "TAX_VALUE" => doubleval($arOrder["TAX"]["VALUE_MONEY"]),
                "COMMENTS" => $arOrder["COMMENT"],
                "BASKET_ITEMS" => array(),
                "TAX_LIST" => array(),
                "ORDER_PROP" => array(),
                "XML_ID" => $arOrder["XML_1C_DOCUMENT_ID"]
            );
            $arAditFields = array(
                "EXTERNAL_ORDER" => "Y",
                "ID_1C" => $arOrder["ID_1C"],
                "VERSION_1C" => $arOrder["VERSION_1C"],
                "UPDATED_1C" => "Y",
                "DATE_INSERT" => CDatabase::FormatDate($arOrder["DATE"]." ".$arOrder["TIME"], "YYYY-MM-DD HH:MI:SS", CLang::GetDateFormat("FULL", LANG)),
            );

            $priceDelivery = 0;
            foreach($arOrder["items"] as $productID => $val)
            {
              $orderFields["BASKET_ITEMS"][] = $this->prepareProduct4Basket($productID, $val, false, $orderFields);
              $priceDelivery += $val["PRICE_DELIVERY_ONE"];
            }

            if(!empty($arOrder["TAX"]))
            {
              $orderFields["TAX_LIST"][] = array(
                  "NAME" => $arOrder["TAX"]["NAME"],
                  "IS_PERCENT" => "Y",
                  "VALUE" => $arOrder["TAX"]["VALUE"],
                  "VALUE_MONEY" => $arOrder["TAX"]["VALUE_MONEY"],
                  "IS_IN_PRICE" => $arOrder["TAX"]["IS_IN_PRICE"],
                  "CODE" => 'VAT1C',
                  "APPLY_ORDER" => '100',
              );
            }

            foreach($arAgent as $k => $v)
            {
              if(!empty($arOrder["ORDER_PROPS"][$k]))
              {
                $orderFields["ORDER_PROP"][$v["VALUE"]] = $arOrder["ORDER_PROPS"][$k];
              }
              if(empty($orderFields["ORDER_PROP"][$v["VALUE"]]) && !empty($arOrder["USER_PROPS"][$v["VALUE"]]))
              {
                $orderFields["ORDER_PROP"][$v["VALUE"]] = $arOrder["USER_PROPS"][$v["VALUE"]];
              }
            }

            //обновляем склад и тип доставки
            $res = CCatalogStore::GetList(array("SORT"=>"ASC"), array("ACTIVE" => "Y"), false, false, array("ID", "XML_ID"));
            while($arRes = $res->GetNext()){
              $arStock[$arRes["XML_ID"]] = $arRes["ID"];
            }
            $storeID = $arStock[$arOrder["STORE"]["XML_ID"]];

            switch ($arOrder["TRAITS"][GetMessage("CC_BSC1_TYPE")]) {
              case 'Обычный':
                $deliveryID = 6;
                break;
              case 'Обычный ВО':
              case 'Обычный BO':
                if($storeID == 1){
                  $storeID = 3;
                }
                if($storeID == 2){
                  $storeID = 4;
                }
                $deliveryID = 6;
                break;
              case 'Срочный':
                $deliveryID = 2;
                break;
              case 'Срочный BO':
              case 'Срочный BO':
                if($storeID == 1){
                  $storeID = 3;
                }
                if($storeID == 2){
                  $storeID = 4;
                }
                $deliveryID = 2;
                break;
              case 'Сверхсрочный':
                if($storeID == 1 || $storeID == 2){
                  $deliveryID = 2;
                }else{
                  $deliveryID = 10;
                }
                break;
              case 'Сверхсрочный ВО':
              case 'Сверхсрочный BO':
                if($storeID == 1){
                  $storeID = 3;
                }
                if($storeID == 2){
                  $storeID = 4;
                }
                $deliveryID = 10;
                break;
            }

            $orderFields["DELIVERY_PRICE"] = $priceDelivery;
            $orderFields["DELIVERY_ID"] = $deliveryID;

            if($arOrder["ID"] = CSaleOrder::DoSaveOrder($orderFields, $arAditFields, 0, $arErrors)){
              $arAditFields = array("UPDATED_1C" => "Y");
              CSaleOrder::Update($arOrder["ID"], $arAditFields);
              CSaleOrder::StatusOrder($arOrder["ID"], $orderFields["STATUS_ID"]);

              if($arOrder["CANCELED"] == "true"){
                CSaleOrder::CancelOrder($arOrder["ID"], "Y", $arOrder["COMMENT"]);
              }

              $order = \Bitrix\Sale\Order::load($arOrder["ID"]);

              $shipmentCollection = $order->getShipmentCollection();

              foreach ($shipmentCollection as $shipment){
                if (!$shipment->isSystem()){
                  $shipment->setStoreId($storeID);
                  $deliveryObj = Bitrix\Sale\Delivery\Services\Manager::getObjectById($deliveryID);
                  $shipment->setFields(array(
                    'DELIVERY_NAME' => $deliveryObj->getName(),
                    'CURRENCY'      => $deliveryObj->getCurrency()
                  ));

                  if(strlen($arOrder["TRAITS"][GetMessage("CC_BSC1_1C_DELIVERY_DATE")])>1){
                    $shipment->setFields(array(
                      'ALLOW_DELIVERY'      => "Y",
                      'DATE_ALLOW_DELIVERY' => new Bitrix\Main\Type\DateTime(CDatabase::FormatDate(str_replace("T", " ", $arOrder["TRAITS"][GetMessage("CC_BSC1_1C_DELIVERY_DATE")]), "YYYY-MM-DD HH:MI:SS", CLang::GetDateFormat("FULL", LANG))),
                      'DELIVERY_DOC_DATE'   => new Bitrix\Main\Type\DateTime(CDatabase::FormatDate(str_replace("T", " ", $arOrder["TRAITS"][GetMessage("CC_BSC1_1C_DELIVERY_DATE")]), "YYYY-MM-DD HH:MI:SS", CLang::GetDateFormat("FULL", LANG)))
                    ));

                    $reservedOrder = N;
                  } else {
                    $reservedOrder = Y;
                  }
                }
              }
              $order->save();

              //добавляем в таблицу резерва
              $fuser_id = \Bitrix\Sale\Fuser::getIdByUserId($orderFields["USER_ID"]);

              foreach ($orderFields["BASKET_ITEMS"] as $key => $value) {
                $hr = new HinoReservationPart($value["PRODUCT_ID"]);
                $arFields = array(
                  "PRODUCT_ID"  => $value["PRODUCT_ID"],
                  "FUSER_ID"    => $fuser_id,
                  "STORE_ID"    => $storeID,
                  "DELIVERY_ID" => $deliveryID,
                  "QUANTITY"    => $value["QUANTITY"],
                  "ORDER_ID"    => $arOrder["ID"],
                  "RESERVED"    => $reservedOrder
                );
                $hr->insertReservation($arFields);
              }

              //add/update user profile
              if(IntVal($arOrder["USER_PROFILE_ID"]) > 0)
              {
                if($arOrder["USER_PROFILE_VERSION"] != $arOrder["AGENT"]["VERSION"])
                  CSaleOrderUserProps::Update($arOrder["USER_PROFILE_ID"], array("VERSION_1C" => $arOrder["AGENT"]["VERSION"], "NAME" => $arOrder["AGENT"]["AGENT_NAME"], "USER_ID" => $arOrder["USER_ID"]));
                $dbUPV = CSaleOrderUserPropsValue::GetList(array(), array("USER_PROPS_ID" =>$arOrder["USER_PROFILE_ID"]));
                while($arUPV = $dbUPV->Fetch())
                {
                  $arOrder["AGENT"]["PROFILE_PROPS_VALUE"][$arUPV["ORDER_PROPS_ID"]] = array("ID" => $arUPV["ID"], "VALUE" => $arUPV["VALUE"]);
                }
              }

              if(IntVal($arOrder["USER_PROFILE_ID"]) <= 0 || (IntVal($arOrder["USER_PROFILE_ID"]) > 0 && $arOrder["USER_PROFILE_VERSION"] != $arOrder["AGENT"]["VERSION"]))
              {
                $dbOrderProperties = CSaleOrderProps::GetList(
                    array("SORT" => "ASC"),
                    array(
                        "PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"],
                        "ACTIVE" => "Y",
                        "UTIL" => "N",
                        "USER_PROPS" => "Y",
                    ),
                    false,
                    false,
                    array("ID", "TYPE", "NAME", "CODE", "USER_PROPS", "SORT", "MULTIPLE")
                );
                while ($arOrderProperties = $dbOrderProperties->Fetch())
                {
                  $curVal = $orderFields["ORDER_PROP"][$arOrderProperties["ID"]];

                  if (strlen($curVal) > 0)
                  {
                    if (IntVal($arOrder["USER_PROFILE_ID"]) <= 0)
                    {
                      $arFields = array(
                          "NAME" => $arOrder["AGENT"]["AGENT_NAME"],
                          "USER_ID" => $arOrder["USER_ID"],
                          "PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"],
                          "XML_ID" => $arOrder["AGENT"]["ID"],
                          "VERSION_1C" => $arOrder["AGENT"]["VERSION"],
                      );
                      $arOrder["USER_PROFILE_ID"] = CSaleOrderUserProps::Add($arFields);
                    }
                    if(IntVal($arOrder["USER_PROFILE_ID"]) > 0)
                    {
                      $arFields = array(
                          "USER_PROPS_ID" => $arOrder["USER_PROFILE_ID"],
                          "ORDER_PROPS_ID" => $arOrderProperties["ID"],
                          "NAME" => $arOrderProperties["NAME"],
                          "VALUE" => $curVal
                      );
                      if(empty($arOrder["AGENT"]["PROFILE_PROPS_VALUE"][$arOrderProperties["ID"]]))
                      {
                        CSaleOrderUserPropsValue::Add($arFields);
                      }
                      elseif($arOrder["AGENT"]["PROFILE_PROPS_VALUE"][$arOrderProperties["ID"]]["VALUE"] != $curVal)
                      {
                        CSaleOrderUserPropsValue::Update($arOrder["AGENT"]["PROFILE_PROPS_VALUE"][$arOrderProperties["ID"]]["ID"], $arFields);
                      }
                    }
                  }
                }
              }
            }
            else
            {
              $this->strError .= "\n".GetMessage("CC_BSC1_ORDER_ADD_PROBLEM", Array("#ID#" => $arOrder["ID_1C"]));
              if(is_array($arErrors))
                $this->strError .= "\n".implode(', ',$arErrors);
            }
          }
          else
          {
            $this->strError .= "\n".GetMessage("CC_BSC1_ORDER_USER_PROBLEM", Array("#ID#" => $arOrder["ID_1C"]));
            if(!empty($arErrors))
            {
              foreach($arErrors as $v)
              {
                $this->strError .= "\n".$v["TEXT"];
              }
            }
          }
        }
        else
        {
          $this->strError .= "\n".GetMessage("CC_BSC1_ORDER_PERSON_TYPE_PROBLEM", Array("#ID#" => $arOrder["ID_1C"]));
        }
      }
      else
      {
        $this->strError .= "\n".GetMessage("CC_BSC1_ORDER_NO_AGENT_ID", Array("#ID#" => $arOrder["ID_1C"]));
      }
    }
  }

}
