<?
CModule::IncludeModule("sale");

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class CSaleExportHino extends CSaleExport
{
  function writeError($mess) {
    $mess = date("Y-m-d H:i:s")."\n".$mess;
    $mess .= "\n--------------------------------------------------------------";
    Bitrix\Main\Diag\Debug::writeToFile($mess, '', "/_logs/integration1c/".date("j_F_Y")."_export.log");
  }

  function ExportOrders2Xml($arFilter = Array(), $nTopCount = 0, $currency = "", $crmMode = false, $time_limit = 0, $version = false, $arOptions = Array())
  {
    self::setVersionSchema($version);
    self::setCrmMode($crmMode);
    self::setCurrencySchema($currency);

    $count = false;
    if(IntVal($nTopCount) > 0)
      $count = Array("nTopCount" => $nTopCount);

    $end_time = self::getEndTime($time_limit);

    if(IntVal($time_limit) > 0)
    {
      $lastOrderPrefix = self::getOrderPrefix();

      if(self::$crmMode)
      {
        $lastOrderPrefix = md5(serialize($arFilter));
        if(!empty($_SESSION["BX_CML2_EXPORT"][$lastOrderPrefix]) && IntVal($nTopCount) > 0)
          $count["nTopCount"] = $count["nTopCount"]+count($_SESSION["BX_CML2_EXPORT"][$lastOrderPrefix]);
      }
      else
      {
        if(IntVal($_SESSION["BX_CML2_EXPORT"][$lastOrderPrefix]) > 0)
        {
          $arFilter["<ID"] = $_SESSION["BX_CML2_EXPORT"][$lastOrderPrefix];
        }
      }
    }

    self::$arResultStat = array(
      "ORDERS" => 0,
      "CONTACTS" => 0,
      "COMPANIES" => 0,
    );

    $bExportFromCrm = self::isExportFromCRM($arOptions);
    $arStore = self::getCatalogStore();
    $arMeasures = self::getCatalogMeasure();
    self::setCatalogMeasure($arMeasures);
    $arAgent = self::getSaleExport();

    echo self::getXmlRootName();?>

    <<?=CSaleExport::getTagName("SALE_EXPORT_COM_INFORMATION")?> <?=self::getCmrXmlRootNameParams()?>><?

    $arOrder = array("ID" => "DESC");

    if (self::$crmMode)
      $arOrder = array("DATE_UPDATE" => "ASC");

    $arSelect = array(
      "ID", "LID", "PERSON_TYPE_ID", "PAYED", "DATE_PAYED", "EMP_PAYED_ID", "CANCELED", "DATE_CANCELED",
      "EMP_CANCELED_ID", "REASON_CANCELED", "STATUS_ID", "DATE_STATUS", "PAY_VOUCHER_NUM", "PAY_VOUCHER_DATE", "EMP_STATUS_ID",
      "PRICE_DELIVERY", "ALLOW_DELIVERY", "DATE_ALLOW_DELIVERY", "EMP_ALLOW_DELIVERY_ID", "PRICE", "CURRENCY", "DISCOUNT_VALUE",
      "SUM_PAID", "USER_ID", "PAY_SYSTEM_ID", "DELIVERY_ID", "DATE_INSERT", "DATE_INSERT_FORMAT", "DATE_UPDATE", "USER_DESCRIPTION",
      "ADDITIONAL_INFO",
      "COMMENTS", "TAX_VALUE", "STAT_GID", "RECURRING_ID", "ACCOUNT_NUMBER", "SUM_PAID", "DELIVERY_DOC_DATE", "DELIVERY_DOC_NUM", "TRACKING_NUMBER", "STORE_ID",
      "ID_1C", "VERSION",
      "USER.XML_ID"
    );

    $bCrmModuleIncluded = false;
    if ($bExportFromCrm)
    {
      $arSelect[] = "UF_COMPANY_ID";
      $arSelect[] = "UF_CONTACT_ID";
      if (IsModuleInstalled("crm") && CModule::IncludeModule("crm"))
        $bCrmModuleIncluded = true;
    }

    $filter = array(
      'select' => $arSelect,
      'filter' => $arFilter,
      'order'  => $arOrder,
      'limit'  => $count["nTopCount"]
    );

    if (!empty($arOptions['RUNTIME']) && is_array($arOptions['RUNTIME']))
    {
      $filter['runtime'] = $arOptions['RUNTIME'];
    }

    $dbOrderList = \Bitrix\Sale\Internals\OrderTable::getList($filter);

    while($arOrder = $dbOrderList->Fetch())
    {
      $order = \Bitrix\Sale\Order::load($arOrder['ID']);
      /* @var $order \Bitrix\Sale\Order*/
      $basket = $order->getBasket();

      $arOrder['ORDER_WEIGHT'] = $basket->getWeight(); // общий вес корзины

      //уведомление о перевесе
      if($arOrder['ORDER_WEIGHT'] >= 50000 && empty($arOrder['ID_1C'])){
        $arSelect = Array("ID", "PROPERTY_MANAGER");
        $arFilter = Array("IBLOCK_ID" => IBLOCK_DEALERS, "ACTIVE"=>"Y", "CONTRAGENT_ID" => $arOrder["USER_ID"]);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        if($ob = $res->GetNext()){
          $managerUserID = $ob["PROPERTY_MANAGER_VALUE"];
        }

        $managerUserID = $managerUserID? $managerUserID: MANAGER_DEFAULT;
        $manager = CUser::GetByID($managerUserID);
        $resultManager = $manager->Fetch();
        CEvent::Send(
          "OVERLOAD_ORDER",
          's1',
          array(
            "EMAIL"  => $resultManager["EMAIL"],
            "NUMBER" => $arOrder["ID"]
          )
        );
      }

      $arOrder['DATE_STATUS']  = $arOrder['DATE_STATUS']->toString();
      $arOrder['DATE_INSERT']  = $arOrder['DATE_INSERT']->toString();
      $arOrder['DATE_UPDATE']  = $arOrder['DATE_UPDATE']->toString();

      foreach($arOrder as $field=>$value)
      {
        if(self::isFormattedDateFields('Order', $field))
        {
          $arOrder[$field] = self::getFormatDate($value);
        }
      }

      if (self::$crmMode)
      {
        if(self::getVersionSchema() > self::DEFAULT_VERSION && is_array($_SESSION["BX_CML2_EXPORT"][$lastOrderPrefix]) && in_array($arOrder["ID"], $_SESSION["BX_CML2_EXPORT"][$lastOrderPrefix]) && empty($arFilter["ID"]))
          continue;
        ob_start();
      }

      self::$arResultStat["ORDERS"]++;

      $agentParams = (array_key_exists($arOrder["PERSON_TYPE_ID"], $arAgent) ? $arAgent[$arOrder["PERSON_TYPE_ID"]] : array() );

      $arResultPayment = self::getPayment($arOrder);
      $paySystems = $arResultPayment['paySystems'];
      $arPayment = $arResultPayment['payment'];

      $arResultShipment = self::getShipment($arOrder);
      $arShipment = $arResultShipment['shipment'];
      $delivery = $arResultShipment['deliveryServices'];

      self::setDeliveryAddress('');
      self::setSiteNameByOrder($arOrder);

      $saleProperties = self::getSaleProperties($order, $arOrder, $agentParams, $bExportFromCrm, $bCrmModuleIncluded, $paySystems, $delivery, $arOptions);

      $arProp = $saleProperties['arProp'];
      $agent = $saleProperties['agent'];

      //Наименование
      $db_sales = CSaleOrderUserProps::GetList(array("DATE_UPDATE" => "DESC"), array("USER_ID" => $arOrder['USER_ID']));
      $ar_sales = $db_sales->Fetch();
      $agent['AGENT_NAME'] = $ar_sales['NAME'];

      //ОфициальноеНаименование
      $arSelect = Array("NAME");
      $arFilter = Array("IBLOCK_ID" => IBLOCK_DEALERS, "ACTIVE"=>"Y", "CONTRAGENT_ID" => $arOrder["USER_ID"]);
      $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

      if($ob = $res->GetNext()){
        $agent['FULL_NAME'] = $ob['~NAME'];
      }

      $arOrderTax = CSaleExport::getOrderTax($arOrder["ID"]);
      $xmlResult['OrderTax'] = self::getXMLOrderTax($arOrderTax);
      self::setOrderSumTaxMoney(self::getOrderSumTaxMoney($arOrderTax));

      $xmlResult['Contragents'] = self::getXmlContragents($arOrder, $arProp, $agent, $bExportFromCrm ? array("EXPORT_FROM_CRM" => "Y") : array());
      $xmlResult['OrderDiscount'] = self::getXmlOrderDiscount($arOrder);
      $xmlResult['SaleStoreList'] = $arStore;

      //получение id склада из таблицы резервирования
      $storeID = HinoReservationPart::getStoreOrder($arOrder["ID"]);

      $arOrder["PRICE_DELIVERY"] = 0; //обнуление цены доставки, чтобы он не добавлял доставку как товар

      $basketItems = self::getXmlBasketItems('Order', $arOrder, array('ORDER_ID'=>$arOrder['ID']), array(), $arShipment);
      $xmlResult['BasketItems'] = $basketItems['outputXML'];

      //подмена реквизита Тип заказа
      foreach ($agent["REKV"] as $key => &$value) {
        if($agentParams[$key]["VALUE"] == "DELIVERY_NAME" && $agentParams[$key]["TYPE"] == "SHIPMENT"){
          switch ($value) {
            case "Обычный":
              if($storeID == 3 || $storeID == 4 || $storeID === "0") $value = "Обычный BO";
              break;
            case "Срочный":
              if($storeID == 1 || $storeID == 2) $value = "Сверхсрочный";
              if($storeID == 3 || $storeID == 4 || $storeID === "0") $value = "Срочный BO";
              break;
            case "Сверхсрочный":
              if($storeID == 3 || $storeID == 4 || $storeID === "0") $value = "Сверхсрочный ВО";
              break;
          }
        }
      }

      //подмена склада
      $xmlResult['ShipmentsStoreList'] = array();
      switch ($storeID) {
        case 3:
          $newStoreID = 1;
          break;
        case "0":
        case 4:
          $newStoreID = 2;
          break;
        default:
          $newStoreID = $storeID;
          break;
      }
      $xmlResult['ShipmentsStoreList'][$arShipment[0]["ID"]] = $newStoreID;

      $xmlResult['SaleProperties'] = self::getXmlSaleProperties($arOrder, $arShipment, $arPayment, $agent, $agentParams, $bExportFromCrm);

      self::OutputXmlDocument('Order', $xmlResult, $arOrder);


      if(self::getVersionSchema() >= self::PARTIAL_VERSION)
      {
        self::OutputXmlDocumentsByType('Payment',$xmlResult,$arOrder, $arPayment);
        self::OutputXmlDocumentsByType('Shipment',$xmlResult,$arOrder, $arShipment);
        self::OutputXmlDocumentRemove('Shipment',$arOrder);
      }

      if (self::$crmMode)
      {
        $c = ob_get_clean();
        $c = CharsetConverter::ConvertCharset($c, $arCharSets[$arOrder["LID"]], "utf-8");
        echo $c;
        $_SESSION["BX_CML2_EXPORT"][$lastOrderPrefix][] = $arOrder["ID"];
      }
      else
      {
        $_SESSION["BX_CML2_EXPORT"][$lastOrderPrefix] = $arOrder["ID"];
      }

      if(self::checkTimeIsOver($time_limit,$end_time))
      {
        break;
      }
    }
    ?></<?=CSaleExport::getTagName("SALE_EXPORT_COM_INFORMATION")?>><?

    return self::$arResultStat;
  }

  function getXmlBasketItems($type, $arOrder, $arFilter, $arSelect=array(), $arShipment=array())
  {
    $bufer = '';
    $result = array();
    ob_start();
    ?><<?=CSaleExport::getTagName("SALE_EXPORT_ITEMS")?>><?

    $select = array("ID", "NOTES", "PRODUCT_XML_ID", "CATALOG_XML_ID", "NAME", "WEIGHT", "PRICE", "BASE_PRICE", "QUANTITY", "DISCOUNT_PRICE", "DISCOUNT_VALUE", "VAT_RATE", "MEASURE_CODE", "SET_PARENT_ID", "TYPE");
    if(count($arSelect)>0)
        $select = array_merge($arSelect,$select);

    $dbBasket = \Bitrix\Sale\Internals\BasketTable::getList(array(
      'select' => $select,
      'filter' => $arFilter,
      'order' => array("NAME" => "ASC")
    ));

    $basketSum = 0;
    $priceType = "";
    $bVat = false;
    $vatRate = 0;
    $vatSum = 0;
    while ($arBasket = $dbBasket->fetch())
    {
      if(strval($arBasket['TYPE'])!='' && $arBasket['TYPE']== \Bitrix\Sale\BasketItemBase::TYPE_SET)
          continue;

      $result[] = $arBasket;

      if(strlen($priceType) <= 0)
        $priceType = $arBasket["NOTES"];
      ?>
      <<?=CSaleExport::getTagName("SALE_EXPORT_ITEM")?>>
        <<?=CSaleExport::getTagName("SALE_EXPORT_ID")?>><?=htmlspecialcharsbx($arBasket["PRODUCT_XML_ID"])?></<?=CSaleExport::getTagName("SALE_EXPORT_ID")?>>
        <<?=CSaleExport::getTagName("SALE_EXPORT_CATALOG_ID")?>><?=htmlspecialcharsbx($arBasket["CATALOG_XML_ID"])?></<?=CSaleExport::getTagName("SALE_EXPORT_CATALOG_ID")?>>
        <<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>><?=htmlspecialcharsbx($arBasket["NAME"])?></<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>>
        <?
        if(self::getVersionSchema() > self::DEFAULT_VERSION)
        {
          if(IntVal($arBasket["MEASURE_CODE"]) <= 0)
            $arBasket["MEASURE_CODE"] = 796;
          ?>
          <<?=CSaleExport::getTagName("SALE_EXPORT_UNIT")?>>
            <<?=CSaleExport::getTagName("SALE_EXPORT_CODE")?>><?=$arBasket["MEASURE_CODE"]?></<?=CSaleExport::getTagName("SALE_EXPORT_CODE")?>>
            <<?=CSaleExport::getTagName("SALE_EXPORT_FULL_NAME_UNIT")?>><?=htmlspecialcharsbx(self::$measures[$arBasket["MEASURE_CODE"]])?></<?=CSaleExport::getTagName("SALE_EXPORT_FULL_NAME_UNIT")?>>
          </<?=CSaleExport::getTagName("SALE_EXPORT_UNIT")?>>
          <<?=CSaleExport::getTagName("SALE_EXPORT_KOEF")?>>1</<?=CSaleExport::getTagName("SALE_EXPORT_KOEF")?>>
          <?
        }
        else
        {
          ?>
          <<?=CSaleExport::getTagName("SALE_EXPORT_BASE_UNIT")?> <?=CSaleExport::getTagName("SALE_EXPORT_CODE")?>="796" <?=CSaleExport::getTagName("SALE_EXPORT_FULL_NAME_UNIT")?>="<?=CSaleExport::getTagName("SALE_EXPORT_SHTUKA")?>" <?=CSaleExport::getTagName("SALE_EXPORT_INTERNATIONAL_ABR")?>="<?=CSaleExport::getTagName("SALE_EXPORT_RCE")?>"><?=CSaleExport::getTagName("SALE_EXPORT_SHT")?></<?=CSaleExport::getTagName("SALE_EXPORT_BASE_UNIT")?>>
          <?
        }

        $arDiscount = array();
        // обычная скидка
        if(DoubleVal($arBasket["DISCOUNT_PRICE"]) > 0){
          $arDiscount[] = array(
            "NAME"           => "Скидка",
            "DISCOUNT_PRICE" => $arBasket["DISCOUNT_PRICE"],
            "DISCOUNT_VALUE" => round(DoubleVal($arBasket["DISCOUNT_VALUE"]), 2),
            "IN_PRICE"       => "true"
          );
        }

        // пропорциональная Транспортная наценка
        if(DoubleVal($arShipment[0]["PRICE_DELIVERY"]) > 0){
          $dsValue = ($arBasket["WEIGHT"]*$arBasket["QUANTITY"]*100)/$arOrder["ORDER_WEIGHT"];
          $DISCOUNT_PRICE = $arShipment[0]["PRICE_DELIVERY"]*($dsValue/100);
          $DISCOUNT_VALUE = (100*$DISCOUNT_PRICE)/($arBasket["PRICE"]*$arBasket["QUANTITY"]);

          $DISCOUNT_PRICE = round($DISCOUNT_PRICE, 2);
          $DISCOUNT_VALUE = round($DISCOUNT_VALUE, 2);

          if(!is_nan($DISCOUNT_PRICE) && !is_nan($DISCOUNT_VALUE) && $DISCOUNT_VALUE > 0 && $DISCOUNT_PRICE > 0){
            $arDiscount[] = array(
              "NAME"           => "Транспортная наценка",
              "DISCOUNT_PRICE" => $DISCOUNT_PRICE,
              "DISCOUNT_VALUE" => $DISCOUNT_VALUE,
              "IN_PRICE"       => "false"
            );
          }
        }
        ?>
        <?if(!empty($arDiscount)):?>
          <<?=CSaleExport::getTagName("SALE_EXPORT_DISCOUNTS")?>>
          <?foreach ($arDiscount as $discountItem):?>
              <<?=CSaleExport::getTagName("SALE_EXPORT_DISCOUNT")?>>
                <<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>><?=$discountItem["NAME"]?></<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>>
                <<?=CSaleExport::getTagName("SALE_EXPORT_AMOUNT")?>><?=$discountItem["DISCOUNT_PRICE"]?></<?=CSaleExport::getTagName("SALE_EXPORT_AMOUNT")?>>
                <Процент><?=$discountItem["DISCOUNT_VALUE"]?></Процент>
                <<?=CSaleExport::getTagName("SALE_EXPORT_IN_PRICE")?>><?=$discountItem["IN_PRICE"]?></<?=CSaleExport::getTagName("SALE_EXPORT_IN_PRICE")?>>
              </<?=CSaleExport::getTagName("SALE_EXPORT_DISCOUNT")?>>
          <?endforeach;?>
          </<?=CSaleExport::getTagName("SALE_EXPORT_DISCOUNTS")?>>
        <?endif;?>

        <?if(self::getVersionSchema() >= self::PARTIAL_VERSION && $type == 'Shipment')
        {?>
        <<?=CSaleExport::getTagName("SALE_EXPORT_PRICE_PER_ITEM")?>><?=$arBasket["BASE_PRICE"]?></<?=CSaleExport::getTagName("SALE_EXPORT_PRICE_PER_ITEM")?>>
        <<?=CSaleExport::getTagName("SALE_EXPORT_QUANTITY")?>><?=$arBasket["SALE_INTERNALS_BASKET_SHIPMENT_ITEM_QUANTITY"]?></<?=CSaleExport::getTagName("SALE_EXPORT_QUANTITY")?>>
        <<?=CSaleExport::getTagName("SALE_EXPORT_AMOUNT")?>><?=$arBasket["BASE_PRICE"]*$arBasket["SALE_INTERNALS_BASKET_SHIPMENT_ITEM_QUANTITY"]?></<?=CSaleExport::getTagName("SALE_EXPORT_AMOUNT")?>>
        <?}
        else{
        ?>
        <<?=CSaleExport::getTagName("SALE_EXPORT_PRICE_PER_ITEM")?>><?=$arBasket["BASE_PRICE"]?></<?=CSaleExport::getTagName("SALE_EXPORT_PRICE_PER_ITEM")?>>
        <<?=CSaleExport::getTagName("SALE_EXPORT_QUANTITY")?>><?=$arBasket["QUANTITY"]?></<?=CSaleExport::getTagName("SALE_EXPORT_QUANTITY")?>>
        <<?=CSaleExport::getTagName("SALE_EXPORT_AMOUNT")?>><?=$arBasket["BASE_PRICE"]*$arBasket["QUANTITY"]?></<?=CSaleExport::getTagName("SALE_EXPORT_AMOUNT")?>>
        <?}?>
        <<?=CSaleExport::getTagName("SALE_EXPORT_PROPERTIES_VALUES")?>>
          <<?=CSaleExport::getTagName("SALE_EXPORT_PROPERTY_VALUE")?>>
            <<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>><?=CSaleExport::getTagName("SALE_EXPORT_TYPE_NOMENKLATURA")?></<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>>
            <<?=CSaleExport::getTagName("SALE_EXPORT_VALUE")?>><?=CSaleExport::getTagName("SALE_EXPORT_ITEM")?></<?=CSaleExport::getTagName("SALE_EXPORT_VALUE")?>>
          </<?=CSaleExport::getTagName("SALE_EXPORT_PROPERTY_VALUE")?>>
          <<?=CSaleExport::getTagName("SALE_EXPORT_PROPERTY_VALUE")?>>
            <<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>><?=CSaleExport::getTagName("SALE_EXPORT_TYPE_OF_NOMENKLATURA")?></<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>>
            <<?=CSaleExport::getTagName("SALE_EXPORT_VALUE")?>><?=CSaleExport::getTagName("SALE_EXPORT_ITEM")?></<?=CSaleExport::getTagName("SALE_EXPORT_VALUE")?>>
          </<?=CSaleExport::getTagName("SALE_EXPORT_PROPERTY_VALUE")?>>
          <?
          $dbProp = CSaleBasket::GetPropsList(Array("SORT" => "ASC", "ID" => "ASC"), Array("BASKET_ID" => $arBasket["ID"]), false, false, array("NAME", "VALUE", "CODE"));
          while($arPropBasket = $dbProp->Fetch())
          {
            ?>
            <<?=CSaleExport::getTagName("SALE_EXPORT_PROPERTY_VALUE")?>>
              <<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>><?=CSaleExport::getTagName("SALE_EXPORT_PROPERTY_VALUE_BASKET")?>#<?=($arPropBasket["CODE"] != "" ? $arPropBasket["CODE"]:htmlspecialcharsbx($arPropBasket["NAME"]))?></<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>>
              <<?=CSaleExport::getTagName("SALE_EXPORT_VALUE")?>><?=htmlspecialcharsbx($arPropBasket["VALUE"])?></<?=CSaleExport::getTagName("SALE_EXPORT_VALUE")?>>
            </<?=CSaleExport::getTagName("SALE_EXPORT_PROPERTY_VALUE")?>>
            <?
          }
          ?>
        </<?=CSaleExport::getTagName("SALE_EXPORT_PROPERTIES_VALUES")?>>
        <?if(DoubleVal($arBasket["VAT_RATE"]) > 0)
        {
          $bVat = true;
          $vatRate = DoubleVal($arBasket["VAT_RATE"]);
          $basketVatSum = (($arBasket["PRICE"] / ($arBasket["VAT_RATE"]+1)) * $arBasket["VAT_RATE"]);
          $vatSum += roundEx($basketVatSum * $arBasket["QUANTITY"], 2);
          ?>
          <<?=CSaleExport::getTagName("SALE_EXPORT_TAX_RATES")?>>
            <<?=CSaleExport::getTagName("SALE_EXPORT_TAX_RATE")?>>
              <<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>><?=CSaleExport::getTagName("SALE_EXPORT_VAT")?></<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>>
              <<?=CSaleExport::getTagName("SALE_EXPORT_RATE")?>><?=$arBasket["VAT_RATE"] * 100?></<?=CSaleExport::getTagName("SALE_EXPORT_RATE")?>>
            </<?=CSaleExport::getTagName("SALE_EXPORT_TAX_RATE")?>>
          </<?=CSaleExport::getTagName("SALE_EXPORT_TAX_RATES")?>>
          <<?=CSaleExport::getTagName("SALE_EXPORT_TAXES")?>>
            <<?=CSaleExport::getTagName("SALE_EXPORT_TAX")?>>
              <<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>><?=CSaleExport::getTagName("SALE_EXPORT_VAT")?></<?=CSaleExport::getTagName("SALE_EXPORT_ITEM_NAME")?>>
              <<?=CSaleExport::getTagName("SALE_EXPORT_IN_PRICE")?>>true</<?=CSaleExport::getTagName("SALE_EXPORT_IN_PRICE")?>>
              <<?=CSaleExport::getTagName("SALE_EXPORT_AMOUNT")?>><?=roundEx($basketVatSum, 2)?></<?=CSaleExport::getTagName("SALE_EXPORT_AMOUNT")?>>
            </<?=CSaleExport::getTagName("SALE_EXPORT_TAX")?>>
          </<?=CSaleExport::getTagName("SALE_EXPORT_TAXES")?>>
          <?
        }
        ?>
        <?//=self::getXmlSaleStoreBasket($arOrder,$arStore)?>
      </<?=CSaleExport::getTagName("SALE_EXPORT_ITEM")?>>
      <?
      $basketSum += $arBasket["PRICE"]*$arBasket["QUANTITY"];
    }

    /*
    if(self::getVersionSchema() >= self::PARTIAL_VERSION)
    {
        if(count($arShipment)>0)
        {
            foreach($arShipment as $shipment)
            {
                self::getOrderDeliveryItem($shipment, $bVat, $vatRate, $vatSum);
            }
        }
    }
    else
    self::getOrderDeliveryItem($arOrder, $bVat, $vatRate, $vatSum);
    */

    ?>
    </<?=CSaleExport::getTagName("SALE_EXPORT_ITEMS")?>><?

    $bufer = ob_get_clean();
    return array('outputXML'=>$bufer,'result'=>$result);
  }
}
