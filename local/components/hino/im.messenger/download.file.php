<?
define("IM_AJAX_INIT", true);
define("PUBLIC_AJAX_MODE", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC","Y");
define("NO_AGENT_CHECK", true);
define("DisableEventsCheck", true);
define("STOP_STATISTICS", true);

if(isset($_GET['action']) && $_GET['action'] == 'showFile'){
  define('BX_SECURITY_SESSION_READONLY', true);
}


require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if($_GET['action'] == 'showFile'){
  $fileId = intval($_GET['fileId']);
  $arFile = CFile::GetFileArray($fileId);

  header('Content-type:'.$arFile["CONTENT_TYPE"]);

  if($_GET['previewImg'] == '1'){
    if($arFile["HEIGHT"] > 120 || $arFile["WIDTH"] > 450){
      $arFileTmp = CFile::ResizeImageGet(
        $fileId,
        array("width" => 450, "height" => 120),
        BX_RESIZE_IMAGE_PROPORTIONAL
      );
      readfile($_SERVER["DOCUMENT_ROOT"].$arFileTmp["src"]);
    }else{
      readfile($_SERVER["DOCUMENT_ROOT"].$arFile["SRC"]);
    }
  }else{
    readfile($_SERVER["DOCUMENT_ROOT"].$arFile["SRC"]);
  }

  die();
}else if($_GET['action'] == 'downloadFile'){
  $fileId = intval($_GET['fileId']);
  $arFile = CFile::GetFileArray($fileId);

  header("Content-type:".$arFile["CONTENT_TYPE"]);
  header("Cache-Control: no-store, no-cache");
  header('Content-Disposition: attachment; filename="'.$arFile["ORIGINAL_NAME"].'"');
  // header('Content-Length: '.$arFile["FILE_SIZE"]);

  readfile($_SERVER["DOCUMENT_ROOT"].$arFile["SRC"]);
  die();
}
