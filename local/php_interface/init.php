<?
define("NEED_AUTH", true);

include_once "const.inc.php";
include_once "classes.inc.php";

AddEventHandler("iblock", "OnIBlockPropertyBuildList", Array("CIBlockPropertyStock", "GetUserTypeDescription"));
AddEventHandler("subscribe", "BeforePostingSendMail", Array("UnSubscribe", "BeforePostingSendMailHandler"));

AddEventHandler("main", "OnAfterUserAdd", Array("HinoUsers", "OnAfterUserEditHandler"));
AddEventHandler("main", "OnAfterUserUpdate", Array("HinoUsers", "OnAfterUserEditHandler"));
AddEventHandler("main", "OnUserDelete", Array("HinoUsers", "OnUserDeleteHandler"));

//заказы
AddEventHandler("sale", "OnOrderDelete", Array("HinoOrders", "removeReservationParts"));

//корзина
AddEventHandler("sale", "OnBeforeBasketDelete", Array("HinoHelper", "removeReservationPartBasket"));

//скидки
AddEventHandler("catalog", "OnGetDiscount", array("HinoDiscount", "OnGetDiscount"));
AddEventHandler("catalog", "OnGetDiscountResult", array("HinoDiscount", "OnGetDiscountResult"));

//изменение инфоблоков
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("HinoHelper", "OnAfterIBlockElementUpdateHandler"));

//агенты Bitrix
include_once "agent.inc.php";
?>
