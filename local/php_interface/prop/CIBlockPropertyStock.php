<?
class CIBlockPropertyStock
{
  function GetUserTypeDescription() {
    return array(
      "PROPERTY_TYPE"        => "S",
      "USER_TYPE"            => "DEALER_STOCK",
      "DESCRIPTION"          => "Привязка к складам HINO",
      "GetPropertyFieldHtml" => array("CIBlockPropertyStock", "GetPropertyFieldHtml"),
      "ConvertToDB"          => array("CIBlockPropertyStock", "ConvertToDB"),
      "ConvertFromDB"        => array("CIBlockPropertyStock", "ConvertFromDB")
    );
  }

  //формируем пару полей для создаваемого свойства
  function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
    global $APPLICATION;
    $APPLICATION->AddHeadScript('/js/plugins/jquery-3.1.1.min.js');
    $APPLICATION->AddHeadScript('/js/plugins/jquery-ui/jquery-ui.min.js');

    $res = CCatalogStore::GetList(array("SORT"=>"ASC"), array("ACTIVE" => "Y"), false, false, array("ID", "SORT", "TITLE"));
    echo
    '
    <style>
      .sortable-stock{
        list-style: none;
        padding: 0;
        margin: 0;
        max-width: 300px;
        width: 100%;
      }
      .sortable-stock__item{
        padding: 7px 5px;
        border-width: 1px;
        border-style: solid;
        border-color: #87919c #959ea9 #9ea7b1 #959ea9;
        margin: 4px 0;
        background: #f5f9f9;
      }
    </style>
    <script>
      $( function() {
        $( ".sortable-stock" ).sortable({
          stop: function( event, ui ) {
            var arrValue = $(this).sortable("toArray", {"attribute": "data-stock-id"});
            $(\'[name="'.$strHTMLControlName["VALUE"].'"]\').val(JSON.stringify(arrValue));
          }
        });
      } );
    </script>';
    while($arRes = $res->GetNext()){
      $arStock[] = $arRes["ID"];
      $arTitle[$arRes["ID"]] = $arRes["TITLE"];
    }


    if(empty($value["VALUE"])){
      $arSort = $arStock;
    }else{

      $arDiff = array_diff($arStock, $value["VALUE"]);
      $arMerge = array_merge($value["VALUE"], $arDiff);

      foreach($arMerge as $val){
        if(in_array($val, $arStock)){
          $arSort[] = $val;
        }
      }
    }

    echo '<ul class="sortable-stock">';
    foreach ($arSort as $sortID) {
      echo '<li class="sortable-stock__item" data-stock-id="'.$sortID.'">'.$arTitle[$sortID].'</li>';
    }
    echo "</ul>";
    echo '<input type="hidden" name="'.$strHTMLControlName["VALUE"].'" value="'.CUtil::PhpToJSObject($value["VALUE"]).'">';
  }

  //сохраняем в базу
  function ConvertToDB($arProperty, $value){
    return $value;
  }

  //читаем из базы
  function ConvertFromDB($arProperty, $value){
    $value["VALUE"] = str_replace("'", '"', $value["VALUE"]);
    $value["VALUE"] = json_decode($value["VALUE"]);
    return $value;
  }
}
