<?
//оповещение о завершение доверенности
function checkUserActive(){
  $nextmonth = mktime(0, 0, 0, date("m")+1, date("d"), date("Y"));
  $currentmonth = mktime(0, 0, 0, date("m")+1, date("d"), date("Y"));

  $res = CUser::GetList(($by="name"), ($order="asc"), array("GROUPS_ID" => array(EMPLOYEES_DEALERS)), array("SELECT" => array("UF_*")));
  while($employee = $res->NavNext()) {
    if(!empty($employee["UF_PROXY_DATE_END"])){
      $dateend = strtotime($employee["UF_PROXY_DATE_END"]);
      $diffdate = $nextmonth - $dateend;

      if($diffdate >= 0 && $diffdate <= 86400){
        $manager = HinoUsers::getManager($employee["ID"]);
        Bitrix\Main\Mail\Event::send(array(
            "EVENT_NAME" => "END_OF_PROXY",
            "LID"        => "s1",
            "C_FIELDS" => array(
              "USER_EMAIL"     => $employee["EMAIL"],
              "USER_EMAIL_ВСС" => $manager["EMAIL"],
              "NAME"           => $employee["NAME"]." ".$employee["LAST_NAME"],
              "DATE_END"       => $employee["UF_PROXY_DATE_END"]
            ),
        ));
      }
      //блокировка пользователей
      if($currentmonth >= $dateend && $employee["ACTIVE"] == "Y"){
        $user = new CUser;
        $user->Update($employee["ID"], array("ACTIVE"=> "N"));
      }
    }
  }
  return "checkUserActive();";
}
