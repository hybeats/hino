<?
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/_logs/".date("F_Y")."_log.log");

define("IBLOCK_DEALERS", "3"); // группы пользователей для новых контрагентов
define("IBLOCK_PARTS", "4"); // торговый каталог с запчастями
define("HIBLOCK_AGREEMENTS", "1"); // Highload Block с договорами
define("IBLOCK_DOCS_TEMPLATES", "5"); // инфоблок с шаблонами документов
define("DEALERS_GROUP_ID", "8"); // id группы пользователей "сотрудники дилера"

define("GENERAL_CHAT_ID", "15"); // id общей группы в чате

define("CONTRAGENTS_HINO", "7");  // Группа Контрагенты 1С [7]
define("EMPLOYEES_DEALERS", "8"); // Группа Сотрудники дилеров [8]
define("EMPLOYEES_HINO", "9");    // Группа Сотрудники HINO [9]
define("ADMIN_HINO", "10");    // Группа Администраторы Hino [10]
define("SERVICE_HINO", "11");    // Группа Сервис Hino [11]
define("SUPPORT_HINO", "12");    // Группа Поддержка HINO [12]

define("MANAGER_DEFAULT", "62"); // менеджер по умолчанию
define("DELIVERY_DEFAULT", "6"); // служба доставки по умолчанию
define("MAX_PRE_ORDER", "9999"); // максимальный предзаказ
