<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;

class AIRHandler extends Base
{
  public static function getClassTitle(){
    return "AIR";
  }

  public static function getClassDescription(){
    return "Срочный AIR, ₽";
  }

  protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment){
    $result = new CalculationResult();

    $storeID = $shipment->getStoreId();
    $weight  = floatval($shipment->getWeight()) / 1000;
    $markup  = 0; //наценка

    switch ($storeID) {
      case 1: //ПДК Основной
      case 2: //Склад г.Артём
        $arPrice = $this->config["RUS"];
        break;
      case 3: //Бельгия
      case 4: //Япония
      case 0: //Япония предзаказ, использоутся всегда т.к. getStoreId возвращает 0. если не установлено
        $arPrice = $this->config["BO"];
        break;
    }

    if($weight > 0 && !empty($arPrice)){
      if ($weight < 3) {
        $markup = $arPrice["PRICE_3"];
      } elseif ($weight < 7) {
        $markup = $arPrice["PRICE_7"];
      } elseif ($weight < 15) {
        $markup = $arPrice["PRICE_15"];
      } elseif ($weight < 25) {
        $markup = $arPrice["PRICE_25"];
      } elseif ($weight < 50) {
        $markup = $arPrice["PRICE_50"];
      }
    }

    $result->setDeliveryPrice(roundEx($markup, 2));
    return $result;
  }

  protected function getConfigStructure(){
    return array(
      "RUS" => array(
        "TITLE"       => "Наценки (Россия)",
        "DESCRIPTION" => "Таблица наценок, ₽",
        "ITEMS" => array(
          "PRICE_3" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 0-3 кг."
          ),
          "PRICE_7" => array(
            "TYPE"     => "NUMBER",
            "MIN"      => 0,
            "NAME"     => "Стоимость доставки 3-7 кг."
          ),
          "PRICE_15" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 7-15 кг."
          ),
          "PRICE_25" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 15-25 кг."
          ),
          "PRICE_50" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 25-50 кг."
          )
        )
      ),
      "BO" => array(
        "TITLE"       => "Наценки (Япония, Бельгия)",
        "DESCRIPTION" => "Таблица наценок, ₽",
        "ITEMS" => array(
          "PRICE_3" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 0-3 кг."
          ),
          "PRICE_7" => array(
            "TYPE"     => "NUMBER",
            "MIN"      => 0,
            "NAME"     => "Стоимость доставки 3-7 кг."
          ),
          "PRICE_15" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 7-15 кг."
          ),
          "PRICE_25" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 15-25 кг."
          ),
          "PRICE_50" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 25-50 кг."
          )
        )
      )
    );
  }

  public function isCalculatePriceImmediately(){
    return true;
  }

  public static function whetherAdminExtraServicesShow(){
    return true;
  }
}
