<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;

class SEAHandler extends Base
{
  public static function getClassTitle(){
    return "SEA";
  }

  public static function getClassDescription(){
    return "SEA Обычный тип доставки, по умолчанию";
  }

  protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment){
    $result = new CalculationResult();
    $arConfig = $this->config["MAIN"];
    $result->setDeliveryPrice(roundEx($arConfig["PRICE"], 2));

    return $result;
  }

  protected function getConfigStructure(){
    return array(
      "MAIN" => array(
        "TITLE"       => "Наценки",
        "DESCRIPTION" => "Наценки (для всех складов)",
        "ITEMS" => array(
          "PRICE" => array(
            "TYPE"    => "NUMBER",
            "MIN"     => 0,
            "DEFAULT" => 0,
            "NAME"    => "Стоимость доставки (руб.)"
          )
        )
      )
    );
  }

  public function isCalculatePriceImmediately(){
    return true;
  }

  public static function whetherAdminExtraServicesShow(){
    return true;
  }
}
