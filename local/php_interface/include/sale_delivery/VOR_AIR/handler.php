<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;

class VOR_AIRHandler extends Base
{
  public static function getClassTitle(){
    return "VOR AIR";
  }

  public static function getClassDescription(){
    return "Сверхсрочный VOR AIR, ₽";
  }

  protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment){
    $result = new CalculationResult();

    $arPrice = $this->config["MAIN"];
    $weight  = floatval($shipment->getWeight()) / 1000;
    $markup  = 0; //наценка
    
    if($weight > 0){
      if ($weight < 3) {
        $markup = $arPrice["PRICE_3"];
      } elseif ($weight < 7) {
        $markup = $arPrice["PRICE_7"];
      } elseif ($weight < 15) {
        $markup = $arPrice["PRICE_15"];
      } elseif ($weight < 25) {
        $markup = $arPrice["PRICE_25"];
      } elseif ($weight < 50) {
        $markup = $arPrice["PRICE_50"];
      }
    }

    $result->setDeliveryPrice(roundEx($markup, 2));
    return $result;
  }

  protected function getConfigStructure(){
    return array(
      "MAIN" => array(
        "TITLE"       => "Наценки",
        "DESCRIPTION" => "Таблица наценок, ₽",
        "ITEMS" => array(
          "PRICE_3" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 0-3 кг."
          ),
          "PRICE_7" => array(
            "TYPE"     => "NUMBER",
            "MIN"      => 0,
            "NAME"     => "Стоимость доставки 3-7 кг."
          ),
          "PRICE_15" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 7-15 кг."
          ),
          "PRICE_25" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 15-25 кг."
          ),
          "PRICE_50" => array(
            "TYPE" => "NUMBER",
            "MIN"  => 0,
            "NAME" => "Стоимость доставки 25-50 кг."
          )
        )
      )
    );
  }

  public function isCalculatePriceImmediately(){
    return true;
  }

  public static function whetherAdminExtraServicesShow(){
    return true;
  }
}
