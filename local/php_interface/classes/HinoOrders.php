<?
Bitrix\Main\Loader::includeModule('sale');
Bitrix\Main\Loader::includeModule('catalog');

class HinoOrders
{
  //удаление из резерва товаров, привязанных к заказу
  public function removeReservationParts($orderID){
    HinoReservationPart::deleteOrder($orderID);
  }

  //возвращает имя заказа
  public static function getName($orderId){
    $order = Bitrix\Sale\Order::load(intval($orderId));
    $shipmentCollection = $order->getShipmentCollection();
    $name = "";

    foreach ($shipmentCollection as $shipment) {
      if (!$shipment->isSystem()){
        $name = $shipment->getField("ID_1C");
        if(!empty($name)) return $name;
      }
    }
    $name = $order->getField("ID_1C");

    return empty($name)? $order->getId() :$name;
  }

  //возвращает Номер УПД(ID_1C отгрузки)
  public static function getIdUPD($orderId){
    $order = Bitrix\Sale\Order::load(intval($orderId));
    $shipmentCollection = $order->getShipmentCollection();
    $idUPD = false;

    foreach ($shipmentCollection as $shipment) {
      if (!$shipment->isSystem()){
        if($shipment->getField("ID_1C") && ($shipment->getField("STATUS_ID") == 'DF' || $shipment->getField("STATUS_ID") == 'SR')){
          $idUPD = $shipment->getField("ID_1C");
        }
      }
    }

    return $idUPD;
  }

  //создание заказов из текущей корзины
  public static function createOrders($commentsOrder){
    global $USER;
    $fuserId = \Bitrix\Sale\Fuser::getIdByUserId($USER->GetID());

    //получение корзины для текущего покупателя
    $basketUser = \Bitrix\Sale\Basket::loadItemsForFUser($fuserId, Bitrix\Main\Context::getCurrent()->getSite());
    foreach ($basketUser as $basketItem) {
      $basketPropertyCollection = $basketItem->getPropertyCollection();
      $basketPropertyItem = $basketPropertyCollection->getPropertyValues();

      $basketUserProps[$basketItem->getProductId()] = array(
        "ID"             => $basketItem->getId(),
        "QUANTITY"       => $basketItem->getQuantity(),
        "CATALOG_XML_ID" => $basketItem->getField("CATALOG_XML_ID"),
        "PRODUCT_XML_ID" => $basketItem->getField("PRODUCT_XML_ID"),
        "ARTICLE"        => $basketPropertyItem["ARTICLE"]["VALUE"]
      );
    }


    //получение товаров по складам
    $basketUserReservation = HinoReservationPart::getBasket($fuserId);

    // разделение на заказы
    foreach ($basketUserReservation as $part) {
      $key = $part["STORE_ID"]."_".$part["DELIVERY_ID"];

      if (!array_key_exists($key, $ordersResult)) {
        $ordersResult[$key] = array(
          "STORE_ID"    => $part["STORE_ID"],
          "DELIVERY_ID" => $part["DELIVERY_ID"],
          "PARTS"       => array()
        );
      }
      $ordersResult[$key]["PARTS"][] = $part;
    }

    $userIdDealer  = HinoUsers::getDealerID($USER->GetID());
    $fuserIdDealer = HinoUsers::getFUserIDDealer($USER->GetID());

    foreach ($ordersResult as $orderItem) {
      $basket = Bitrix\Sale\Basket::loadItemsForFUser(
        $fuserIdDealer,
        Bitrix\Main\Context::getCurrent()->getSite()
      )->getOrderableItems();

      //добавление в корзину запчастей
      foreach ($orderItem["PARTS"] as $partOrder) {
        $item = $basket->createItem('catalog', $partOrder["PRODUCT_ID"]);

        //проверка наличия
        $arStoreProduct = array();
        $reservationStore = HinoReservationPart::getReservationStore($partOrder["PRODUCT_ID"]);
        $rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $partOrder["PRODUCT_ID"]), false, false, array('AMOUNT', 'STORE_ID'));
        while($arOb = $rsStore->Fetch()){
          $arStoreProduct[$arOb["STORE_ID"]] = $arOb["AMOUNT"] - $reservationStore[$arOb["STORE_ID"]];
        }

        if($partOrder["QUANTITY"] > $arStoreProduct[$orderItem["STORE_ID"]] && $orderItem["STORE_ID"] !== "0"){
          $arResult["error"][] = "Недостаточное количество на складе - ".$basketUserProps[$partOrder["PRODUCT_ID"]]["ARTICLE"];
        }

        $item->setFields(array(
          'QUANTITY'               => $partOrder["QUANTITY"],
          'CURRENCY'               => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
          'LID'                    => Bitrix\Main\Context::getCurrent()->getSite(),
          'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
          "CATALOG_XML_ID"         => $basketUserProps[$partOrder["PRODUCT_ID"]]["CATALOG_XML_ID"],
          "PRODUCT_XML_ID"         => $basketUserProps[$partOrder["PRODUCT_ID"]]["PRODUCT_XML_ID"]
        ));
      }

      if(!empty($arResult["error"])) continue;

      $order = Bitrix\Sale\Order::create(Bitrix\Main\Context::getCurrent()->getSite(), $userIdDealer);
      $propertyCollection = $order->getPropertyCollection();
      $order->setField("STATUS_ID", "SI"); //к отгрузке

      $comment = '';
      //комментарий для предзаказов
      if($orderItem["STORE_ID"] == 0){
        $comment .= 'BACK ORDER';
      }
      if ($commentsOrder[$orderItem["STORE_ID"]."_".$orderItem["DELIVERY_ID"]]){
        $comment .= " ".$commentsOrder[$orderItem["STORE_ID"]."_".$orderItem["DELIVERY_ID"]];
      }
      if (!empty($comment)) {
        $order->setField("COMMENTS", $comment);
      }

      //добавление свойств пользователя к заказу
      $orderUserPropsDB = CSaleOrderUserProps::GetList(array("DATE_UPDATE" => "DESC"), array("USER_ID" => $userIdDealer));
      $orderUserPropsArray = $orderUserPropsDB->Fetch();

      $user_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $orderUserPropsArray['ID']));
      while ($arPropVals = $user_propVals->Fetch()){
        $propertyCollection->getItemByOrderPropertyId($arPropVals["ORDER_PROPS_ID"])->setValue($arPropVals["VALUE"]);
      }

      /*добовляем адрес доставки*/
      $hinoUsers = new HinoUsers();
      $arUserProp = $hinoUsers->getUserArray($USER->GetID());
      $arDealerProp = HinoUsers::getDealer($USER->GetID(), array('PROPERTY_DELIVERY_ADDRESS_1', 'PROPERTY_DELIVERY_ADDRESS_2', 'PROPERTY_ADDRESS_FULL'));

      if ($arUserProp['UF_DELIVERY_ADDRESS'] != '' && !empty($arDealerProp["~PROPERTY_".$arUserProp['UF_DELIVERY_ADDRESS']."_VALUE"])) {
        $deliveryAddress = $arDealerProp["~PROPERTY_".$arUserProp['UF_DELIVERY_ADDRESS']."_VALUE"];
      } else {
        $deliveryAddress = $arDealerProp["~PROPERTY_ADDRESS_FULL_VALUE"];
      }
      $propertyCollection->getItemByOrderPropertyId(5)->setValue($deliveryAddress);

      /*добовляем ФИО менеджера дилера, который создал заказ*/
      $propertyCollection->getItemByOrderPropertyId(6)->setValue($USER->GetFullName());

      $order->setPersonTypeId(1); //юр.лицо
      $order->setBasket($basket);

      //добавляем доставку
      $shipmentCollection = $order->getShipmentCollection();
      $shipment = $shipmentCollection->createItem();
      $shipmentItemCollection = $shipment->getShipmentItemCollection();
      $shipment->setStoreId($orderItem["STORE_ID"] === "0"? 4: $orderItem["STORE_ID"]); //ставим Японию если это предзаказ

      $deliveryObj = Bitrix\Sale\Delivery\Services\Manager::getObjectById($orderItem["DELIVERY_ID"]);
      $shipment->setFields(array(
        'DELIVERY_ID'   => $deliveryObj->getId(),
        'DELIVERY_NAME' => $deliveryObj->getName(),
        'CURRENCY'      => $deliveryObj->getCurrency()
      ));

      foreach ($basket as $basketItem){
        $item = $shipmentItemCollection->createItem($basketItem);
        $item->setQuantity($basketItem->getQuantity());
      }

      //добавляем платежную систему
      $paymentCollection = $order->getPaymentCollection();
      $payment = $paymentCollection->createItem(
        Bitrix\Sale\PaySystem\Manager::getObjectById(1) // по умолчанию
      );

      $payment->setField("SUM", $order->getPrice());
      $payment->setField("CURRENCY", $order->getCurrency());

      $result = $order->save();

      if (!$result->isSuccess()){
        $arResult["error"][] = $result->getErrors();
      }else{
        $orderID = $result->getId();
        $arResult["result"][] = $orderID;

        // $basketUser = \Bitrix\Sale\Basket::loadItemsForFUser($fuserId, Bitrix\Main\Context::getCurrent()->getSite());
        //обновление кол-ва в корзине и привязка резерва к заказу
        foreach ($orderItem["PARTS"] as $partOrder) {
          $basketItemTmp = $basketUser->getItemById($basketUserProps[$partOrder["PRODUCT_ID"]]["ID"]);

          //уменьшаем кол-во
          $basketItemTmp->setField('QUANTITY', $basketItemTmp->getQuantity() - $partOrder["QUANTITY"]);
          $basketUser->save();

          //резервируем за заказом
          $hr = new HinoReservationPart($partOrder["PRODUCT_ID"], $partOrder["DELIVERY_ID"]);
          $newStoreID = $partOrder["STORE_ID"] === "0"? 4: $partOrder["STORE_ID"]; //ставим Японию если это предзаказ

          $hr->updateOrder($orderID, $partOrder["STORE_ID"], $newStoreID);
        }
      }
    }

    //обновление состояния корзины
    \Bitrix\Sale\BasketComponentHelper::updateFUserBasketQuantity($fuserId);

    return $arResult;
  }
}
