<?
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");

class HinoDiscount{
    protected static $discountID = null;

    protected function getArrayDiscount(){
      $cache      = new CPHPCache();
      $cache_time = 36000;
      $cache_id   = "discountList";
      $cache_path = "discountList";

      if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path)){
        $res = $cache->GetVars();
        if (is_array($res["discountList"]) && (count($res["discountList"]) > 0))
          $discountList = $res["discountList"];
      }
      if (!is_array($discountList)){
        $dbProductDiscounts = CCatalogDiscount::GetList(array(), array(), false, false, array("ID", "VALUE"));
        while ($arProductDiscounts = $dbProductDiscounts->Fetch()){
          $discountList[intval($arProductDiscounts["VALUE"])] = $arProductDiscounts["ID"];
        }

        //////////// end cache /////////
        if ($cache_time > 0){
          $cache->StartDataCache($cache_time, $cache_id, $cache_path);
          $cache->EndDataCache(array("discountList" => $discountList));
        }
      }

      return $discountList;
    }

    protected function addDiscount($name, $value){
      $discountID = CCatalogDiscount::Add(array(
        "SITE_ID"    => "s1",
        "NAME"       => $name,
        "CURRENCY"   => "RUB",
        "ACTIVE"     => "Y",
        "VALUE_TYPE" => "P",
        "VALUE"      => $value
      ));

      //очищаем кеш
      $obCache = new CPHPCache();
      $obCache->CleanDir(false, "cache/discountList");

      return $discountID;
    }

    /**
     * Вызывается в методе CCatalogDiscount::GetDiscount перед получением всех скидок
     * @return bool true
     */
    public function OnGetDiscount($intProductID, $intIBlockID, $arCatalogGroups,
        $arUserGroups, $strRenewal, $siteID, $arDiscountCoupons, $boolSKU, $boolGetIDS){

      $res = CIBlockElement::GetByID($intProductID);
      if($ar_res = $res->GetNextElement()){
        $arProps = $ar_res->GetProperties();

        foreach ($arProps["CML2_TRAITS"]["DESCRIPTION"] as $key => $value) {
          $arProps["CML2_TRAITS_FORMAT"][$value] = $arProps["CML2_TRAITS"]["VALUE"][$key];
        }

        $discountTmp = intval($arProps["CML2_TRAITS_FORMAT"]["ЗначениеСкидкиНаценки"]);
      }

      if(isset($discountTmp) && $discountTmp > 0){
        $discountList = self::getArrayDiscount();
        if (!array_key_exists($discountTmp, $discountList)) {
          self::$discountID = self::addDiscount($arProps["CML2_TRAITS_FORMAT"]["ЦеноваяГруппа"], $discountTmp);
        }else{
          self::$discountID = $discountList[$discountTmp];
        }

      }else{
        self::$discountID = 0;
      }

      return true;
    }

    /**
      * Вызывается в методе CCatalogDiscount::GetDiscount после получения
      * и обработки всех скидок для товара
      * @return array() возвращаем только нужную нам скидку, или создаем её
      */
    public function OnGetDiscountResult(&$arResult){
      $tmpArResult = $arResult;
      $arResult = array();

      if(!empty(self::$discountID)){
        $discountResult = array();

        foreach($tmpArResult as $key_discount => $discount){
          if($discount["ID"] == self::$discountID){
            $discountResult = $discount;
            break;
          }
        }
        $arResult[] = $discountResult;
      }
      self::$discountID = null;
    }

}
