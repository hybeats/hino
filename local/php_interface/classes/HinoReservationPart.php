<?
/**
 * класс для обработки разделения запчасти по складам и способам доставки
 */
class HinoReservationPart{
  const TABLE_NAME = "b_sale_basket_hino_reservation";

  function __construct($idProduct, $delivery, $user_id = 0){
    global $USER;
    CModule::IncludeModule('sale');
    $this->user_id   = $user_id > 0? $user_id: $USER->GetID();
    $this->idProduct = $idProduct;
    $this->delivery  = $delivery > 0? $delivery: DELIVERY_DEFAULT;
    $this->fuser_id  = \Bitrix\Sale\Fuser::getIdByUserId($this->user_id);
  }

  public function insertReservation($arFields){
    global $DB;

    $arInsert = $DB->PrepareInsert(self::TABLE_NAME, $arFields);
    $strSql = "INSERT INTO ".self::TABLE_NAME." (".$arInsert[0].") VALUES (".$arInsert[1].")";
    $DB->Query($strSql, false, "Line: ".__LINE__);
  }

  private function deleteReservation($where){
    global $DB;

    $strSql = "DELETE FROM ".self::TABLE_NAME." WHERE ".$where;
    $DB->Query($strSql, false, "Line: ".__LINE__);
  }

  //возвращает общее кол-во запчасти на всех складах
  public function getCountReservation($orderID = 0){
    global $DB;

    $where = "`PRODUCT_ID` = '".$this->idProduct."' AND `FUSER_ID` = '".$this->fuser_id."' AND `DELIVERY_ID` = '".$this->delivery."' AND `ORDER_ID` = '".$orderID."'";

    $strSql = "SELECT SUM(`QUANTITY`) as sum FROM ".self::TABLE_NAME." WHERE ".$where;
    $resdb = $DB->Query($strSql, false, "Line: ".__LINE__);

    $res = $resdb->Fetch();

    return intval($res["sum"]);
  }

  //возвращает резерв по складам
  public static function getReservationStore($idProduct){
    global $DB;

    $where = "`PRODUCT_ID` = '".$idProduct."' AND `RESERVED` = 'Y'";

    $strSql = "SELECT `STORE_ID`, SUM(`QUANTITY`) QUANTITY FROM ".self::TABLE_NAME." WHERE ".$where." GROUP BY STORE_ID";
    $resdb = $DB->Query($strSql, false, "Line: ".__LINE__);

    $resArray = array();
    while($res = $resdb->Fetch()){
      $resArray[$res["STORE_ID"]] = $res["QUANTITY"];
    }

    return $resArray;
  }

  //возвращает кол-во запчасти по складам
  public function getCountReservationStore($allDelivery = true, $allFUser = true, $orderID = 0){
    global $DB;

    $where = "`PRODUCT_ID` = '".$this->idProduct."'";

    //-1 для всех заказов
    if($orderID >= 0){
      $where .= "  AND `ORDER_ID` = '".$orderID."'";
    }

    if(!$allDelivery){
      //исключаем текущий тип доставки
      $where .= " AND `DELIVERY_ID` <> '".$this->delivery."'";
    }

    if(!$allFUser){
      //только для текущего покупателя
      $where .= " AND `FUSER_ID` = '".$this->fuser_id."'";
    }

    $strSql = "SELECT `STORE_ID`, SUM(`QUANTITY`) QUANTITY FROM ".self::TABLE_NAME." WHERE ".$where." GROUP BY STORE_ID";
    $resdb = $DB->Query($strSql, false, "Line: ".__LINE__);

    $resArray = array();
    while($res = $resdb->Fetch()){
      $resArray[$res["STORE_ID"]] = $res["QUANTITY"];
    }

    return $resArray;
  }

  //возвращает все товары из корзины
  public static function getBasket($fuser_id){
    global $DB;

    $strSql = "SELECT * FROM ".self::TABLE_NAME." WHERE `FUSER_ID` = '".$fuser_id."' AND `ORDER_ID` = '0'";
    $resdb = $DB->Query($strSql, false, "Line: ".__LINE__);

    $resArray = array();
    while($res = $resdb->Fetch()){
      $resArray[] = $res;
    }

    return $resArray;
  }
  //обновление заказа
  public static function updateOrderFields($orderID, $arFieldsUpdate){
    global $DB;
    $orderID = intval($orderID);
    if($orderID <= 0) return false;

    $strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFieldsUpdate);
    $strSql = "UPDATE ".self::TABLE_NAME." SET ".$strUpdate." WHERE `ORDER_ID` = '".$orderID."'";
    $DB->Query($strSql, false, "Line: ".__LINE__);
  }

  //обновление заказа
  public function updateOrder($orderID, $storeID, $newStoreID = 0){
    global $DB;
    $storeID = intval($storeID);
    $arFields = array(
      "ORDER_ID" => intval($orderID),
      "RESERVED" => "Y"
    );
    if($newStoreID > 0){
      $arFields["STORE_ID"] = $newStoreID;
    }
    $where = "`PRODUCT_ID` = '".$this->idProduct."' AND `FUSER_ID` = '".$this->fuser_id."' AND `DELIVERY_ID` = '".$this->delivery."' AND `STORE_ID` = '".$storeID."'";

    $strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
    $strSql = "UPDATE ".self::TABLE_NAME." SET ".$strUpdate." WHERE ".$where;
    $DB->Query($strSql, false, "Line: ".__LINE__);
  }

  // возвращает склад заказа
  public static function getStoreOrder($orderID){
    global $DB;

    $strSql = "SELECT `STORE_ID` FROM ".self::TABLE_NAME." WHERE `ORDER_ID` = '".$orderID."'";
    $resdb = $DB->Query($strSql, false, "Line: ".__LINE__);

    $res = $resdb->Fetch();

    return $res["STORE_ID"];
  }

  //удаление заказа
  public static function deleteOrder($orderID){
    global $DB;

    $strSql = "DELETE FROM ".self::TABLE_NAME." WHERE `ORDER_ID` = '".$orderID."'";
    $DB->Query($strSql, false, "Line: ".__LINE__);
  }

  //удаление запчасти
  public static function deletePart($idProduct, $fuser_id, $orderID = 0){
    global $DB;
    $where = "`PRODUCT_ID` = '".$idProduct."' AND `FUSER_ID` = '".$fuser_id."' AND `ORDER_ID` = '".$orderID."'";

    $strSql = "DELETE FROM ".self::TABLE_NAME." WHERE ".$where;
    $DB->Query($strSql, false, "Line: ".__LINE__);
  }

  //удаляет строку по id
  public static function deletePartByID($reservation_id){
    global $DB;

    $strSql = "SELECT `QUANTITY` FROM ".self::TABLE_NAME." WHERE `ID` = '".$reservation_id."' AND `ORDER_ID` = '0'";
    $resdb = $DB->Query($strSql, false, "Line: ".__LINE__);

    $resArray = array();
    $res = $resdb->Fetch();
    $returnQuantity = $res["QUANTITY"];

    $strSql = "DELETE FROM ".self::TABLE_NAME." WHERE `ID` = '".$reservation_id."' AND `ORDER_ID` = '0'";
    $DB->Query($strSql, false, "Line: ".__LINE__);

    return $returnQuantity;
  }

  //обновление распределения количества по складам и способам доставки для текущего дилера
  public function removePartStore($storeID){
    $where = "`PRODUCT_ID` = '".$this->idProduct."' AND `FUSER_ID` = '".$this->fuser_id."' AND `ORDER_ID` = '0' AND `DELIVERY_ID` = '".$this->delivery."' AND `STORE_ID` = '".$storeID."'";
    $this->deleteReservation($where);
  }

  //обновление распределения количества по складам и способам доставки для текущего дилера
  public function updateQuantity($countProduct){
    $where = "`PRODUCT_ID` = '".$this->idProduct."' AND `FUSER_ID` = '".$this->fuser_id."' AND `ORDER_ID` = '0' AND `DELIVERY_ID` = '".$this->delivery."'";

    $this->deleteReservation($where);
    $this->saveQuantity($countProduct);
  }

  //сохранение распределения количества по складам и способам доставки для текущего дилера
  public function saveQuantity($countProduct){
    global $DB;

    //уже отложено в корзину для этого пользователя для других типов доставки
    // $arStoreProductRes = $this->getCountReservationStore(false, false);
    $arStoreProductRes = self::getReservationStore($this->idProduct);

    //получаем остатки из текущего заказа
    $where = "`PRODUCT_ID` = '".$this->idProduct."' AND `RESERVED` = 'N' AND `ORDER_ID` = '0'";
    $strSql = "SELECT `STORE_ID`, SUM(`QUANTITY`) QUANTITY FROM ".self::TABLE_NAME." WHERE ".$where." GROUP BY STORE_ID";
    $resdb = $DB->Query($strSql, false, "Line: ".__LINE__);

    while($res = $resdb->Fetch()){
      $arStoreProductRes[$res["STORE_ID"]] += $res["QUANTITY"];
    }

    //наличие на складах
    $rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $this->idProduct), false, false, array('AMOUNT', 'STORE_ID'));
    while($arOb = $rsStore->Fetch()){
      $arStoreProduct[$arOb["STORE_ID"]] = $arOb["AMOUNT"] - $arStoreProductRes[$arOb["STORE_ID"]];
    }

    //приоритет складов
    $listStore = HinoUsers::getDealerListStore($this->user_id);

    if(!empty($listStore) && !empty($arStoreProduct) && $countProduct > 0){

      foreach ($listStore as $storeID) {
        if($arStoreProduct[$storeID] > 0){

          //костыль для Сверхсрояной доставки из Японии
          $deliveryID = $this->delivery;
          if($deliveryID == 10 && $storeID != 4){
            $deliveryID = 2;
          }

          if($arStoreProduct[$storeID] >= $countProduct){
            $arFields = array(
              "PRODUCT_ID"  => $this->idProduct,
              "FUSER_ID"    => $this->fuser_id,
              "STORE_ID"    => $storeID,
              "DELIVERY_ID" => $deliveryID,
              "QUANTITY"    => $countProduct,
              "RESERVED"    => "N",
              "ORDER_ID"    => 0
            );
            $this->insertReservation($arFields);
            $countProduct = 0;
            break;
          }else{
            $arFields = array(
              "PRODUCT_ID"  => $this->idProduct,
              "ORDER_ID"    => 0,
              "STORE_ID"    => $storeID,
              "DELIVERY_ID" => $deliveryID,
              "FUSER_ID"    => $this->fuser_id,
              "QUANTITY"    => $arStoreProduct[$storeID],
              "RESERVED"    => "N"
            );
            $countProduct -= $arStoreProduct[$storeID];
            $this->insertReservation($arFields);
          }
        }
      }

      //оформляем предзаказ
      if($countProduct > 0){
        $arFields = array(
          "PRODUCT_ID"  => $this->idProduct,
          "ORDER_ID"    => 0,
          "STORE_ID"    => 0,
          "DELIVERY_ID" => $this->delivery,
          "FUSER_ID"    => $this->fuser_id,
          "QUANTITY"    => $countProduct,
          "RESERVED"    => "N",
        );
        $this->insertReservation($arFields);
      }
    }else{
      // AddMessage2Log("не найден список складов или нет в наличии. FuserID = ".$this->fuser_id."; idProduct = $this->idProduct");
      return false;
    }
  }
}
