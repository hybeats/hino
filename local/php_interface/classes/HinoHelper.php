<?
Bitrix\Main\Loader::includeModule('sale');
Bitrix\Main\Loader::includeModule('catalog');

class HinoHelper
{
  public function removeReservationPartBasket($ID) {
    $arFields = CSaleBasket::GetByID($ID);
    HinoReservationPart::deletePart($arFields["PRODUCT_ID"], $arFields["FUSER_ID"], $arFields["ORDER_ID"]);
  }

  public static function arrayRecursiveDiff ($aArray1, $aArray2) {
    $aReturn = array();

    foreach ($aArray1 as $mKey => $mValue) {
      if (array_key_exists($mKey, $aArray2)) {
        if (is_array($mValue)) {
          $aRecursiveDiff = self::arrayRecursiveDiff($mValue, $aArray2[$mKey]);
          if (count($aRecursiveDiff)) { $aReturn[$mKey] = $aRecursiveDiff; }
        } else {
          if ($mValue != $aArray2[$mKey]) {
            $aReturn[$mKey] = $mValue;
          }
        }
      } else {
        $aReturn[$mKey] = $mValue;
      }
    }

    return $aReturn;
  }

  public static function arrayRecursiveCompare ($aArray1, $aArray2) {
    $return == false;
    if(count($aArray1) == count($aArray2)){
      $diff = self::arrayRecursiveDiff($aArray1, $aArray2);
      $return = empty($diff)? true: false;
    }
    return $return;
  }

  public function OnAfterIBlockElementUpdateHandler($arFields){
    if($arFields["IBLOCK_ID"] == IBLOCK_DEALERS){
      HinoUsers::OnDealerUpdateHandler($arFields);
    }
  }
}


?>
