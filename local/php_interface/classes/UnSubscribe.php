<?
class UnSubscribe {

  function BeforePostingSendMailHandler(&$arFields) {
    $USER_NAME = "Подписчик";
    //Попробуем найти подписчика.
    $rs = CSubscription::GetByEmail($arFields["EMAIL"]);
    if($ar = $rs->Fetch()) {
      if(intval($ar["USER_ID"]) > 0) {
        $rsUser = CUser::GetByID($ar["USER_ID"]);
        if($arUser = $rsUser->Fetch()) {
          $USER_NAME = $arUser["LAST_NAME"]." ".$arUser["NAME"]." ".$arUser["SECOND_NAME"];
        }
      }
    }
    $arFields["BODY"] = str_replace("#NAME#", $USER_NAME, $arFields["BODY"]);
    $arFields["BODY"] = str_replace("#CONFIRM_CODE#", $ar["CONFIRM_CODE"], $arFields["BODY"]);
    $arFields["BODY"] = str_replace("#ID#", $ar["ID"], $arFields["BODY"]);

    return $arFields;
  }
}
