<?
/**
 * класс для работы с запчастями Hino
 */
CModule::IncludeModule('iblock');
CModule::includeModule('sale');
CModule::IncludeModule('catalog');

class HinoProduct{
  public function getXmlIdByArticle( $article = "" ){
    $article = trim($article);
    if($article == "") return false;

    $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_PARTS, "ACTIVE" => "Y", "PROPERTY_CML2_ARTICLE" => $article), false, false, array("XML_ID"));
    $ob = $res->GetNext();

    return $ob["XML_ID"]? $ob["XML_ID"]: false;
  }

  public function getProductByXmlId( $xmlId = "" ){
    $xmlId = trim($xmlId);
    if($xmlId == "") return false;

    $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_PARTS, "ACTIVE" => "Y", "XML_ID" => $xmlId), false, false, array("ID", "NAME"));
    $ob = $res->GetNext();

    return $ob? $ob: false;
  }

  public function checkNotEmptyReplace($arReplacement){
    $flag = false;

    if(!empty($arReplacement)){
      $partInfo = $this->getPartsInfo($arReplacement);
      foreach ($partInfo as $value) {
        if($value['fields']['presence'] > 0){
          $flag = true;
          break;
        }
      }
    }

    return $flag;
  }

  private function createReplacementElement( $part, &$arArticle){
    $tmpArticle = trim($part["Артикул"]);
    $arArticle[] = $tmpArticle;
    $replacement[] = array(
      "xml_id"  => $part["Ид"],
      "article" => $tmpArticle,
      "type"    => empty($part["ТипЗамены"])? false: $part["ТипЗамены"]
    );

    if (array_key_exists("Главный", $part)) {
      $tmpArticle = trim($part["Главный"]["Артикул"]);
      $arArticle[] = $tmpArticle;
      $replacement[] = array(
        "xml_id"  => $part["Главный"]["Ид"],
        "article" => $tmpArticle,
        "type"    => "VY / M"
      );
    }
    if (array_key_exists("Второстепенный", $part)) {
      $tmpArticle = trim($part["Второстепенный"]["Артикул"]);
      $arArticle[] = $tmpArticle;
      $replacement[] = array(
        "xml_id"  => $part["Второстепенный"]["Ид"],
        "article" => $tmpArticle,
        "type"    => "VY / S"
      );
    }
    return $replacement;
  }

  public function getReplacementSoap( $article = array() ){
    if(empty($article)) return false;

    $hs = new HinoSoap('ChangesReport');
    $request = array(
      "Запрос" => array(
        "Артикул" => $article
      )
    );

    $result = $hs->client->GetChanges($request);

    $xml = simplexml_load_string($result->return);
    $json = json_encode($xml);
    $arraySoapReplacement = json_decode($json,TRUE);

    foreach ($article as $key => $val) {
      $listReplacement = count($article) == 1? $arraySoapReplacement["АртикулЗапроса"]["ЦепочкаЗамен"] :$arraySoapReplacement["АртикулЗапроса"][$key]["ЦепочкаЗамен"];

      $arArticle = array();
      if(!$listReplacement || $val == $listReplacement["Замена"]["Артикул"]){
        $replacement[$val] = false;
        continue;
      }elseif(count($listReplacement) == 1) {
        $replacement[$val]["replace"][0] = array();

        foreach ($listReplacement["Замена"] as $part) {
          $tmpReplace = $this->createReplacementElement($part, $arArticle);
          $replacement[$val]["replace"][0] = array_merge($replacement[$val]["replace"][0], $tmpReplace);
        }
      }else{
        foreach ($listReplacement as $keyItem=>$listReplacementItem) {
          $replacement[$val]["replace"][$keyItem] = array();
          foreach ($listReplacementItem["Замена"] as $part) {
            $tmpReplace = $this->createReplacementElement($part, $arArticle);
            $replacement[$val]["replace"][$keyItem] = array_merge($replacement[$val]["replace"][$keyItem], $tmpReplace);
          }
        }
      }

      $replacement[$val]["article"] = array_unique($arArticle, SORT_STRING);
    }

    return $replacement;
  }

  public function getPartsInfo($arArticle, $keyResult = "article", $arLastElement){
    if(!empty($arArticle)){

      //получаем информацию из БД
      $arElementInfo = $this->getElementInfoByArticle($arArticle);

      $arID = array_map(function($value){
        return $value["FIELDS"]["ID"];
      }, $arElementInfo);

      $arProductInfo = $this->getProductInfoByID($arID);

      //список складов
      $rsStore = CCatalogStore::GetList(array(), array(), false, false, array('ID', 'UF_CODE'));
      while($arOb = $rsStore->Fetch()){
        $arStore[$arOb["ID"]] = $arOb["UF_CODE"];
      }

      //получение текущей корзины
      global $USER;
      $fuser_id = \Bitrix\Sale\Fuser::getIdByUserId($USER->GetID());

      $dbBasketItems = CSaleBasket::GetList(array(),array("FUSER_ID" => $fuser_id, "LID" => SITE_ID, "ORDER_ID" => false), false, false, array("PRODUCT_ID", "QUANTITY"));
      while ($arItems = $dbBasketItems->Fetch()){
        $arBasket[$arItems["PRODUCT_ID"]] = $arItems["QUANTITY"];
      }

      foreach ($arArticle as $key=>$article) {
        $idProduct = $arElementInfo[$article]["FIELDS"]["ID"];

        $price = CPrice::GetBasePrice($idProduct);

        //наличие
        $presence = 0;

        $reservationStore = HinoReservationPart::getReservationStore($idProduct);
        $rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $idProduct), false, false, array('AMOUNT', 'STORE_ID'));
        while($arOb = $rsStore->Fetch()){
          $tmpCount = $arOb["AMOUNT"] - $reservationStore[$arOb["STORE_ID"]];
          $presence += $tmpCount;
          $arStoreProduct[$arStore[$arOb["STORE_ID"]]] = $tmpCount > 0? $tmpCount: 0;
        }

        $size = implode("x", array(
          $arElementInfo[$article]["PROPS"]["CML2_TRAITS_FORMAT"]["Длина"],
          $arElementInfo[$article]["PROPS"]["CML2_TRAITS_FORMAT"]["Ширина"],
          $arElementInfo[$article]["PROPS"]["CML2_TRAITS_FORMAT"]["Высота"]
        ));

        $tmpKey = ($keyResult == "id")? $idProduct: $article;
        $code = trim($arElementInfo[$article]["PROPS"]["CML2_TRAITS_FORMAT"]["ProductCode"]);

        $priceResult = $price["PRICE"] - $price["PRICE"]*(intval($arElementInfo[$article]["PROPS"]["CML2_TRAITS_FORMAT"]["ЗначениеСкидкиНаценки"])/100); //Скидка

        // Получаем МРЦ
        $dbPriceMRC = CPrice::GetListEx(
          array(),
          array("PRODUCT_ID" => $idProduct, "CATALOG_GROUP_ID"=> 2),
          false,
          false,
          array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY")
        );
        $arPriceMRC = $dbPriceMRC->Fetch();

        $arResult[$tmpKey] = array(
          "id"       => $idProduct,
          "quantity" => (int)$arBasket[$idProduct],
          "preorder" => in_array($article, $arLastElement),
          "fields" => array(
            "number"      => $article,
            "name"        => $arElementInfo[$article]["FIELDS"]["NAME"],
            "format_name" => "<span title=\"".$arElementInfo[$article]["FIELDS"]["NAME"]."\">".TruncateText($arElementInfo[$article]["FIELDS"]["NAME"], 30)."</span>",
            "price"       => $priceResult,
            "price_mrc"   => doubleval($arPriceMRC["PRICE"]),
            "presence"    => intval($presence) > 0? intval($presence): 0,
            "moscow"      => $arStoreProduct["moscow"],
            "artem"       => $arStoreProduct["artem"],
            "belgium"     => $arStoreProduct["belgium"],
            "japan"       => $arStoreProduct["japan"],
            "grade"       => $arElementInfo[$article]["PROPS"]["CML2_TRAITS_FORMAT"]["TMG"],
            "code"        => $code[0]? $code[0]: "-",
            "size"        => $size,
            "weight"      => number_format($arProductInfo[$idProduct]["WEIGHT"] / 1000, 2)
          )
        );
      }

      return $arResult;
    }else{
      return false;
    }
  }


  public function getElementInfoByArticle( $arArticle = array() ){
    if(empty($arArticle)) return false;

    $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_PARTS, "ACTIVE" => "Y", "PROPERTY_CML2_ARTICLE" => $arArticle), false, false, array("ID", "IBLOCK_ID", "NAME", "XML_ID"));
    while($ob = $res->GetNextElement()){
      $arFields = $ob->GetFields();
      $arProps  = $ob->GetProperties();

      foreach ($arProps["CML2_TRAITS"]["DESCRIPTION"] as $key => $value) {
        $arProps["CML2_TRAITS_FORMAT"][$value] = $arProps["CML2_TRAITS"]["VALUE"][$key];
      }
      $arResult[$arProps["CML2_ARTICLE"]["VALUE"]] = array(
        "FIELDS" => $arFields,
        "PROPS"  => $arProps
      );
    }

    return empty($arResult)? false: $arResult;
  }

  public function getProductInfoByID( $arID = array() ){
    if(empty($arID)) return false;

    $db_res = CCatalogProduct::GetList(array(), array("ID" => $arID));
    while ($ar_res = $db_res->Fetch()){
      $arResult[$ar_res["ID"]] = $ar_res;
    }

    return empty($arResult)? false: $arResult;
  }

  public static function addBasket( $idProduct, $quantity, $article, $fuser_id, $deliveryID = 0, $pre_order = 0){
    $intProductIBlockID = (int)CIBlockElement::GetIBlockByID($idProduct);

    if ($intProductIBlockID > 0){
      $rp = new HinoReservationPart($idProduct, $deliveryID);

      $productInfo = self::getProductInfoByID(array($idProduct));
      $allQuantity = $productInfo[$idProduct]["QUANTITY"];

      if($pre_order){
        $quantity = ($quantity > MAX_PRE_ORDER)? MAX_PRE_ORDER: $quantity;
      }else{
        $quantity = ($quantity > $allQuantity)? $allQuantity: $quantity;
      }

      $basket = Bitrix\Sale\Basket::loadItemsForFUser($fuser_id, Bitrix\Main\Context::getCurrent()->getSite());

      $arProduct = self::getElementInfoByArticle($article);

      $item = $basket->createItem('catalog', $idProduct);
      $item->setFields(array(
        "QUANTITY"               => $quantity,
        "CURRENCY"               => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
        "LID"                    => Bitrix\Main\Context::getCurrent()->getSite(),
        "PRODUCT_PROVIDER_CLASS" => 'CCatalogProductProvider',
        "CATALOG_XML_ID"         => (string)CIBlock::GetArrayByID(IBLOCK_PARTS, 'XML_ID'),
        "PRODUCT_XML_ID"         => $arProduct[$article]["FIELDS"]["~XML_ID"]
      ));

      $resultAdd = $basket->save();

      if($resultAdd->isSuccess()){
        $basketPropertyCollection = $item->getPropertyCollection();
        $basketPropertyCollection->setProperty(array(
          array("NAME" => "Артикул", "CODE" => "ARTICLE", "VALUE" => $article)
        ));
        $basketPropertyCollection->save();

        $rp->saveQuantity($quantity);
        return array("result" => $quantity);
      }else{
        return array("error" => "CATALOG_ERROR2BASKET");
      }
    }else{
      return array("error" => "CATALOG_ELEMENT_NOT_FOUND");
    }
  }

  public static function updateBasket( $idProduct, $quantity, $fuser_id, $type = "update", $deliveryID = 0, $pre_order = 0){
    $intProductIBlockID = (int)CIBlockElement::GetIBlockByID($idProduct);

    if ($intProductIBlockID > 0){
      $rp = new HinoReservationPart($idProduct, $deliveryID);
      //получение текущей корзины
      $dbBasketItems = CSaleBasket::GetList(array(),array("FUSER_ID" => $fuser_id, "LID" => SITE_ID, "ORDER_ID" => false), false, false, array("ID", "PRODUCT_ID", "QUANTITY"));
      while ($arItems = $dbBasketItems->Fetch()){
        $arBasket[$arItems["PRODUCT_ID"]] = $arItems;
      }

      //проверяем доступное кол-во
      if($pre_order){
        $allQuantity = MAX_PRE_ORDER;
      }else{
        $productInfo = self::getProductInfoByID($idProduct);
        $allQuantity = $productInfo[$idProduct]["QUANTITY"];
      }

      $oldQuantity = $rp->getCountReservation();  //уже было добавлено с текущим $deliveryID

      if($type == "add"){ // добавляем к уже добавленным, иначе просто обновляем кол-во
        //кол-во для обновления
        $updateQuantity = min(($arBasket[$idProduct]["QUANTITY"] + $quantity), $allQuantity);
      }else{
        //кол-во для обновления
        $updateQuantity = min((($arBasket[$idProduct]["QUANTITY"] - $oldQuantity) + $quantity), $allQuantity);
      }
      //нужно распределить
      $newQuantity = $updateQuantity - $arBasket[$idProduct]["QUANTITY"] + $oldQuantity;

      CSaleBasket::Update($arBasket[$idProduct]["ID"], array("QUANTITY" => $updateQuantity));
      $rp->updateQuantity($newQuantity);

      $quantity = $updateQuantity; //стало в корзине

      return array("result" => $quantity);
    }else{
      return array("error" => "CATALOG_ELEMENT_NOT_FOUND");
    }
  }
}

?>
