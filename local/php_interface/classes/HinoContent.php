<?
/**
 * класс для работы с контентом Hino
 */
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class HinoContent
{
  public function getDealerByParams( $user_id, $dealer_id ){

    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*", "PREVIEW_PICTURE", "CODE" );
    $arFilter = ($dealer_id)? Array("IBLOCK_ID"=> IBLOCK_DEALERS, "ID" => $dealer_id):
                              Array("IBLOCK_ID"=> IBLOCK_DEALERS, "PROPERTY_EMPLOYEE" => $user_id);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement()){
      $arResult["FIELDS"] = $ob->GetFields();
      $arResult["PROPS"] = $ob->GetProperties();
    }

    return $arResult;
  }

  public function getDocsByDealerCode( $code ){

    $hlblock_id = HIBLOCK_AGREEMENTS;
    $hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    $rsData = $entity_data_class::getList(array(
      "select" => array("*"),
      "order" => array("ID" => "DESC"),
      "filter" => array("UF_PARTNER" => $code)
    ));
    if($arData = $rsData->Fetch()) {
      $arResult[] = $arData;
    }
    
    return $arResult;
  }

  public function getDocsTemplates($iblock_id){

    $docs = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $iblock_id), false, false, Array("ID", "IBLOCK_ID", "PROPERTY_FILE", "NAME"));
    while($doc = $docs->GetNextElement()){
      $arDocFields = $doc->GetFields();
      $arDocProps = $doc->GetProperties();
      $arResult[$arDocFields["NAME"]] = CFile::GetPath($arDocProps["FILE"]["VALUE"]);
    }

    return $arResult;
  }


}

?>
