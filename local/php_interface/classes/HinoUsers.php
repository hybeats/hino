<?
/**
 * класс для работы с пользователями hino
 */
Bitrix\Main\Loader::includeModule("iblock");
Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");

class HinoUsers
{
  public function getUserArray( $user_id ){

    $rsUser = CUser::GetByID($user_id);
    $arResult = $rsUser->Fetch();

    return $arResult;
  }

  public function getUsersByIdFilter( $array_id ){

    if (!empty($array_id)) {
      foreach ($array_id as $key => $value) {
        $employeesFilter .= $value." | ";
      }

      $res = CUser::GetList(($by="name"), ($order="asc"), array("ID" => $employeesFilter), array("SELECT" => array("UF_*")));
      while($Employee = $res->NavNext()) {
        $arResult[$Employee["ID"]] = array(
          'ID'                  => $Employee['ID'],
          'LAST_NAME'           => $Employee['LAST_NAME'],
          'NAME'                => $Employee['NAME'],
          'SECOND_NAME'         => $Employee['SECOND_NAME'],
          'EMAIL'               => $Employee['EMAIL'],
          'PERSONAL_PHONE'      => $Employee['PERSONAL_PHONE'],
          'UF_PROXY_NUMBER'     => $Employee['UF_PROXY_NUMBER'],
          'UF_PROXY_DATE_START' => $Employee['UF_PROXY_DATE_START'],
          'UF_PROXY_DATE_END'   => $Employee['UF_PROXY_DATE_END'],
          'UF_DELIVERY_ADDRESS' => $Employee['UF_DELIVERY_ADDRESS'],
        );
      }
    }

    return $arResult;
  }

  //проверяет админские права у юзера Hino
  public static function isAdmin(){
    return CSite::InGroup(array(1, EMPLOYEES_HINO, ADMIN_HINO));  //Админы и сотрудники Hino
  }

  //проверяет является ли пользователь сотрудником поддержки HINO
  public static function isSupportGroup(){
    return CSite::InGroup(array(SUPPORT_HINO));
  }

  //проверяет является ли пользователь сотрудником сервиса HINO
  public static function isServiceGroup(){
    return CSite::InGroup(array(SERVICE_HINO));
  }

  //проверяет является ли пользователь сотрудником дилера
  public static function isDealer(){
    $arDealers = HinoUsers::getDealer();
    return !empty($arDealers);
  }

  //возвращает status дилера к которому привязан текущий пользователь.
  public static function getDealerStatus( $user_id = 0 ){
    global $USER;
    $dealer_status = false;
    $user_id = $user_id > 0? $user_id: $USER->GetID();

    $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_STATUS");
    $arFilter = Array("IBLOCK_ID" => IBLOCK_DEALERS, "ACTIVE"=>"Y", "PROPERTY_EMPLOYEE" => $user_id);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

    if($ob = $res->GetNextElement()){
      $arProps       = $ob->GetProperties();
      $dealer_status = $arProps["STATUS"]["VALUE_XML_ID"];
    }

    return $dealer_status;
  }

  //возвращает id дилера к которому привязан текущий пользователь.
  public static function getDealerID( $user_id = 0 ){
    global $USER;

    $user_id = $user_id > 0? $user_id: $USER->GetID();

    $arSelect = Array("ID", "PROPERTY_CONTRAGENT_ID");
    $arFilter = Array("IBLOCK_ID" => IBLOCK_DEALERS, "ACTIVE"=>"Y", "PROPERTY_EMPLOYEE" => $user_id);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

    if($ob = $res->GetNext()){
      $dealer_id = $ob["PROPERTY_CONTRAGENT_ID_VALUE"];
    }

    return $dealer_id;
  }

  //возвращает персональног менеджера
  public static function getManager( $user_id = 0 ){
    global $USER;
    $result =  false;
    $user_id = $user_id > 0? $user_id: $USER->GetID();

    $arSelect = Array("ID", "PROPERTY_MANAGER");
    $arFilter = Array("IBLOCK_ID" => IBLOCK_DEALERS, "ACTIVE"=>"Y", "PROPERTY_EMPLOYEE" => $user_id);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

    if($ob = $res->GetNext()){
      $managerUserID = $ob["PROPERTY_MANAGER_VALUE"];
    }

    $managerUserID = $managerUserID? $managerUserID: MANAGER_DEFAULT;
    $manager = CUser::GetByID($managerUserID);
    $result = $manager->Fetch();

    return $result;
  }

  //возвращает id покупателя дилера к которому привязан текущий пользователь.
  //Пытается создать покупателя если его нет
  public static function getFUserIDDealer( $user_id = 0 ){
    global $USER;
    $fuser_id = false;

    $user_id = $user_id > 0? $user_id: $USER->GetID();

    $dealer_id = self::getDealerID($user_id);

    if($dealer_id){
      if ($arFUser = CSaleUser::GetList(array('USER_ID' => $dealer_id))){
        $fuser_id = $arFUser["ID"];
      }else{
        // пытаемся создать
        $fuser_id  = \Bitrix\Sale\Fuser::getIdByUserId($dealer_id);
      }
    }

    return $fuser_id;
  }

  public function OnAfterUserEditHandler(&$arFields){
    //обновление чатов
    if (\Bitrix\Main\Loader::includeModule('im')){
      // получаем все чаты
      $arChats = CIMChat::GetChatData(Array(
        'GET_LIST' => 'Y',
        'USE_CACHE' => 'N',
        'USER_ID'  => 1
      ));

      $res = CUser::GetUserGroupList($arFields['ID']);
      while ($arGroup = $res->Fetch()){
        $arGroupID[] = $arGroup["GROUP_ID"];
      }

      // GENERAL_CHAT_ID - общий чат
      //добавление - удаление из открытого чата
      if ( (in_array(EMPLOYEES_DEALERS, $arGroupID) || in_array(EMPLOYEES_HINO, $arGroupID) || in_array(ADMIN_HINO, $arGroupID) || in_array(SUPPORT_HINO, $arGroupID))
            && $arFields['ACTIVE'] == 'Y'
      ) {
        if(!in_array($arFields["ID"], $arChats["userInChat"][GENERAL_CHAT_ID])){
          $chat = new \CIMChat(0);
          $chat->AddUser(GENERAL_CHAT_ID, $arFields["ID"], true, true);
        }
      }else{
        if(in_array($arFields["ID"], $arChats["userInChat"][GENERAL_CHAT_ID])){
          $chat = new \CIMChat;
          $chat->DeleteUser(GENERAL_CHAT_ID, $arFields["ID"], true, true);
        }
      }
    }
  }

  public static function OnDealerUpdateHandler($arFields){
    //обновление чатов
    if (\Bitrix\Main\Loader::includeModule('im')){
      $arChats = CIMChat::GetChatData(Array(
        'GET_LIST' => 'Y',
        'USE_CACHE' => 'N',
        'USER_ID'  => 1
      ));

      //находим дилерский чат
      $arDealerUser = array_values($arFields["PROPERTY_VALUES"][8]);
      $idChat = 0;
      $arChatOldUser = array();
      foreach ($arChats["chat"] as $id => $value) {
        if($value["description"] == $arDealerUser[0]["VALUE"]){
          $idChat = $id;
          $arChatOldUser = $arChats["userInChat"][$id];
          break;
        }
      }

      //сотрудники
      $arChatNewUser = array();
      foreach ($arFields["PROPERTY_VALUES"][9] as $key => $value) {
        if($value["VALUE"]){
          $arChatNewUser[] = $value["VALUE"];
        }
      }

      //менеджер
      $arChatNewManager = array();
      foreach ($arFields["PROPERTY_VALUES"][10] as $key => $value) {
        if($value["VALUE"]){
          $arChatNewManager[] = $value["VALUE"];
        }
      }
      if(empty($arChatNewManager)) $arChatNewManager[] = MANAGER_DEFAULT;

      $arMergeUser = array_merge(array(1), $arChatNewUser, $arChatNewManager);

      $chat = new \CIMChat;
      if($idChat > 0){
        $arDeleteUser = array_diff($arChatOldUser, $arMergeUser);
        foreach ($arDeleteUser as $value) {
          $chat->DeleteUser($idChat, $value);
        }

        $arAddUser = array_diff($arMergeUser, $arChatOldUser);
        foreach ($arAddUser as $value) {
          $chat->AddUser($idChat, $value);
        }
      }else{  //создаем если нет чата
        $idChat = $chat->Add(array(
          'TITLE'       => htmlspecialchars_decode($arFields["NAME"]),
          'DESCRIPTION' => $arDealerUser[0]["VALUE"],
          'TYPE'        => IM_MESSAGE_CHAT,
          'AUTHOR_ID'   => '1',
        ));

        $chat->AddUser($idChat, $arMergeUser);
      }
    }
  }

  public function OnUserDeleteHandler(&$arFields){
    //обновление чатов
    if (\Bitrix\Main\Loader::includeModule('im')){
      // получаем все чаты
      $arChats = CIMChat::GetChatData(Array(
        'GET_LIST' => 'Y',
        'USE_CACHE' => 'N',
        'USER_ID'  => 1
      ));

      $chat = new \CIMChat;
      foreach ($arChats["userInChat"] as $id => $value) {
        if(!in_array($arFields["ID"], $value)){
          $chat->DeleteUser($id, $arFields["ID"]);
        }
      }
    }
  }

  //возвращает массив свойств дилера к которому привязан текущий пользователь.
  public static function getDealer( $user_id = 0, $arSelectUser = array()){
    global $USER;

    $user_id = $user_id > 0? $user_id: $USER->GetID();

    $arSelect = Array("ID");
    $arFilter = Array("IBLOCK_ID" => IBLOCK_DEALERS, "ACTIVE"=>"Y", "PROPERTY_EMPLOYEE" => $user_id);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array_merge($arSelect, $arSelectUser));

    if($ob = $res->GetNext()){
      return $ob;
    }else{
      return false;
    }
  }

  // возвращает список приоритета складов для дилера по пользователю
  public static function getDealerListStore( $user_id = 0 ){
    global $USER;
    $user_id = $user_id > 0? $user_id: $USER->GetID();
    $dealer = self::getDealer($user_id);

    return self::getDealerListStoreByID($dealer["ID"]);
  }

  public static function cleanCacheStoreDealer($dealer_id){
    $cache_id = 'storelist_'.$dealer_id;
    $cache_path = 'storelist';
    $obCache = \Bitrix\Main\Data\Cache::createInstance();
    $obCache->clean($cache_id, $cache_path);
  }

  // возвращает список приоритета складов для дилера по id дилера
  public static function getDealerListStoreByID( $dealer_id ){
    $cache = new CPHPCache();
    $cache_time = 36000;
    $cache_id = 'storelist_'.$dealer_id;
    $cache_path = 'storelist';

    if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path)){
      $res = $cache->GetVars();
      if (is_array($res["storelist"]) && (count($res["storelist"]) > 0))
        $storeList = $res["storelist"];
    }
    if (!is_array($storeList)){
      if($dealer_id){
        $res = CCatalogStore::GetList(array("SORT"=>"ASC"), array("ACTIVE" => "Y"), false, false, array("ID", "SORT", "TITLE"));
        while($arRes = $res->GetNext()){
          $arStock[] = $arRes["ID"];
        }

        $res = CIBlockElement::GetByID($dealer_id);

        $ob      = $res->GetNextElement();
        $arProps = $ob->GetProperties();

        if(empty($arProps["STOCK"]["VALUE"])){
          $arSort = $arStock;
        }else{
          $arDiff = array_diff($arStock, $arProps["STOCK"]["VALUE"]);
          $arMerge = array_merge($arProps["STOCK"]["VALUE"], $arDiff);

          foreach($arMerge as $val){
            if(in_array($val, $arStock)){
              $arSort[] = $val;
            }
          }
        }

        $storeList = $arSort;
      }else{
        $storeList = false;
      }

      if ($cache_time > 0){
        $cache->StartDataCache($cache_time, $cache_id, $cache_path);
        $cache->EndDataCache(array("storelist"=>$storeList));
      }
    }

    return $storeList;
  }

}

?>
