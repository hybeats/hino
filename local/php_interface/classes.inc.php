<?
$arClasses = array(
  "HinoSoap"             => "/local/php_interface/classes/HinoSoap.php",
  "HinoHelper"           => "/local/php_interface/classes/HinoHelper.php",
  "HinoProduct"          => "/local/php_interface/classes/HinoProduct.php",
  "HinoContent"          => "/local/php_interface/classes/HinoContent.php",
  "HinoUsers"            => "/local/php_interface/classes/HinoUsers.php",
  "HinoOrders"           => "/local/php_interface/classes/HinoOrders.php",
  "HinoDiscount"         => "/local/php_interface/classes/HinoDiscount.php",
  "HinoReservationPart"  => "/local/php_interface/classes/HinoReservationPart.php",
  "UnSubscribe"          => "/local/php_interface/classes/UnSubscribe.php",
  "CIBlockPropertyStock" => "/local/php_interface/prop/CIBlockPropertyStock.php",
);

CModule::AddAutoloadClasses("", $arClasses);
?>
