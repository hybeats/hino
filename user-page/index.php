<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hino. Личный кабинет");
?>

<?$APPLICATION->IncludeComponent(
	"hino:user.page",
	(CSite::InGroup(array(EMPLOYEES_DEALERS)))? "dealer-user": "hino-user", //проверка принадлежности пользователя к группе "Сотрудники дилера"
	Array(
		"USER_ID" => CUser::GetID(),
	)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
