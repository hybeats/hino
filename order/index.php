<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hino. Оформить заказ");
?>
<script type="text/x-template" id="hino-part">
  <div class="parts-table__row">
    <div class="parts-table__col parts-table__col_name">{{index +1}}</div>
    <div class="parts-table__col parts-table__col_number">{{part.article}}</div>
    <div class="parts-table__col parts-table__col_name" v-html="part.name"></div>
    <div class="parts-table__col parts-table__col_presence" v-if="store > 0">{{part.presence}} шт.</div>
    <div class="parts-table__col parts-table__col_count">{{part.count}} шт.</div>
    <div class="parts-table__col parts-table__col_shipping">{{shipping.name}}</div>
    <div class="parts-table__col parts-table__col_grade">{{part.grade}}</div>
    <div class="parts-table__col parts-table__col_code">{{part.code}}</div>
    <div class="parts-table__col parts-table__col_weight">{{resultWeight}}</div>
    <div class="parts-table__col parts-table__col_addition">-</div>
    <div class="parts-table__col parts-table__col_cost">{{part.price.formatPrice()}} руб.</div>
    <div class="parts-table__col parts-table__col_result-cost">{{resultCost.formatPrice()}} руб.</div>
    <div class="parts-table__col parts-table__col_edit">
      <span class="table-button-wrap">
        <button class="table-button table-button_edit" title="Редактировать" v-on:click="$emit('edit')"></button>
      </span>
      <span class="table-button-wrap" v-if="part.edit">
        <button class="table-button table-button_delete" title="Удалить" v-on:click="$emit('remove')"></button>
      </span>
    </div>
  </div>
</script>

<script type="text/x-template" id="hino-order">
  <div class="order-page__table parts-table">
    <div class="parts-table__header">
      <div class="parts-table__head parts-table__head_name">№</div>
      <div class="parts-table__head parts-table__head_number">Артикул</div>
      <div class="parts-table__head parts-table__head_name">Наименование</div>
      <div class="parts-table__head parts-table__head_presence" v-if="store > 0">Наличие</div>
      <div class="parts-table__head parts-table__head_count">Количество</div>
      <div class="parts-table__head parts-table__head_shipping">Тип<br/>доставки</div>
      <div class="parts-table__head parts-table__head_grade">Moving<br/>Grade</div>
      <div class="parts-table__head parts-table__head_code">Код<br/>продукта</div>
      <div class="parts-table__head parts-table__head_weight">Вес</div>
      <div class="parts-table__head parts-table__head_addition">Наценка</div>
      <div class="parts-table__head parts-table__head_cost">Стоимость</div>
      <div class="parts-table__head parts-table__head_result-cost">Общая<br/>стоимость</div>
      <div class="parts-table__head parts-table__head_edit">Редактировать</div>
    </div>
    <hino-part v-for="(part, index) in order.parts"
      v-bind:index = "index"
      v-bind:part = "part"
      v-bind:store = "store"
      v-bind:shipping = "shipping"
      v-on:remove = "removePart(part)"
      v-on:edit   = "editPart(part)"
      :key="part.id">
    </hino-part>
    <div class="parts-table__row_result">
      <div class="parts-table__col parts-table__col_name parts-table__col_empty"></div>
      <div class="parts-table__col parts-table__col_number parts-table__col_empty"></div>
      <div class="parts-table__col parts-table__col_name parts-table__col_empty"></div>
      <div class="parts-table__col parts-table__col_count parts-table__col_empty"></div>
      <div class="parts-table__col parts-table__col_presence parts-table__col_empty" v-if="store > 0"></div>
      <div class="parts-table__col parts-table__col_shipping parts-table__col_empty"></div>
      <div class="parts-table__col parts-table__col_size parts-table__col_empty"></div>
      <div class="parts-table__col parts-table__col_size parts-table__col_empty"><b>Итого:</b></div>
      <div class="parts-table__col parts-table__col_weight">{{orderWeight}}</div>

      <div class="parts-table__col parts-table__col_addition" v-if="orderWeight > 50 && shipping.id != 6">По запросу</div>
      <div class="parts-table__col parts-table__col_addition" v-else>{{order.delivery_price.formatPrice()}} руб.</div>

      <div class="parts-table__col parts-table__col_cost">{{orderCost.formatPrice()}} руб.</div>
      <div class="parts-table__col parts-table__col_result-cost">{{orderResultCost.formatPrice()}} руб.</div>
      <div class="parts-table__col parts-table__col_empty"></div>
    </div>
  </div>
</script>

<section class="order-page" id="order-page">
  <div class="container-fluid">
    <div class="main-wrap order-page__wrap">
      <div class="row order-page__form order-form" method="post">
        <form id="form_add_part" class="col-md-10 col-xs-12 order-form__col">
          <div class="row order-form__row">
            <div class="col-sm-3 col-xs-6 order-form__col">
              <input class="input-default order-form__input" type="text" v-model.trim="formAdd.article" v-on:input="filterArticle" name="article" placeholder="Артикул">
            </div>
            <div class="col-sm-3 col-xs-6 order-form__col">
              <input class="input-default order-form__input" type="number" v-model="formAdd.quantity" name="quantity" min="1" placeholder="Количество">
            </div>
            <div class="col-sm-3 col-xs-6 order-form__col">
              <select class="select-default order-form__select" v-model="formAdd.delivery" v-if="formAdd.delivery" name="delivery">
                <option v-for="item in info.delivery" :value="item.id">{{item.name}}</option>
              </select>
            </div>
            <div class="col-sm-3 col-xs-6 order-form__col">
              <input class="order-form__submit" type="submit" v-on:click.prevent="addPart" name="order_form_submit" value="Добавить">
            </div>
          </div>
        </form>
        <div class="col-md-2 col-xs-12 order-form__col">
          <div class="input-file">
            <button class="input-file__btn order-page__load-file" type="button">Загрузить txt</button>
            <input class="input-file__hidden" type="file" v-on:change="onFileChange" accept="text/plain">
          </div>
        </div>
      </div>

      <div class="row" v-if="!emptyOrders()">
        <div class="order-page__stock" v-for="store in info.stores" v-if="!emptyStore(store.id)">
          <div class="col-lg-12">
            <h2 class="order-page__title">{{store.title}}</h2>
          </div>
          <div class="col-lg-12" v-for="(order, deliveryID) in orders[store.id]" v-if="order.parts.length > 0">
            <hino-order
              v-bind:order    = "order"
              v-bind:shipping = "getDeliveryToID(deliveryID)"
              v-bind:delivery = "deliveryID"
              v-bind:store    = "store.id"
              :key="deliveryID"
              v-on:edit   = "editPart"
              v-on:remove = "removePart">
            </hino-order>
            <div class="order-comment">
              <button v-if="!order.comment" v-on:click.prevent="updateComment(order)" class="order-comment__button">
                <span>Добавить комментарий</span>
              </button>
              <div v-if="order.comment" class="order-comment__text">
                <div class="row">
                  <div class="col-xs-10">
                    {{order.comment}}
                  </div>
                  <div class="col-xs-2 order-comment__buttons">
                    <span class="table-button-wrap">
                      <button title="Редактировать комментарий" class="table-button table-button_edit" v-on:click.prevent="updateComment(order)"></button>
                    </span>
                    <span class="table-button-wrap">
                      <button title="Удалить комментарий" class="table-button table-button_delete" v-on:click.prevent="clearComment(order)"></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="row">
            <div class="col-sm-offset-6 col-sm-3">
              <a href="#" class="order-page__clear" v-on:click.prevent="clearBasket">Очистить корзину</a>
            </div>
            <div class="col-sm-3">
              <a href="#" class="order-page__submit" v-on:click.prevent="createOrders">Оформить заказ</a>
            </div>
          </div>
        </div>
      </div>
      <div v-else class="order-page__empty">В корзине ничего нет</div>
    </div>
  </div>
</section>

<script src="/js/order/script.js" charset="utf-8"></script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
