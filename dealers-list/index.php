<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hino. Список дилеров");
?>
<?$APPLICATION->IncludeComponent(
  "bitrix:news.list",
  "dealers-list",
  Array(
    "DISPLAY_DATE"              => "Y",
    "DISPLAY_NAME"              => "Y",
    "DISPLAY_PICTURE"           => "Y",
    "DISPLAY_PREVIEW_TEXT"      => "Y",
    "AJAX_MODE"                 => "N",
    "IBLOCK_TYPE"               => "1c_hino",
    "IBLOCK_ID"                 => "3",
    "NEWS_COUNT"                => "30",
    "SORT_BY1"                  => "ACTIVE_FROM",
    "SORT_ORDER1"               => "DESC",
    "SORT_BY2"                  => "SORT",
    "SORT_ORDER2"               => "ASC",
    "FILTER_NAME"               => "",
    "FIELD_CODE"                => Array("ID"),
    "PROPERTY_CODE"             => Array("STATUS"),
    "CHECK_DATES"               => "N",
    "DETAIL_URL"                => "",
    "PREVIEW_TRUNCATE_LEN"      => "",
    "ACTIVE_DATE_FORMAT"        => "d.m.Y",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
    "ADD_SECTIONS_CHAIN"        => "Y",
    "HIDE_LINK_WHEN_NO_DETAIL"  => "Y",
    "PARENT_SECTION"            => "",
    "PARENT_SECTION_CODE"       => "",
    "INCLUDE_SUBSECTIONS"       => "Y",
    "CACHE_TYPE"                => "A",
    "CACHE_TIME"                => "0",
    "CACHE_FILTER"              => "Y",
    "CACHE_GROUPS"              => "Y",
    "SET_TITLE"                 => "N",
    "SET_STATUS_404"            => "Y",
    "SHOW_404"                  => "Y",
    "MESSAGE_404"               => "",
    "PAGER_BASE_LINK"           => "",
    "PAGER_TEMPLATE"            => "hino-pager",
  )
 );?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
