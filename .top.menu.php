<?
$aMenuLinks = Array(
  Array(
    "Поиск запчасти",
    "/parts/",
    Array(),
    Array("MODIFIER"=>"search"),
    ""
  ),
  Array(
    "Оформить заказ",
    "/order/",
    Array(),
    Array("MODIFIER"=>"order"),
    "HinoUsers::isDealer()"
  ),
  Array(
    "История заказов",
    "/history/",
    Array(),
    Array("MODIFIER"=>"history"),
    "HinoUsers::isDealer() || HinoUsers::isAdmin() || HinoUsers::isServiceGroup() || HinoUsers::isSupportGroup()"
  ),
  Array(
    "Новости",
    "/news/",
    Array(),
    Array("MODIFIER"=>"news"),
    ""
  )
);
?>
