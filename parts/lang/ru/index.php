<?
$MESS["hino_tab_number"]      = "Артикул";
$MESS["hino_tab_format_name"] = "Наименование";
$MESS["hino_tab_price"]       = "Цена";
$MESS["hino_tab_price_mrc"]   = "МРЦ";
$MESS["hino_tab_presence"]    = "Наличие";
$MESS["hino_tab_moscow"]      = "Москва";
$MESS["hino_tab_artem"]       = "Артем";
$MESS["hino_tab_belgium"]     = "Бельгия";
$MESS["hino_tab_japan"]       = "Япония";
$MESS["hino_tab_type"]        = "Тип<br/>замен";
$MESS["hino_tab_grade"]       = "Moving<br/>Grade";
$MESS["hino_tab_code"]        = "Код<br/>продукта";
$MESS["hino_tab_size"]        = "Габариты";
$MESS["hino_tab_weight"]      = "Вес";
$MESS["hino_tab_status"]      = "Статус";
$MESS["hino_tab_main"]        = "Главный/<br/>Второстепенный";

$MESS["hino_tab_order"]    = "Заказать";
$MESS["hino_tab_cart"]     = "В корзине";
$MESS["hino_tab_disabled"] = "Уведомить<br/>менеджера";
