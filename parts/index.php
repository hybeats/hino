<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hino. Поиск запчастей");

include_once $_SERVER["DOCUMENT_ROOT"]."/parts/lang/".LANGUAGE_ID."/index.php";
$APPLICATION->AddHeadString('<script type="text/javascript">BX.message('.json_encode($MESS).');</script>');
?>

<script type="text/x-template" id="hino-part">
  <div class="parts-table__row" :class="classStatus()">
    <div v-for="field in fields" class="parts-table__col" :class="classRow(field)" v-html="getPropValue(field)"></div>
    <div class="parts-table__col parts-table__col_status" :class="classStatus()" v-if="info.id && dealer" v-on:click.prevent="$emit('add')">
      <span>{{bxMessButton()}}</span>
    </div>
  </div>
</script>

<section class="parts-page" id="parts-page">
  <div class="parts-page__wrap main-wrap container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <form id="parts-filter" class="parts-page__filter" method="post" v-on:submit.prevent="search">
          <div class="parts-page__filter-wrap">
            <input class="parts-page__input input-default" type="text" v-model.trim="articleSearch" v-on:input="filterArticle" name="vendor_code" placeholder="Артикул">
          </div>
          <div class="parts-page__filter-wrap">
            <input class="parts-page__submit" type="submit" name="submit" value="">
          </div>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div v-if="searchResult.resultPL">
          <div class="parts-table" v-for="resultObj in searchResult.result">
            <div class="parts-table__header">
              <div v-for="field in searchResult.fieldsDisplay" class="parts-table__head" v-html="getMessage(field)"></div>
            </div>
            <hino-part v-for="(part, index) in resultObj"
              :key="index"
              v-on:add="openModal(part.article)"
              v-bind:part="part"
              v-bind:info="searchResult.replacementInfo[part.article]"
              v-bind:fields="searchResult.fieldsDisplay">
            </hino-part>
          </div>
          <div class="parts-page__preview-text parts-page__preview-text_p0">
            <a href="#" class="parts-page__btn" v-on:click.prevent="mail">Уведомить менеджера</a>
          </div>
        </div>
        <div v-else-if="searchResult.result.length > 0" class="parts-page__table">
          <div class="parts-table" v-for="resultObj in searchResult.result">
            <div class="parts-table__header">
              <div v-for="field in searchResult.fieldsDisplay" class="parts-table__head" :class="classHead(field)" v-html="getMessage(field)"></div>
              <div class="parts-table__head parts-table__head_status" v-if="searchResult.dealer" v-html="getMessage('status')"></div>
            </div>
            <hino-part v-for="(part, index) in resultObj"
              :key="index"
              v-on:add="openModal(part.article)"
              v-bind:part="part"
              v-bind:info="searchResult.replacementInfo[part.article]"
              v-bind:dealer="searchResult.dealer"
              v-bind:fields="searchResult.fieldsDisplay">
            </hino-part>
          </div>
        </div>
        <div v-else-if="searchResult.result.length == 0" class="parts-page__preview-text">
          По вашему запросу ничего не найдено. Отправить запрос менеджеру на проверку артикула &laquo;<b>{{lastArticleSearch}}</b>&raquo;<br/>
          <a href="#" class="parts-page__btn" v-on:click.prevent="mail">Уведомить менеджера</a>
        </div>
        <div v-else class="parts-page__preview-text">
          Поиск осуществляется с учетом заменяющих запасных частей.<br/>
          Для целей тестирования Вы можете использовать следующие номера запасных частей: 6700137261, 4851080768
        </div>
      </div>
    </div>
  </div>
</section>

<script src="/js/parts/script.js" charset="utf-8"></script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
