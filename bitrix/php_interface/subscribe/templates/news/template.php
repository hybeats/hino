<?//Запретим прямой вызов скрипта
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('subscribe');
//определяем глобальные переменные и присваиваем им первоначальные значения.
global $SUBSCRIBE_TEMPLATE_RESULT;
$SUBSCRIBE_TEMPLATE_RESULT = false;
global $SUBSCRIBE_TEMPLATE_RUBRIC;
$SUBSCRIBE_TEMPLATE_RUBRIC = $arRubric;
global $APPLICATION;

//Собираем массив новых новостей и формируем шаблон рассылки
$arNews = array();
$lastSendDate = $arRubric["START_TIME"];
CModule::IncludeModule("iblock");
$arSelect = Array("ID", "NAME", "DISPLAY_ACTIVE_FROM", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=> 1, ">=ACTIVE_FROM" => $lastSendDate);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
?>
<?if($res->SelectedRowsCount() > 0):?>
  <?
  $SUBSCRIBE_TEMPLATE_RESULT = true;
  while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arNews[] = $arFields;
  }
  ?>
  <div class="news-mail__list">
    <strong class="news-mail__title">Добрый день! Ознакомьтесь со списком свежих новостей Hino.</strong>
    <?foreach ($arNews as $arItem): ?>
    <div class="news-mail__item">
      <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
      <p class="news-mail__text"><?=$arItem["PREVIEW_TEXT"]?></p>
    </div>
    <?endforeach; ?>
  </div>
<?endif;?>


<?
//создаем выпуск
$new_date = $DB->FormatDate(date("d.m.Y H:i:s"), "DD.MM.YYYY HH:MI:SS", CSite::GetDateFormat("FULL", "ru"));
if($SUBSCRIBE_TEMPLATE_RESULT)
  return array(
    "SUBJECT"         => $arRubric["NAME"],
    "BODY_TYPE"       => "html",
    "CHARSET"         => "UTF-8",
    "DIRECT_SEND"     => "Y",
    "FROM_FIELD"      => $arRubric["FROM_FIELD"],
    "AUTO_SEND_FLAG"  => "Y",
    "AUTO_SEND_TIME"  => $new_date
  );
else
  return false;
?>
