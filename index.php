<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hino. Официальный дистрибьютор Hino Motors Ltd");
?>
<section class="banner">
  <img class="banner__img" src="/images/main-page-banner.jpg" alt="hino">
</section>

<section class="main-news">
  <div class="main-news__wrap main-wrap container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <strong class="main-news__title">Новости</strong>
      </div>
    </div>
    <?$APPLICATION->IncludeFile("/include/main_page_news.php", Array(), Array());?>
  </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
