Vue.component('hino-modal', {
  template:
    '<div class="hino-popup" :class="popupMod()" v-show="show">'+
      '<div class="hino-popup__container">'+
        '<div class="hino-popup__wrap">'+
          '<div class="hino-popup__header">'+
            '<strong class="hino-popup__title">{{title}}</strong>'+
            '<span class="hino-popup__close" v-on:click.prevent="onClose"></span>'+
          '</div>'+
          '<div class="hino-popup__content">'+
            '<slot></slot>'+
            '<div class="hino-popup__button-wrap">'+
              '<button v-on:click.prevent="onSubmit" class="hino-popup__btn hino-popup__btn_submit">ОК</button>'+
              '<button v-if="showCancel" v-on:click.prevent="onClose" class="hino-popup__btn hino-popup__btn_cancel">Отмена</button>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
    '</div>',
  props:[ 'show', 'showCancel', 'title', 'mod' ],
  methods: {
    popupMod: function(){
      return (this.mod)? 'hino-popup_' + this.mod: false;
    },
    onSubmit: function(){
      this.$emit('submitModal');
      $('body').removeClass('body_modal-open');
    },
    onClose: function(){
      this.$emit('closeModal');
      $('body').removeClass('body_modal-open');
    }
  }
});

var HinoModalConfirmVue = Vue.extend({
  template:
    '<hino-modal :show="show" :showCancel="showCancel" :title="title" v-on:submitModal="submit" v-on:closeModal="close">'+
      '<div class="hino-popup__text">{{text}}</div>'+
    '</hino-modal>',
  data: function(){
    return {
      show:       false,
      showCancel: true,
      title:      '',
      text:       '',
      callback:   false
    }
  },
  methods: {
    submit: function(){
      if(typeof this.callback == 'function'){
        this.callback();
      }
      this.show = false;
    },
    close: function(){
      this.show = false;
    }
  }
});

var HinoModalVue = Vue.extend({
  template:
    '<hino-modal :show="option.show" :showCancel="option.showCancel" :title="option.title" :mod="option.mod" v-on:submitModal="close" v-on:closeModal="close">'+
      '<div class="hino-popup__text" v-html="option.text"></div>'+
    '</hino-modal>',
  data: function(){
    return {
      option :{
        show:       false,
        mod:        '',
        showCancel: false,
        title:      '',
        text:       ''
      }
    }
  },
  methods: {
    close: function(){
      this.option.show = false;
    }
  }
});

var HinoModalAddPartVue = Vue.extend({
  template:
    '<hino-modal :show="option.show" :showCancel="option.showCancel" :title="option.title" v-on:submitModal="submit" v-on:closeModal="close">' +
        '<span class="hino-popup__spinner ui-spinner ui-corner-all ui-widget ui-widget-content" v-if="option.showQuantity">'+
          '<a v-on:click.prevent="decrement" class="hino-popup__dec" role="button"></a>'+
          '<input class="hino-popup__number" type="text" v-model="value.quantity">'+
          '<a v-on:click.prevent="increment" class="hino-popup__inc" role="button"></a>'+
        '</span>'+
        '<div v-if="option.showDelivery" class="hino-popup__select-wrap">'+
          '<span class="hino-popup__select-title">Тип доставки</span>' +
          '<select class="select-default hino-popup__select" v-model="value.delivery" name="delivery">' +
            '<option v-for="item in deliveryArrFormated" :value="item.id">{{item.name}}</option>'+
          '</select>'+
        '</div>'+
    '</hino-modal>',
  mounted: function(){
    var _this = this;
    $.ajax({
      type:     "POST",
      dataType: 'json',
      url:      '/ajax/order/index.php',
      data:     { action: 'getdelivery', sessid: BX.bitrix_sessid() },
    })
    .done(function(data){
      _this.deliveryArr = data.result.delivery;
    })
    .fail(function( jqXHR, textStatus){
      console.log(textStatus);
    });
  },
  data: function(){
    return {
      option: {
        show:         false,
        showCancel:   false,
        showQuantity: true,
        showDelivery: false,
        store:        false,
        title:        '',
      },
      callback: false,
      value: {
        delivery: 6,
        quantity: false,
      },
      part:          false,
      deliveryArr:   false
    }
  },
  computed:{
    deliveryArrFormated: function(){
      var result = [];
      for (var i in this.deliveryArr) {
        if (this.deliveryArr.hasOwnProperty(i)) {
          if(this.deliveryArr[i].id == 10 ){
            if(this.option.store == 4 || this.option.store == 0){
              result.push(this.deliveryArr[i]);
            }
          }else{
            result.push(this.deliveryArr[i]);
          }
        }
      }
      return result;
    }
  },
  watch: {
    part: function(val) {
      this.value.quantity = parseInt(val.quantity);
      this.value.delivery = parseInt(val.delivery);
    },
    'value.quantity': function (val) {
      var valStr = val.toString();
      if (!valStr.match(/^[\d]{0,}$/)){
        this.value.quantity = 1;
      }else{
        this.value.quantity = parseInt(val) > parseInt(this.part.max) ? this.part.max : parseInt(val) < 1 ? 1 : val;
      }
    }
  },
  methods: {
    increment: function(){
      this.value.quantity = (this.value.quantity < this.part.max)? this.value.quantity + 1 : this.part.max;
    },
    decrement: function(){
      this.value.quantity = (this.value.quantity > 0)? this.value.quantity - 1 : 0;
    },
    submit: function(){
      if(typeof this.callback == 'function'){
        this.callback(this.part, this.value.quantity, this.value.delivery);
      }
      this.option.show = false;
    },
    close: function(){
      this.option.show = false;
    }
  }
});

var HinoModalAddCommentVue = Vue.extend({
  template:
    '<hino-modal :show="option.show" :showCancel="option.showCancel" :mod="option.mod" :title="option.title" v-on:submitModal="submit" v-on:closeModal="close">' +
        '<textarea class="hino-popup__textarea" v-model.trim="comment" v-on:input="filterComment">{{comment}}</textarea>'+
    '</hino-modal>',
  data: function(){
    return {
      option: {
        show:       false,
        showCancel: true,
        title:      'Добавить комментарий',
        mod:        'comment'
      },
      callback: false,
      comment:    false
    }
  },
  methods: {
    filterComment: function(event) {
      var value = event.target.value;
      this.comment = value.substring(0, 500);
    },
    submit: function(){
      if(typeof this.callback == 'function'){
        this.callback(this.comment);
      }
      this.option.show = false;
    },
    close: function(){
      this.option.show = false;
    }
  }
});

var HinoConfirm = {
  instance: false,
  open:function(option, callback){
    $('body').addClass('body_modal-open');
    if (!this.instance) {
      this.instance = new HinoModalConfirmVue({
        el: document.createElement('div')
      });
      document.body.appendChild(this.instance.$el);
    }
    if(!this.instance.show){
      this.instance.callback = callback;
      this.instance.show = true;
      this.instance.title = option.title;
      this.instance.text  = option.text;
    }
  }
};

var HinoModal = {
  instance: false,
  open:function(option){
    $('body').addClass('body_modal-open');
    if (!this.instance) {
      this.instance = new HinoModalVue({
        el: document.createElement('div')
      });
      document.body.appendChild(this.instance.$el);
    }
    if(!this.instance.show){
      Object.assign(this.instance.option, option, {show: true});
    }
  }
};

var HinoModalAddPart = {
  instance: false,
  open:function(part, option, callback){
    $('body').addClass('body_modal-open');
    if (!this.instance) {
      this.instance = new HinoModalAddPartVue({
        el: document.createElement('div')
      });
      document.body.appendChild(this.instance.$el);
    }
    if(!this.instance.show){
      Object.assign(this.instance.option, option, {show: true});

      this.instance.part     = part;
      this.instance.callback = callback;
    }
  }
};

var HinoModalAddComment = {
  instance: false,
  open:function(comment, callback){
    $('body').addClass('body_modal-open');
    if (!this.instance) {
      this.instance = new HinoModalAddCommentVue({
        el: document.createElement('div')
      });
      document.body.appendChild(this.instance.$el);
    }
    if(!this.instance.show){
      Object.assign(
        this.instance,
        {
          comment:  comment,
          callback: callback
        }
      );

      this.instance.option.show = true;
    }
  }
};
