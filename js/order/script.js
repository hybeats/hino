Vue.component('hino-part', {
  template: '#hino-part',
  props: ['part', 'shipping', 'store', 'index'],
  computed: {
    resultCost: function () {
      return (this.part.count * this.part.price);
    },
    resultWeight: function () {
      return (parseFloat(this.part.weight) * this.part.count).toFixed(2);
    }
  }
});
Vue.component('hino-order', {
  template: '#hino-order',
  props: ['shipping', 'order', 'store'],
  computed: {
    orderWeight: function(){
      var result = 0;
      for (var part in this.order.parts) {
        if (this.order.parts.hasOwnProperty(part)) {
          result += parseFloat(this.order.parts[part].weight) * this.order.parts[part].count;
        }
      }
      return result.toFixed(2);
    },
    orderCost : function(){
      var result = 0;
      for (var part in this.order.parts) {
        if (this.order.parts.hasOwnProperty(part)) {
          result += this.order.parts[part].price;
        }
      }
      return result;
    },
    orderResultCost : function(){
      var result = 0;
      for (var part in this.order.parts) {
        if (this.order.parts.hasOwnProperty(part)) {
          result += (this.order.parts[part].count * this.order.parts[part].price);
        }
      }

      result += this.order.delivery_price;
      return result;
    }
  },
  methods: {
    removePart: function(part){
      HinoConfirm.open(
        {
          title: 'Подтверждение удаления',
          text: 'Удалить позицию "' + part.article + '" из заказа?'
        },
        function(){
          this.$emit('remove', part);
        }.bind(this)
      );
    },
    editPart: function(part){
      var max = (part.storeId == 0)? 9999: parseInt(part.available) + parseInt(part.count);

      if(max > 0){
        HinoModalAddPart.open(
          {
            part_id:        part.id,
            reservation_id: part.reservation_id,
            article:        part.article,
            quantity:       part.count,
            max:            max,
            delivery:       part.deliveryId
          },
          {
            title:        'Редактирование',
            showDelivery: true,
            showQuantity: part.edit,
            store:        part.storeId
          },
          function(part, quantity, delivery){
            this.$emit('edit', part, quantity, delivery);
          }.bind(this)
        );
      }else{
        HinoHelper.toasts.showToast('Недостаточно на складе!', {theme: 'error'});
      }
    }
  }
});

// создание корневого экземпляра
var HinoOrderPage = new Vue({
  el: '#order-page',
  data: {
    urlOrder: '/ajax/order/index.php',
    urlPart:  '/ajax/parts/index.php',
    formAdd:{
      article:  '',
      quantity: 1,
      delivery: null
    },
    info: {
      delivery: null,
      stores:   null,
    },
    orders: false,
    countParts: 0
  },
  methods: {
    filterArticle: function(event) {
      var value = event.target.value;
      this.formAdd.article = value.replace(/[^\#\-0-9a-zA-Z]/gim,'').toUpperCase();
    },
    init: function(){
      this.fetchInitData();
    },
    onFileChange: function(e){
      var files = e.target.files || e.dataTransfer.files;
      if (files.length > 0){
        if (files[0].type != 'text/plain'){
          HinoHelper.toasts.showToast('Неверный тип файла!', {theme: 'error'});
        }else{
          var formData = new FormData();
          formData.append('file', files[0]);

          HinoConfirm.open(
            {
              title: 'Загрузка файла',
              text:  'Загрузить запчасти из файла ' + files[0].name + '?'
            },
            function(){
              formData.append('action', 'loadfile');
              formData.append('sessid', BX.bitrix_sessid());

              var _this = this;
              HinoHelper.loader.show();

              $.ajax({
                url:         _this.urlPart,
                data:        formData,
                processData: false,
                contentType: false,
                dataType:    'json',
                type:        'POST'
              })
              .always(function(){
                HinoHelper.loader.hide();
                bx_basket_HINO.refreshCart({}); // обновление компонента sale.basket.basket.line
              })
              .done(function(data){
                HinoHelper.showMess(data.mess);

                if(typeof data.popup != 'undefined'){
                  HinoModal.open({
                    title: 'Статус загрузки файла',
                    text:  data.popup.text,
                    mod:   'file-load'
                  });
                }

                _this.fetchInitData();
              })
              .fail(function( jqXHR, textStatus){
                HinoHelper.toast.showToast('Неизвестная ошибка: ' + textStatus, {theme: 'error'});
              });

            }.bind(this)
          );

          //очищяем input file
          e.target.value = "";
        }
      }
    },
    getDeliveryToID: function(id){
      var result = false;

      this.info.delivery.forEach(function(item) {
        if(item.id == id){
          result = item;
          return;
        }
      });

      return result;
    },
    emptyOrders: function(){
      var result = true;

      for (var storeID in this.orders) {
        if (this.orders.hasOwnProperty(storeID)) {
          if(!this.emptyStore(storeID)){
            result = false;
            break;
          }
        }
      }

      return result;
    },
    emptyStore: function(storeID){
      var result = true;

      for (var deliveryId in this.orders[storeID]) {
        if (this.orders[storeID].hasOwnProperty(deliveryId)) {
          if(this.orders[storeID][deliveryId].parts.length > 0){
            result = false;
            break;
          }
        }
      }

      return result;
    },
    fetchInitData: function(){
      HinoHelper.fetch(
        this.urlOrder,
        {action: 'getbasket'},
        function(data){
          if(typeof data.error == 'undefined' &&  typeof data.result != 'undefined'){
            this.info = {
              delivery: data.result.delivery,
              stores:   data.result.stores
            };
            this.info.stores.push({
              id:      0,
              title:   'Предзаказ',
              uf_code: 'pre_order'
            });

            this.orders = data.result.orders;
            this.formAdd.delivery = "6"; // SEA по умолчанию
          }
        }.bind(this)
      );
    },
    addPart: function(){
      if(!this.formAdd.article) return false;

      HinoHelper.fetch(
        this.urlPart,
        {
          action:   'addbasket',
          type_add: 'add',
          article:  this.formAdd.article,
          quantity: this.formAdd.quantity,
          delivery: this.formAdd.delivery,
        },
        function(data){
          if(typeof data.result != 'undefined'){
            this.fetchInitData();
          }
          if(typeof data.popup != 'undefined'){
            HinoModal.open({
              title: 'Недостаточное количество',
              text:  data.popup.text
            });
          }

          this.formAdd.article = '';
          this.formAdd.quantity = 1;
        }.bind(this),
        function(){
          bx_basket_HINO.refreshCart({}); // обновление компонента sale.basket.basket.line
        }
      );
    },
    removePart: function(part){
      HinoHelper.fetch(
        this.urlPart,
        {
          action:     'removebasket',
          part_id:    part.id,
          deliveryId: part.deliveryId,
          article:    part.article,
          storeId:    part.storeId
        },
        function(){
          this.fetchInitData();
        }.bind(this),
        function(){
          bx_basket_HINO.refreshCart({}); // обновление компонента sale.basket.basket.line
        }
      );
    },
    editPart: function(part, quantity, delivery){
      HinoHelper.fetch(
        this.urlPart,
        {
          action:         'updatepart',
          part_id:        part.part_id,
          reservation_id: part.reservation_id,
          article:        part.article,
          quantity:       quantity,
          delivery:       delivery
        },
        function(){
          this.fetchInitData();
        }.bind(this),
        function(){
          bx_basket_HINO.refreshCart({}); // обновление компонента sale.basket.basket.line
        }
      );
    },
    createOrders: function(){
      HinoConfirm.open(
        {
          title: 'Подтверждение',
          text: 'Оформить заказ?'
        },
        function(){
          var comments = {};

          $.each(this.orders, function functionName(storeID, storeOrders) {
            $.each(storeOrders, function functionName(deliveryID, order) {
              if(order.comment){
                comments[storeID + '_' + deliveryID] = order.comment;
              }
            });
          });

          HinoHelper.fetch(
            this.urlOrder,
            { action: 'createorder', comments: comments },
            function(){
              this.fetchInitData();
            }.bind(this),
            function(){
              bx_basket_HINO.refreshCart({}); // обновление компонента sale.basket.basket.line
            }
          );
        }.bind(this)
      );
    },
    clearBasket: function(){
      HinoConfirm.open(
        {
          title: 'Подтверждение',
          text: 'Очистить корзину?'
        },
        function(){
          HinoHelper.fetch(
            this.urlOrder,
            { action: 'clearbasket' },
            function(){
              this.fetchInitData();
            }.bind(this),
            function(){
              bx_basket_HINO.refreshCart({}); // обновление компонента sale.basket.basket.line
            }
          );
        }.bind(this)
      );
    },
    updateComment: function(order){
      var orderComment = order.comment ? order.comment : '';
      HinoModalAddComment.open(
        orderComment,
        function(comment){
          order.comment = comment;
        }.bind(this)
      );
    },
    clearComment: function(order){
      order.comment = '';
    }
  }
});

$(function() {
  HinoOrderPage.init();
});
