$(document).ready(function() {
  // fixed header
  $( window ).scroll(function () {
    var header        = $('.header'),
        orgElementTop = header.outerHeight();

    if ($(window).scrollTop() >= orgElementTop + 100) {
      if(!header.hasClass('header_fixed')){
        header.addClass('header_fixed');
        $('main.content').css({
          paddingTop: orgElementTop + 'px'
        });
      }
    }
    if ($(window).scrollTop() <= orgElementTop) {
      if(header.hasClass('header_fixed')){
        header.removeClass('header_fixed');
        $('main.content').css({
          paddingTop: '0px'
        });
      }
    }
  });

  //hamburger menu
  $('.hamburger').click(function(){
    var screenWidth = document.documentElement.clientWidth;

		$(this).toggleClass('open');
    if( $(this).hasClass('open')) {
      $(this).css('transform', 'translate('+(-screenWidth+115)+'px)');
    }else {
      $(this).css('transform', 'translate(30px)');
    }
    $('.header-menu').fadeToggle();
	});

  //user page tabs
  $('.toggle-trigger').click(function(e) {
    e.preventDefault();
    var thisTrigger = $(this),
    thisParentValue = thisTrigger.parent().data('tab') || thisTrigger.parent().parent().data('tab'),
    toggleValue = thisTrigger.data('toggle'),
    togglecontent = $('.toggle-content[data-content="'+toggleValue+'"]');

    $('[data-tab="'+thisParentValue+'"] > .toggle-trigger').removeClass('js-active');
    $('.toggle-content[data-tab="'+thisParentValue+'"]').removeClass('js-active');
    thisTrigger.addClass('js-active');
    togglecontent.stop().addClass('js-active');
  });

  // main page news
  var newsWrap = $('.main-news').find('.main-news__wrap');
  $('body').on( 'click', '.js-load', function(e) {
    e.preventDefault();
    $.ajax({
      type: 'GET',
      url: $(this).attr('href')
    })
    .done(function(html){
      newsWrap.find('.button-wrap').remove();
      newsWrap.append(html);
    });
  });


  //subscribe-form
  var _form = $('#news-subscribe');
  var form_data = $('#news-subscribe').serialize();
  $.ajax({
    type: "POST",
    dataType: 'json',
    url: "/ajax/forms/subscription_check.php",
    data: form_data,
    success: function(result) {
      _form.find('input[name="sub_status"]').val(result.SUBSCRIBE_STATUS);
      _form.find('input[name="sub_exists"]').val(result.SUBSCRIBER_EXISTS);
      if (result.SUBSCRIBE_STATUS == "Y") {
        _form.find('input[name="action"]').val('unsubscribe');
        _form.find('.subscribe-form__label').text('Вы уже подписаны на рассылку');
        _form.find('.subscribe-form__submit').val('Отписаться');
      }
    }
  });
  $('#news-subscribe').validate({
    submitHandler: function(form) {
      var _form_data = $(form).serialize();
      $.ajax({
        type: "POST",
        dataType: 'json',
        url: "/ajax/forms/subscribe.php",
        data: _form_data,
        success: function(result) {
          $('.subscribe-form__result').html(result.MESSAGE);
        }
      });
    }
  });

});

Number.prototype.formatPrice = function(c, d, t){
  var n = this,
      c = isNaN(c = Math.abs(c)) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? " " : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
      j = (j = i.length) > 3 ? j % 3 : 0;
  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

var HinoHelper = {
  fetch: function(url, datafetch, callbackDone, callbackAlways){
    if(this.loader) this.loader.show();
    var _this = this;

    $.extend(
      datafetch,
      { sessid: BX.bitrix_sessid() }
    );

    $.ajax({
      type:     "POST",
      dataType: 'json',
      url:      url,
      data:     datafetch,
    })
    .always(function(){
      if(_this.loader) _this.loader.hide();
      if(typeof callbackAlways == 'function'){
        callbackAlways();
      }
    })
    .done(function(data){
      if(typeof data.mess != 'undefined' && data.mess.length > 0){
        data.mess.forEach(function(item){
          _this.toasts.showToast(
            item.text,
            {theme: item.type}
          );
        });
      }
      _this.showMess();
      if(typeof callbackDone == 'function'){
        callbackDone(data);
      }
    })
    .fail(function( jqXHR, textStatus){
      _this.toasts.showToast('Неизвестная ошибка: ' + textStatus, {theme: 'error'});

      $.ajax({
        type:     "POST",
        dataType: 'json',
        url:      '/ajax/helper/index.php',
        data:     {
          action:       'log',
          sessid:       BX.bitrix_sessid(),
          responseText: jqXHR.responseText,
          textStatus:   textStatus
        },
      });
    });
  },
  showMess: function(arrMess){
    if(typeof arrMess != 'undefined' && arrMess.length > 0){
      var _this = this;

      arrMess.forEach(function(item){
        _this.toasts.showToast(
          item.text,
          {theme: item.type}
        );
      });
    }
  }
}

//глобальные Vue component
var hinoLoaderComponent = Vue.extend({
  template: '<div class="loader" v-bind:class="{ loader_on: on }"><div class="loader__loading">Пожалуйста, подождите...</div></div>',
  data: function () {
    return {
      on: false
    }
  },
  methods: {
    show: function(){ this.on = true; },
    hide: function(){ this.on = false;}
  }
});

$(function() {
  //монтируем глобальные Vue компоненты
  HinoHelper.loader = new hinoLoaderComponent({ el: document.createElement('div') });
  document.getElementById('main-content').appendChild(HinoHelper.loader.$el);

  var hinoToastsComponent = Vue.extend(window.vueToasts.default)
  HinoHelper.toasts = new hinoToastsComponent({ el: document.createElement('div') });
  document.body.appendChild(HinoHelper.toasts.$el);
});
