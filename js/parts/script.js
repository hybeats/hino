Vue.component('hino-part', {
  template: '#hino-part',
  props: ['part', 'info', 'fields', 'dealer'],
  computed: {
    status: function(){
      var status = false;

      if(!this.info.id) {
        status = 'disabled';
      }else if (this.info.quantity > 0) {
        status = 'cart';
      }else{
        status = 'order';
      }
      return status;
    }
  },
  methods:{
    classStatus: function(){
      return 'status_' + this.status;
    },
    classRow: function(mod){
      return 'parts-table__col_' + mod;
    },
    getPropValue: function(field){
      var strField = '';

      switch (field) {
        case 'number':
          strField = this.info.fields.number;
          if(strField != this.$parent.lastArticleSearch){
            strField += '<br/>(замена)'
          }
          break;
        case 'type':
          strField = (this.part.type)? this.part.type: '';
          break;
        case 'price':
        case 'price_mrc':
          strField = this.info.fields[field].formatPrice() + ' руб.';
          break;
        default:
          strField = this.info.fields[field];
      }
      return strField;
    },
    bxMessButton: function(){
      var str = BX.message("hino_tab_" + this.status);
      if(this.info.quantity) str += ' (' + this.info.quantity + ')';
      return str;
    }
  }
});

// создание корневого экземпляра
new Vue({
  el: '#parts-page',
  data: {
    urlPart: '/ajax/parts/index.php',
    lastArticleSearch: '',
    articleSearch: '',
    searchResult: {
      fieldsDisplay:   false,
      replacementInfo: false,
      result:          false
    }
  },
  mounted: function(){
    var article = this.getUrlParameter('article');
    if(article){
      this.articleSearch = article;
      this.search();
    }
  },
  methods:{
    getUrlParameter: function(name) {
      name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
      var regex = new RegExp('[\\?&]' + name + '=([^&]*)');
      var results = regex.exec(location.href);
      return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    },
    filterArticle: function(event) {
      var value = event.target.value;
      this.articleSearch = value.replace(/[^\#\-0-9a-zA-Z]/gim,'').toUpperCase();
    },
    classHead: function(mod){
      return 'parts-table__head_' + mod;
    },
    getMessage: function(name){
      return BX.message("hino_tab_" + name);
    },
    search: function(){
      if(!this.articleSearch) return false;
      this.searchResult.result = false;

      HinoHelper.fetch(
        this.urlPart,
        {action: 'search', vendor_code: this.articleSearch},
        function(data){
          this.articleSearch = '';
          if(typeof data.result != 'undefined'){
            this.lastArticleSearch            = data.article_search;
            this.searchResult.fieldsDisplay   = data.fields_display;
            this.searchResult.replacementInfo = data.replacement_info;
            this.searchResult.result          = data.result;
            this.searchResult.resultPL        = data.resultPL;
            this.searchResult.dealer          = data.dealer;
          }
        }.bind(this)
      );
    },
    mail: function(){
      if(!this.lastArticleSearch) return false;

      HinoHelper.fetch(
        '/ajax/mail/index.php',
        {
          action:  'article_not_found',
          article: this.lastArticleSearch
        },
        function(){
          this.lastArticleSearch     = '';
          this.searchResult.result   = false;
          this.searchResult.resultPL = false;
        }.bind(this)
      );
    },
    openModal: function(article){
      if(!article) return false;

      var part = this.searchResult.replacementInfo[article];
      HinoModalAddPart.open(
        {
          part_id:  part.id,
          article:  part.fields.number,
          quantity: part.quantity,
          max:      (part.preorder)? 9999: part.fields.presence,
          preorder: part.preorder
        },
        {
          title: 'Добавить в заказ',
        },
        function(part, quantity){

          HinoHelper.fetch(
            this.urlPart,
            {
              action:   'addbasket',
              part_id:  part.part_id,
              quantity: quantity,
              article:  part.article,
              preorder: +part.preorder
            },
            function(data){
              if(typeof data.result != 'undefined'){
                this.searchResult.replacementInfo[data.result.article].quantity = data.result.quantity;
              }
            }.bind(this),
            function(){
              bx_basket_HINO.refreshCart({}); // обновление компонента sale.basket.basket.line
            }
          );
        }.bind(this)
      );
    }
  }
});
