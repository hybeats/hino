<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hino. 404");
?>
<div class="main-wrap container-fluid">
  <div class="page-404">
    <h1 class="page-404__title">404</h1>
    <h3 class="page-404__subtitle">Страница не найдена</h3>
  </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
