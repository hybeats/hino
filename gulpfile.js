var gulp            = require('gulp'),
    path            = require('path'),
    browserSync     = require('browser-sync'),
    gulpLoadPlugins = require('gulp-load-plugins');


var reload = browserSync.reload;
var $ = gulpLoadPlugins();

var path = {
  'images': ['images/**/*' , 'local/**/*.{jpeg,png,svg}'],
  'styles': ['css/**/*.scss', 'local/**/*.scss', 'local/**/.default/**/*.scss','local/**/.default/**/**/**/.default/*.scss']
};

gulp.task('images', function(){
  gulp.src(path.images, {base: './'})
      .pipe(
        $.imagemin({
          progressive: true,
          interlaced: true
        })
      )
      .pipe(gulp.dest('./'));
});

gulp.task('styles', function(){
  var AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
  ];

  return gulp.src(path.styles, {base: './'})
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe(gulp.dest('./'));
});

gulp.task('watch', ['styles'], function(){
  browserSync.init({
    proxy: 'hino'
  });

  gulp.watch( path.styles, ['styles', reload]);
  // gulp.watch( path.images, ['images', reload] );
});
gulp.task('default', ['watch']);
gulp.task('imgmin' , ['images']);
