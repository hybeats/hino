@import "normalize.scss";
@import "fonts.scss";
html, body {
  font-size: 10px;
  font-family: 'HelveticaNeueCyr', Arial, sans-serif;
  height: 100%;
  min-width: 300px;
}
.body_modal-open{
  overflow: hidden;
}
.main-wrap {
  max-width: 1200px;
  margin: 0 auto;
}
.main-content {
  position: relative;
}
.title-h1 {
  font-size: 2.4rem;
  color: #040404;
  text-transform: uppercase;
  font-weight: bold;
  margin-top: 30px;
  margin-bottom: 30px;
}
.news-title {
  text-align: left;
  display: block;
  font-weight: 500;
  font-size: 1.8rem;
  color: #303030;
  text-decoration: none;
}
.news-date {
  text-align: left;
  display: block;
  color: #a7a7a7;
  font-size: 1.4rem;
  font-style: italic;
  font-weight: normal;
  position: relative;
}
.news-text {
  text-align: left;
  font-size: 1.4rem;
  color: #3d3d3d;
}
.input-default {
  outline: 0;
  border: 0;
  border-bottom: 1px solid #dadada;
  padding: 10px;
  font-size: 1.6rem;
  color: #303030;
  font-weight: 400;
}
.input-file {
  overflow: hidden;
  position: relative;
  &:hover .input-file__btn {
    background: darken(#e6e6e6, 20%);
  }
  &__btn {
    width: 100%;
    color: #000000;
    font-size: 1.6rem;
    outline: 0;
    border: 0;
    background: #e6e6e6;
    padding: 10px 20px 10px;
  }
  &__hidden {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    transform: scale(20);
    letter-spacing: 10em;     /* IE 9 fix */
    -ms-transform: scale(20); /* IE 9 fix */
    opacity: 0;
    cursor: pointer
  }
  &__text {
    margin-top: 10px;
    display: block;
    text-align: center;
    font-size: 1.4rem;
  }
}
.select-default {
  outline: 0;
  border: 0;
  border-bottom: 1px solid #dadada;
  padding: 10px;
  font-size: 1.6rem;
  color: #000000;
  font-weight: 400;
  width: 100%;
}
.table-button-wrap{
  width: 30px;
  height: 23px;
  display: inline-block;
  vertical-align: middle;
}
.table-button {
  width: 23px;
  height: 23px;
  display: inline-block;
  vertical-align: middle;
  padding: 0 10px;
  border: none;
  background-color: transparent;
  cursor: pointer;

  &_delete {
    background: url(/images/icons/delete.svg) no-repeat center;
    background-size: cover;
    transition: transform 0.3s ease-out;
    &:hover {
      transform: rotate(180deg);
    }
  }

  &_edit {
    background: url(/images/icons/edit.svg) no-repeat center;
    background-size: cover;
  }
}
.add-button {
  position: absolute;
  outline: none;
  right: 0;
  top: 50%;
  margin-top: -16px;
  border: 0;
  padding: 0;
  width: 32px;
  height: 32px;
  background: url(/images/icons/add.png) no-repeat center, transparent;
  &:hover {
    background: url(/images/icons/add_hover.png) no-repeat center, transparent;
  }
}

.loader{
  position: absolute;
  background-color: rgba(255, 255, 255, 0.8);
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  z-index: 100;
  display: none;

  &_on{
    display: block;
  }

  &__loading {
    position: absolute;
    top: 50%;
    left: 0;
    font-size: 1.8rem;
    text-align: center;
    color: #4a4a4a;
    white-space: nowrap;
    width: 100%;

    &:before{
      content: "";
      display: inline-block;
      vertical-align: middle;
      margin-right: 20px;
      width: 23px;
      height: 23px;
      background: url('/images/icons/loading.png') no-repeat center;
      animation: spin 2s linear infinite;
    }
  }
}

/**
  * global table styles
  */
.table {
  font-size: 1.4rem;
  font-weight: 400;
  text-align: center;
  display: table;
  width: 100%;
  &__row {
    display: table-row;
    width: 100%;
    position: relative;
    & .table__col {
      border-top: 1px dashed #c1c1c1;
      border-left: 1px dashed #c1c1c1;
      &:last-of-type {
        border-right: 1px dashed #c1c1c1;
      }
    }
    &:last-child {
      .table__col {
        border-bottom: 1px dashed #c1c1c1;
      }
    }
  }
  &__col {
    display: table-cell;
    padding-top: 20px;
    padding-bottom: 20px;
    vertical-align: middle;
  }
  &__header {
    display: table-row;
    width: 100%;
    background: #888787;
    color: #ffffff;
  }
  &__head {
    display: table-cell;
    padding-top: 20px;
    padding-bottom: 20px;
    vertical-align: middle;
    background: #888787;
  }
}

/**
  * subscribe form styles
  */
.subscribe-form {
  margin: 20px 0;
  text-align: center;
  &__label {
    display: inline-block;
    color: #e30007;
    font-size: 1.8rem;
    font-weight: 400;
    margin-right: 30px;
  }
  &__submit {
    display: inline-block;
    font-size: 1.8rem;
    color: #ffffff;
    background: #e30007;
    padding: 14px 20px;
    border: 0;
    outline: 0;
    cursor: pointer;
    &:hover {
      text-decoration: underline;
    }
  }
  &__result {
    display: block;
    font-size: 1.4rem;
    padding: 14px 0;
  }
}


/**
  * news-list styles
  */
.news-list {
  &.row {
    padding-top: 50px;
    &:first-of-type {
      padding-top: 0;
    }
  }
  &__image {
    display: block;
    width: auto;
    height: 240px;
    margin: 0 auto;
    color: #3d3d3d;
    outline: none;
  }
  &__item {
    padding-bottom: 50px;
    &:nth-child(3n-2) {
      border-bottom: 1px dashed #dadada;
    }
    &:hover {
      & > .news-list__title {
        color: #e30007;
      }
      & > .news-list__date {
        &:before {
          display: block;
        }
      }
    }
  }
  &__date {
    text-align: left;
    padding-top: 25px;
    display: block;
    color: #a7a7a7;
    font-size: 1.4rem;
    font-style: italic;
    font-weight: normal;
    position: relative;
    &:before {
      content: "";
      display: none;
      position: absolute;
      top: 0;
      left: 0;
      width: 30%;
      height: 2px;
      border-bottom: 2px solid #e30007;
    }

  }
  &__title {
    text-align: left;
    display: block;
    margin: 22px 0;
    font-weight: 500;
    font-size: 1.8rem;
    color: #303030;
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
  &__text {
    margin: 0;
    padding: 0;
    text-align: left;
    font-size: 1.4rem;
    color: #3d3d3d;
  }
}

/**
  * header styles
  */
.header {
  width: 100%;
  padding-top: 40px;
  position: relative;
  background: #ffffff;
  opacity: 1;

  &__title {
    font-size: 2.4rem;
    color: #040404;
    text-transform: uppercase;
    font-weight: bold;
  }
  &__subtitle {
    font-size: 1.8rem;
    color: #040404;
    font-weight: normal;
  }
  &__wrap {
    display: table;
    float: left;
  }
  &__link {
    display: table-cell;
    vertical-align: middle;
  }
  &__logo {
    width: 100%;
  }
  &__logo-wrap {
    width: 128px;
    float: left;
    margin-right: 35px;
    margin-left: 15px;
  }
  &__text {
    padding-top: 15px;
    float: left;
  }
  &__logo-clone{
    visibility: hidden;

    & .header__logo-wrap{
      width: 40px;
      padding-top: 8px;
    }
    & .header__text{
      padding-top: 17px;
    }
    & .header__title{
      margin: 0;
      font-size: 2rem;
    }
  }

  &_fixed {
    position: fixed;
    top:0;
    left: 0;
    width: 100%;
    z-index: 500;
    animation: header 0.5s;
    padding-top: 0;

    & .header__container {
      display: none;
    }
    & .header__logo-clone{
      visibility: visible;
    }
  }
}
@keyframes header {
  0%   {
    opacity: 0;
    transform: translateY(-100%);
  }
  100% {
    opacity: 1;
    transform: translateY(0);
  }
}
.navbar {
  list-style: none;
  position: relative;
  &__logo {
    height: 56px;
    position: absolute;
    left: 0;
    top: 0;
    & > a {
      height: 100%;
    }
    & > a > .header__logo {
      width: auto;
      height: 100%;
    }
  }
  &__list {
    display: table-cell;
    vertical-align: bottom;
    text-align: right;
    padding: 15px 0;
  }
  &__text {
    position: relative;
    color: #818181;
    font-weight: normal;
    font-size: 1.4rem;
    &_mail {
      padding-left: 30px;
      &:before {
        content: "";
        display: block;
        position: absolute;
        left: 0;
        top: 50%;
        margin-top: -6px;
        width: 17px;
        height: 12px;
        background: url(/images/icons/mail.png) no-repeat center;
      }
    }
    &_cart {
      padding-left: 30px;
      &:before {
        content: "";
        display: block;
        position: absolute;
        left: 0;
        top: 50%;
        margin-top: -10px;
        width: 20px;
        height: 19px;
        background: url(/images/icons/cart.png) no-repeat center;
      }
    }
  }
  &__link {
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
  &__item {
    display: inline-block;
    vertical-align: middle;
    padding: 5px 18px;
    border-right: 1px solid #ebebeb;
    &:last-child {
      border-right: none;
    }
  }
  &__user {
    color: #303030;
    font-size: 1.4rem;
    vertical-align: middle;
    position: relative;
    text-decoration: none;
    &:before {
      position: absolute;
      left: -34px;
      top: 50%;
      margin-top: -12px;
      content: "";
      width: 24px;
      height: 24px;
      background: url(/images/icons/user.png) no-repeat center;
    }
    &:hover {
      text-decoration: underline;
    }
  }
  &__status {
    margin-left: 10px;
    display: inline-block;
    vertical-align: middle;
    width: 10px;
    height: 10px;
    border-radius: 50%;
    &_active {
      background: #39b54a;
    }
    &_locked {
      background: #e30007;
    }
    &_prepayment {
      background: #ffc000;
    }
  }
}
.hamburger {
  display: none;
}
/**
  * Main page
  */
.banner {
  &__img {
    width: 100%;
  }
}
.content{
  box-sizing: border-box;
  min-height: 100%;
  padding-bottom: 170px;
}
.footer {
  background: #000;
  color: #fff;
  height: 170px;
  margin-top: -170px;
  padding-top: 28px;
  padding-bottom: 20px;

  &__wrap {}
  &__info {
    padding: 10px 12px;
    border-left: 1px solid #3d3d3d;
    margin-bottom: 40px;
  }
  &__title {
    display: block;
    font-weight: bold;
    font-size: 1.4rem;
    text-transform: uppercase;
    margin-bottom: 16px;
  }
  &__subtitle {
    display: block;
    font-weight: normal;
    font-size: 1.4rem;
  }
  &__rights {
    font-size: 1.2rem;
    font-weight: normal;
  }
  &__supportix {
    display: block;
    font-size: 1.2rem;
    font-weight: normal;
    text-align: right;
  }
  &__button-pdf{
    font-size: 1.7rem;
    font-weight: normal;
    display: inline-block;
    background-color: #ff0003;
    background-image: url('/images/icons/pdf-file.svg');
    background-position: 10px center;
    background-repeat: no-repeat;
    background-size: contain;
    color: #fff;
    padding: 10px;
    width: 100%;
    max-width: 260px;
    margin: 25px 0 15px;
    text-decoration: none;
  }
}


/**
  * parts page styles
  */
.parts-page {
  position: relative;
  &__preview-text{
    font-size: 1.6rem;
    text-align: center;
    width: 100%;
    max-width: 800px;
    margin: 0 auto;
    padding: 80px 0;
    color: #4a4a4a;
    line-height: 1.5;

    &_p0{
      padding: 0;
    }
  }
  &__table {
    margin-top: 75px;
    margin-bottom: 75px;
  }
  &__wrap {
    padding-top: 55px;
  }
  &__filter {
    display: block;
    width: 100%;
    text-align: center;
  }
  &__filter-wrap {
    display: inline-block;
    vertical-align: middle;
    margin: 0 15px;
  }
  &__submit {
    outline: none;
    border: 0;
    width: 30px;
    height: 30px;
    background: url(/images/icons/search.svg) no-repeat center;
    cursor: pointer;
  }
  &__btn{
    display: inline-block;
    text-align: center;
    max-width: 200px;
    padding: 13px 15px;
    width: 100%;
    background: #888787;
    color: #ffffff;
    margin: 20px auto;
    text-decoration: none;
    font-size: 1.6rem;

    &:hover{
      background: darken(#888787, 20%);
    }
  }
}
.parts-table {
  font-size: 1.4rem;
  font-weight: 400;
  text-align: center;
  display: table;
  width: 100%;
  margin-top: 35px;
  margin-bottom: 35px;

  &__row {
    display: table-row;
    width: 100%;
    position: relative;
    &:hover {
      & > .parts-table__col {
        background: #ededed;
      }
      & > .parts-table__col_nubmer {
        background: #e6ece8;
      }
      & > .parts-table__col_price {
        background: #ece1e1;
      }
      & > .parts-table__col_code {
        background: #eae4e6;
      }
      & > .parts-table__col_delete {
        background: inherit;
      }
    }
    & .parts-table__col {
      border-top: 1px dashed #c1c1c1;
      border-left: 1px dashed #c1c1c1;
      &:last-of-type {
        border-right: 1px dashed #c1c1c1;
      }
    }
    &:last-child {
      .parts-table__col {
        border-bottom: 1px dashed #c1c1c1;
      }
    }
    & .parts-table__col_empty {
      border-bottom: 0;
      border-left: 0;
      border-right: 0;
      border-top: 0;
    }
    &.status_disabled {
      color: #9a9a9a;
    }
    &.status_cart {
      & .parts-table__col {
        &:first-child {
          border-left: 3px solid #c0e4cd;
        }
        &:last-child {
          border-right: 3px solid #c0e4cd;
        }
      }
    }
    &_result {
      display: table-row;
      width: 100%;
      position: relative;
      & .parts-table__col_number, & .parts-table__col_count {
        background: inherit;
      }
      & .parts-table__col {
        border-top: 1px dashed #c1c1c1;
      }
      & .parts-table__col_addition {
        background: #dadada;
      }
      & .parts-table__col_cost, & .parts-table__col_weight {
        background: #cccbcb;
      }
      & .parts-table__col_result-cost {
        background: #be7088;
        color: #ffffff;
        font-weight: 500;
      }
      &:hover {
        & > .parts-table__col {
          background: inherit;
        }
        & > .parts-table__col_addition {
          background: #dadada;
        }
        & > .parts-table__col_cost, & .parts-table__col_weight {
          background: #cccbcb;
        }
        & > .parts-table__col_result-cost {
          background: #be7088;
        }
      }
    }
  }
  &__col {
    display: table-cell;
    padding: 20px 10px;
    vertical-align: middle;
    &_delete {
      text-align: center;
      vertical-align: middle;
    }
    &_type {
      white-space: nowrap;
    }
    &_number {
      background: #f6fbf8;
    }
    &_price, &_count {
      background: #fef2f2;
    }
    &_code, &_result-cost {
      background: #faf4f6;
    }
    &_status {
      cursor: pointer;
      padding-left: 20px;

      & span {
        position: relative;
        display: inline-block;
        padding-left: 10px;
        color: #be6f88;
        &:before {
          display: inline-block;
          content: "+";
          position: absolute;
          left: -13px;
          top: 50%;
          margin-top: -16px;
          font-size: 26px;
        }
      }
      &:hover span {
        color: #e74c4c;
      }
      &.status_disabled:hover span {
        color: #000000;
      }
    }
    &.status_disabled {
      & span {
        position: relative;
        display: inline-block;
        padding-left: 10px;
        color: #888787;
        &:before {
          display: inline-block;
          content: "!";
          position: absolute;
          left: -5px;
          top: 50%;
          margin-top: -13px;
          font-size: 25px;
        }
      }
    }

    &.status_cart {
      & span {
        position: relative;
        display: inline-block;
        padding-left: 10px;
        color: #1b8568;
        white-space: nowrap;
        &:before {
          display: inline-block;
          content: "✓";
          position: absolute;
          left: -10px;
          top: 50%;
          margin-top: -10px;
          font-size: 20px;
        }
      }
    }
  }
  &__header {
    display: table-row;
    width: 100%;
    background: #888787;
    color: #ffffff;
  }
  &__head {
    display: table-cell;
    padding: 20px 5px;
    vertical-align: middle;
    &_number {
      background: #c0e4cd;
      color: #000000;
    }
    &_price, &_count {
      background: #e74c4c;
    }
    &_moscow, &_shipping {
      background: #b4b4b4;
      color: #000000;
    }
    &_artem {
      background: #cdcdcd;
      color: #000000;
    }
    &_belgium {
      background: #dadada;
      color: #000000;
    }
    &_japan, &_addition {
      background: #e6e3e3;
      color: #000000;
    }
    &_code, &_result-cost {
      background: #cf96a8;
    }
    &_status {
      background: #dadada;
      color: #000000;
      font-style: italic;
    }
  }
}
.hino-popup{
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0,0,0, .6);
  z-index: 1002;
  overflow: auto;

  &__label{
    margin-bottom: 15px;
    display: block;
  }
  &__spinner{
    margin-bottom: 20px;
  }
  &__inc, &__dec{
    border-style: none;
    cursor: pointer;
    vertical-align: middle;
    display: inline-block;
    width: 50px;
    height: 50px;
    margin: 0;
    background: #e6e6e6;
    color: #303030;
    font-size: 1.8rem;
    top: 0;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }
  &__inc{
    left: 100px;
    background: url(/images/icons/plus.svg) no-repeat center, #e6e6e6;
    background-size: 50% 50%;
  }
  &__dec{
    right: 100px;
    background: url(/images/icons/minus.svg) no-repeat center, #e6e6e6;
    background-size: 50% 50%;
  }
  &__container{
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    text-align: center;

    &:before{
      content: '';
      display: inline-block;
      height: 100%;
      vertical-align: middle;
    }
  }
  &__wrap {
    width: 90%;
    max-width: 380px;
    background: #fff;
    display: inline-block;
    vertical-align: middle;
    margin: 40px 0;
  }
  &__header {
    padding: 35px 10px 30px 10px ;
    background: #e6e6e6;
    position: relative;
  }
  &__title {
    display: block;
    text-align: center;
    color: #000000;
    font-size: 1.8rem;
    font-weight: 400;
  }
  &__close {
    position: absolute;
    top: 50%;
    margin-top: -5px;
    right: 30px;
    width: 17px;
    height: 17px;
    background: url(/images/icons/cancel.svg) no-repeat center;
    background-size: cover;
    cursor: pointer;
  }
  &__content {
    padding: 20px;
    text-align: center;
    font-size: 1.8rem;
  }
  &__radio {
    display: block;
    text-align: left;
    margin: 8px 0;
    font-size: 1.6rem;

    & input[type="radio"] {
      margin-right: 10px;
    }
  }
  &__select-wrap, &__text{
    margin-bottom: 15px;
  }
  &__select-title {
    font-size: 16px
  }
  &__select {
    display: block;
    margin: 5px auto;
    width: 220px;
  }
  &__number {
    display: inline-block;
    border: none;
    vertical-align: middle;
    margin: 0;
    background: #e6e6e6;
    color: #303030;
    font-size: 1.8rem;
    padding: 15px;
    margin: 0 25px;
    width: 90px;
    text-align: center;
    -moz-appearance:textfield;
    &::-webkit-outer-spin-button, &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    }
    &:focus {
      outline: 0;
    }
  }
  &__button-wrap{
    text-align: center;
  }
  &__btn {
    display: inline-block;
    width: 110px;
    margin: 0 15px;
    border: 0;
    outline: 0;
    padding: 15px 0;
    font-size: 1.8rem;
    color: #ffffff;
    border-radius: 3px;
    cursor: pointer;

    &_submit{
      background: #79c594;
      &:hover {
        background: darken(#79c594, 5%);
      }
    }
    &_cancel{
      background: #cf96a8;
      &:hover {
        background: darken(#cf96a8, 20%);
      }
    }
  }
  &__submit {
    display: block;
    width: 110px;
    margin: 30px auto 0;
    border: 0;
    outline: 0;
    padding: 20px 0;
    background: #cf96a8;
    color: #ffffff;
    font-size: 1.8rem;
    border-radius: 3px;
    cursor: pointer;
    &:hover {
      background: darken(#cf96a8, 20%);
    }
  }
  &__textarea{
    width: 100%;
    height: 200px;
    font-size: 16px;
    resize: vertical;
  }
  &_file-load{
    & .hino-popup__wrap{
      max-width: 600px;
    }
    & .hino-popup__text{
      text-align: left;
    }
  }
  &_comment{
    & .hino-popup__wrap{
      max-width: 700px;
    }
  }
}
.ui-spinner {
  overflow: visible;
}
.ui-spinner-up, .ui-spinner-down {
  display: block!important;
  width: 60px;
  height: 60px;
  margin: 0;
  background: #e6e6e6;
  color: #303030;
  font-size: 1.8rem;
  cursor: pointer;
}
.ui-spinner-up {
  left: 100px;
  background: url(/images/icons/plus.svg)no-repeat center, #e6e6e6;
  background-size: 50% 50%;
}
.ui-spinner-down {
  right: 100px;
  background: url(/images/icons/minus.svg)no-repeat center, #e6e6e6;
  background-size: 50% 50%;
}
.bottom-fixed {
  position: fixed;
  width: 100%;
  bottom: 0;
}
/**
  * order page styles
  */
.order-page {
  padding-bottom: 40px;
  &__form {
    margin-top: 60px;
  }
  &__title {
    color: #131313;
    font-size: 1.8rem;
  }
  &__submit {
    display: inline-block;
    text-align: center;
    padding: 13px 15px;
    width: 100%;
    background: #888787;
    color: #ffffff;
    margin: 20px auto;
    text-decoration: none;
    font-size: 1.6rem;
    &:hover {
      background: darken(#888787, 20%);
    }
  }
  &__clear {
    display: inline-block;
    text-align: center;
    padding: 13px 15px;
    width: 100%;
    background: #e6e6e6;
    color: #000000;
    margin: 20px auto;
    text-decoration: none;
    font-size: 1.6rem;
    &:hover {
      background: darken(#e6e6e6, 5%);
    }
  }

  &__load-file{
    margin: 0 auto;
    display: block;
    max-width: 250px;
  }
  &__empty{
    font-size: 1.6rem;
    text-align: center;
    width: 100%;
    max-width: 800px;
    margin: 0 auto;
    padding: 80px 0;
    color: #4a4a4a;
    line-height: 1.5;
  }
}
.order-comment{
  border-top: 1px dashed #e6e3e3;
  border-bottom: 1px solid #e6e3e3;

  &__buttons{
    text-align: right;
    padding-right: 40px;
  }
  &__button{
    display: inline-block;
    text-align: center;
    padding: 13px 15px;
    width: 100%;
    background: #e6e6e6;
    color: #000000;
    margin: 20px auto;
    text-decoration: none;
    font-size: 1.6rem;
    border: none;
    max-width: 300px;
  }
  &__text{
    font-size: 16px;
    padding: 10px 0;
    display: block;
    text-align: left;
  }
}
.order-form {
  &__col {
    padding-bottom: 20px;
  }
  &__input {
    width: 100%;
  }
  &__btn {
    display: block;
    width: 100%;
    margin: 0 auto;
  }
  &__select {
    width: 80%;
  }
  &__submit {
    width: 80%;
    color: #ffffff;
    font-size: 1.6rem;
    outline: 0;
    border: 0;
    background: #888787;
    padding: 10px 20px 10px;
    cursor: pointer;
    &:hover {
      background: darken(#888787, 20%);
    }
  }
}
/**
  * 404 page
  */
  .page-404{
    text-align: center;
    padding: 50px 0;

    &__title{
      font-size: 200px;
      margin: 0;
      line-height: 1;
    }
    &__subtitle{
      margin: 0;
      line-height: 1;
      font-size: 28px;
    }
  }

/**
  * js styles
  */
.toggle-content {
  display: none;
}
.js-active {
  display: block;
}

/**
  * animation
  */

@keyframes spin {
  0% {transform: rotate(0);}
  100% {transform: rotate(360deg);}
  0% {transform: rotate(0);}
}
