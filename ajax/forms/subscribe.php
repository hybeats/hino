<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$SUB_EXISTS = $_POST["sub_exists"];
$SUB_STATUS = $_POST["sub_status"];
$ACTION = $_POST["action"];
CModule::IncludeModule("subscribe");
$rubric_id = 3;
//получаем данные о текущем пользователе
$user_id = CUser::GetID();
$user_email = CUser::GetEmail();
$subscrCheck = CSubscription::GetList( array(), array("USER_ID" => $user_id), false );
$subscr_arr = $subscrCheck->Fetch();
$subscr = new CSubscription;

switch ($ACTION) {

  case 'subscribe':

    if($SUB_EXISTS == "N") {
      //добавляем подписчика
      $arFields = Array(
        "USER_ID" => ($USER->IsAuthorized()? $user_id: false),
        "FORMAT" => "html",
        "EMAIL" => $user_email,
        "ACTIVE" => "Y",
        "RUB_ID" => array($rubric_id),
        "CONFIRMED" => "Y"
      );
      $newID = $subscr->Add($arFields);
      if($newID > 0) {
        $result["MESSAGE"] = "Вы успешно подписались на рассылку";
      } else {
        $result["MESSAGE"] = "Подписка на рассылку не удалась";
      }
    } else {
      //проверяем подписан ли пользователь на рассылку
      $arRub = CSubscription::GetRubricArray($subscr_arr["ID"]);
      if ($SUB_STATUS == "Y") {
        $result["MESSAGE"] = "Вы уже подписаны на рассылку";
      } else {
        array_push($arRub, $rubric_id);
        $subscr->Update($subscr_arr["ID"], array("RUB_ID" => $arRub), "s1");
        $result["MESSAGE"] = "Вы успешно подписались на рассылку";
      }
    }

  break;

  case 'unsubscribe':
    $arRub = CSubscription::GetRubricArray($subscr_arr["ID"]);
    unset($arRub[array_search($rubric_id,$arRub)]);
    $subscr->Update($subscr_arr["ID"], array("RUB_ID" => $arRub), "s1");
    $result["MESSAGE"] = "Вы успешно отписались от рассылки";
  break;

  default:
    return false;
  break;
}


echo json_encode($result);
?>
