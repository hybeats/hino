<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("subscribe");
$rubric_id = 3;
$result = array();
//получаем данные о текущем пользователе
$user_id = CUser::GetID();

//проверяем наличие подписчика
$subscrCheck = CSubscription::GetList( array(), array("USER_ID" => $user_id), false );
$subscr_arr = $subscrCheck->Fetch();

$subscr = new CSubscription;

if(empty($subscr_arr)) {
  $result["SUBSCRIBE_STATUS"] = "N";
  $result["SUBSCRIBER_EXISTS"] = "N";
} else {
  $result["SUBSCRIBER_EXISTS"] = "Y";
  $arRub = CSubscription::GetRubricArray($subscr_arr["ID"]);
  if (in_array($rubric_id, $arRub)) {
    $result["MESSAGE"] = "Вы уже подписаны на рассылку";
    $result["SUBSCRIBE_STATUS"] = "Y";
  } else {
    $result["SUBSCRIBE_STATUS"] = "N";
  }
}

echo json_encode($result);
?>
