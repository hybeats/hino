<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if ($_REQUEST["action"] == "download"){
  if(!empty($_REQUEST["name"])){
    $file = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$_REQUEST["name"].".zip";

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="documents.zip"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: '.filesize($file));

    readfile($file);
    unlink($file);
  }
  exit;
}

if (check_bitrix_sessid()){
  if ($_REQUEST["action"] == "check"){
    $hs = new HinoSoap('PrintedDocuments');

    $request = array("Запрос" => trim($_REQUEST["name1c"]));
    $result  = $hs->client->GetPrintedDocuments($request);

    if($result->return->message == "Успешно"){
      $zip_name  = "hino_".mktime();
      $full_name = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$zip_name.".zip";

      file_put_contents($full_name, $result->return->binaryData);
      $arResult["result"]["name"] = $zip_name;
    }else{
      $arResult["mess"][] = array(
        "text" => $result->return->message,
        "type" => "error"
      );
    }
  }
}else{
  $arResult["mess"][] = array(
    "text" => Loc::getMessage("SESSION_ERROR"),
    "type" => "error"
  );
}

echo json_encode($arResult);
die();
