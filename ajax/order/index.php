<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Localization\Loc;

header("Content-Type: application/x-javascript; charset=".LANG_CHARSET);
Loc::loadMessages(__FILE__);

if (check_bitrix_sessid()){

  Bitrix\Main\Loader::includeModule('sale');
  Bitrix\Main\Loader::includeModule('catalog');

  if ($_POST["action"] == "getdelivery" || $_POST["action"] == "getbasket"){
    $deliveryListDB = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();
    foreach ($deliveryListDB as $delivery){
      //исключение службы "Без доставки"
      if($delivery["ID"] == 1) continue;

      $arResult["result"]["delivery"][] = array(
        "id"   => $delivery["ID"],
        "name" => $delivery["NAME"]
      );
    }
  }

  if ($_POST["action"] == "getbasket"){
    if (CModule::includeModule("sale") && CModule::includeModule("catalog")){
      global $USER;

      $rsStore = CCatalogStore::GetList(array(), array(), false, false, array("ID", "TITLE", "UF_CODE"));
      while($arOb = $rsStore->Fetch()){
        $arStore[$arOb["ID"]] = array(
          "UF_CODE" => $arOb["UF_CODE"],
          "TITLE"   => $arOb["TITLE"]
        );
      }

      //приоритет складов
      $listStore = HinoUsers::getDealerListStore($USER->GetID());
      foreach ($listStore as $id) {
        $arResult["result"]["stores"][] = array(
          "id"      => $id,
          "uf_code" => $arStore[$id]["UF_CODE"],
          "title"   => $arStore[$id]["TITLE"]
        );
      }

      $fuser_id = \Bitrix\Sale\Fuser::getIdByUserId($USER->GetID());

      //получение id товаров в текущей корзине
      $dbBasketItems = CSaleBasket::GetList(array(),array("FUSER_ID" => $fuser_id, "LID" => SITE_ID, "ORDER_ID" => false), false, false, array("ID"));
      while ($arItems = $dbBasketItems->Fetch()){
        $arBasketID[] = $arItems["ID"];
      }

      if(!empty($arBasketID)){
        //получение артикулов товаров в текущей корзине
        $db_res = CSaleBasket::GetPropsList(array(), array("BASKET_ID" => $arBasketID, "CODE" => "ARTICLE"));
        while ($ar_res = $db_res->Fetch()){
          if($ar_res["VALUE"]){
            $arArticle[] = $ar_res["VALUE"];
          }
        }

        $hp = new HinoProduct();
        $arReplacementInfo = $hp->getPartsInfo($arArticle, "id");

        $basketUser = HinoReservationPart::getBasket($fuser_id);

        //определение елементов доступных для редактирования
        foreach ($basketUser as $part) {
          if(!in_array($part["STORE_ID"], $partsByStores[$part["PRODUCT_ID"]])){
            $partsByStores[$part["PRODUCT_ID"]][] = $part["STORE_ID"];
          }
          //вычисляем доступное кол-во для каждого склада
          $partsAvailable[$part["PRODUCT_ID"]][$part["STORE_ID"]] += $part["QUANTITY"];
        }
        foreach ($listStore as $id) {
          foreach ($partsByStores as &$arStoreItem) {
            if(count($arStoreItem) > 1){
              unset($arStoreItem[array_search($id, $arStoreItem)]);
            }
          }
        }

        foreach ($basketUser as $part) {
          if($part["STORE_ID"] == 0){
            $presence  = 0;
            $available = 0;
          }else{
            $presence  = $arReplacementInfo[$part["PRODUCT_ID"]]["fields"][$arStore[$part["STORE_ID"]]["UF_CODE"]]; //количество на складе заказа
            $available = $presence - $partsAvailable[$part["PRODUCT_ID"]][$part["STORE_ID"]]; //доступно для заказа
          }

          $ordersResult[$part["STORE_ID"]][$part["DELIVERY_ID"]]["parts"][] = array(
            "id"             => $part["PRODUCT_ID"],
            "reservation_id" => $part["ID"],
            "storeId"        => $part["STORE_ID"],
            "deliveryId"     => $part["DELIVERY_ID"],
            "article"        => $arReplacementInfo[$part["PRODUCT_ID"]]["fields"]["number"],
            "name"           => $arReplacementInfo[$part["PRODUCT_ID"]]["fields"]["format_name"],
            "count"          => $part["QUANTITY"],
            "presence"       => $presence,
            "available"      => $available,
            "grade"          => $arReplacementInfo[$part["PRODUCT_ID"]]["fields"]["grade"],
            "code"           => $arReplacementInfo[$part["PRODUCT_ID"]]["fields"]["code"],
            "weight"         => $arReplacementInfo[$part["PRODUCT_ID"]]["fields"]["weight"],
            "price"          => $arReplacementInfo[$part["PRODUCT_ID"]]["fields"]["price"],
            "edit"           => in_array($part["STORE_ID"], $partsByStores[$part["PRODUCT_ID"]])
          );
        }

        //расчитываем транспортную наценку для каждого заказа
        foreach ($ordersResult as $storeID => &$arOrders) {
          foreach ($arOrders as $deliveryID => &$orders) {
            $orders['comment'] = '';

            if($deliveryID <= 0) continue;

            $basket = \Bitrix\Sale\Basket::create(SITE_ID);
            $basket->setFUserId($fuser_id);

            foreach ($orders["parts"] as $partOrder) {
              $item = $basket->createItem('catalog', $partOrder["id"]);

              $item->setFields(array(
                'QUANTITY'               => $partOrder["count"],
                'CURRENCY'               => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                'LID'                    => 's1',
                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider'
              ));
            }

            $orderTmp = Bitrix\Sale\Order::create(SITE_ID, $USER->GetID());
            $orderTmp->setBasket($basket);

            //добавляем доставку
            $shipmentCollection = $orderTmp->getShipmentCollection();
            $shipment = $shipmentCollection->createItem();
            $shipmentItemCollection = $shipment->getShipmentItemCollection();

            $shipment->setStoreId($storeID);

            $deliveryObj = Bitrix\Sale\Delivery\Services\Manager::getObjectById($deliveryID);
            $shipment->setFields(array(
              'DELIVERY_ID'   => $deliveryObj->getId(),
              'DELIVERY_NAME' => $deliveryObj->getName(),
              'CURRENCY'      => $deliveryObj->getCurrency()
            ));

            foreach ($basket as $basketItem){
              $item = $shipmentItemCollection->createItem($basketItem);
              $item->setQuantity($basketItem->getQuantity());
            }

            $orders["delivery_price"] = $orderTmp->getDeliveryPrice();
          }
        }

        $arResult["result"]["orders"] = $ordersResult;
      }else{
        //пустая корзина
        $arResult["result"]["orders"] = null;
      }
    }else{
      AddMessage2Log(Loc::getMessage("MODULE_NOT_FOUND"));
      $arResult["mess"][] = array(
        "text" => Loc::getMessage("MODULE_NOT_FOUND"),
        "type" => "error"
      );
    }
  }

  if ($_POST["action"] == "createorder"){
    if(HinoUsers::getDealerStatus() == "active" && HinoUsers::isDealer()){
      $res = HinoOrders::createOrders($_POST["comments"]);
      if(!empty($res["error"])){
        foreach ($res["error"] as $text) {
          $arResult["mess"][] = array(
            "text" => $text,
            "type" => "error"
          );
        }
      }else{
        foreach ($res["result"] as $orderID) {
          $arResult["mess"][] = array(
            "text" => Loc::getMessage("ORDER_SUCCESS", Array ("#ORDER_ID#" => $orderID)),
            "type" => "success"
          );
        }
      }
    }else{
      $arResult["mess"][] = array(
        "text" => Loc::getMessage("PERMISSION_ERROR"),
        "type" => "error"
      );
    }
  }

  if ($_POST["action"] == "clearbasket"){
    if (!HinoUsers::isDealer()) {
      $arResult["mess"][] = array(
        "text" => GetMessage('DEALER_LINK_ERROR'),
        "type" => "error"
      );
    } else {
      global $USER;
      $fuser_id = \Bitrix\Sale\Fuser::getIdByUserId($USER->GetID());

      if (CSaleBasket::DeleteAll($fuser_id)){
        $arResult["mess"][] = array(
          "text" => GetMessage("BASKET_DELETE_SUCCESS"),
          "type" => "success"
        );
      }else{
        $arResult["mess"][] = array(
          "text" => GetMessage("BASKET_DELETE_ERROR"),
          "type" => "error"
        );
      }
    }
  }

  if ($_POST["action"] == "updateorder"){
    if (HinoUsers::isAdmin() || HinoUsers::isSupportGroup()) {
      $orderId = intval($_REQUEST["order_id"]);
      if($orderId > 0){

        $order = \Bitrix\Sale\Order::load($orderId);
        $shipmentCollection = \Bitrix\Sale\ShipmentCollection::load($order);

        foreach ($shipmentCollection as $shipment){
          if($shipment->isSystem()) continue;

          $shipment->setField("DELIVERY_DOC_NUM", trim($_REQUEST["ttn"]));
          $shipment->setField("COMPANY_ID", intval($_REQUEST["company"]));
          $shipment->save();
        }

        $order->save();

        $arResult["mess"][] = array(
          "text" => GetMessage("ORDER_EDIT_SUCCESS"),
          "type" => "success"
        );
      }else{
        $arResult["mess"][] = array(
          "text" => GetMessage("ORDER_EDIT_ERROR"),
          "type" => "error"
        );
      }
    } else {
      $arResult["mess"][] = array(
        "text" => Loc::getMessage("PERMISSION_ERROR"),
        "type" => "error"
      );
    }
  }

  if ($_POST["action"] == "getorderitems"){
    if ($order = Bitrix\Sale\Order::load(intval($_POST["id"]))) {
      $basket = $order->getBasket();

      $arResult["result"] = array("order" => array(
        "name"       => $order->getField("ID_1C")? $order->getField("ID_1C"): $order->getId(),
        "comments"   => $order->getField("COMMENTS"),
        "weight"     => $basket->getWeight(),
        "cost"       => $basket->getPrice(),
        "resultcost" => $order->getPrice()
      ));

      foreach ($basket as $basketItem) {
        $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_PARTS, "ID" => $basketItem->getProductId()), false, false, array("ID", "IBLOCK_ID"));

        if($ob = $res->GetNextElement()){
          $arProps  = $ob->GetProperties();
          foreach ($arProps["CML2_TRAITS"]["DESCRIPTION"] as $key => $value) {
            $arProps["CML2_TRAITS_FORMAT"][$value] = $arProps["CML2_TRAITS"]["VALUE"][$key];
          }
          $code = strval(intval($arProps["CML2_TRAITS_FORMAT"]["ProductCode"]));

          $arResult["result"]["order_item_list"][] = array(
            "article"   => $arProps["CML2_ARTICLE"]["VALUE"],
            "name"      => $basketItem->getField('NAME'),
            "count"     => $basketItem->getQuantity(),
            "grade"     => $arProps["CML2_TRAITS_FORMAT"]["TMG"],
            "code"      => $code[0]? $code[0]: "-",
            "weight"    => $basketItem->getWeight(),
            "price"     => $basketItem->getPrice(),
            "fullprice" => $basketItem->getFinalPrice(),
          );
        }
      }
    } else {
      $arResult["mess"][] = array(
        "text" => Loc::getMessage("ORDER_EMPTY"),
        "type" => "error"
      );
    }
  }

  $arResult["sessid"] = bitrix_sessid();
}else{
  $arResult["mess"][] = array(
    "text" => Loc::getMessage("SESSION_ERROR"),
    "type" => "error"
  );
}

echo json_encode($arResult);
die();
