<?
$MESS["MODULE_NOT_FOUND"] = "Ошибка подключения модулей";
$MESS["ORDER_SUCCESS"] = "Успешно создан заказ №#ORDER_ID#";
$MESS["SESSION_ERROR"] = "Ошибка сессии";
$MESS["PERMISSION_ERROR"] = "Ошибка доступа. Обратитесь к менеджеру.";

$MESS["BASKET_DELETE_SUCCESS"] = "Корзина успешно очишена.";
$MESS["BASKET_DELETE_ERROR"]   = "Ошибка очистки корзины";

$MESS["ORDER_EDIT_SUCCESS"] = "Заказа успешно изменен.";
$MESS["ORDER_EDIT_ERROR"]   = "Ошибка редактирования заказа.";
$MESS["ORDER_EMPTY"]        = "Заказ не найден.";

$MESS["DEALER_LINK_ERROR"] = "Пользователь не привязан к дилеру";
