<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header("Content-Type: application/x-javascript; charset=".LANG_CHARSET);

Bitrix\Main\Loader::includeModule("iblock");
Bitrix\Main\Loader::includeModule("currency");


use Bitrix\Main\Mail\Event,
    Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (check_bitrix_sessid() && HinoUsers::isAdmin()){
  $request = Application::getInstance()->getContext()->getRequest();

  if ($request->getPost("action") == "blockdealer"){
    $newValue = $request->getPost("locked") == 1? 10: 9; //10 заблокирован, 9 - активен

    CIBlockElement::SetPropertyValuesEx(
      $request->getPost("id_dealer"),
      false,
      array(
        "STATUS" => array("VALUE" => $newValue)
      )
    );
    $arResult["result"] = array(
      "locked" => $newValue == 9? 0: 1
    );
    $arResult["mess"][] = array(
      "text" => $newValue == 9? Loc::getMessage('SUCCESS_UNLOCK'): Loc::getMessage('SUCCESS_LOCK'),
      "type" => "success"
    );
  }
  if ($request->getPost("action") == "updatedealer"){
    $arUpdateProps = array();

    $manager = (int)$request->getPost("manager");
    if($manager > 0){
      $arUpdateProps["MANAGER"] = array("VALUE" => $manager);
    }

    $storeList = $request->getPost("store");
    if(!empty($storeList)){
      $arUpdateProps["STOCK"] = array("VALUE" => $storeList);
    }

    $id_dealer = (int)$request->getPost("id_dealer");
    if($id_dealer > 0 && !empty($arUpdateProps)){
      CIBlockElement::SetPropertyValuesEx($id_dealer, false, $arUpdateProps);

      //очишаем кеш
      HinoUsers::cleanCacheStoreDealer($id_dealer);

      $arResult["mess"][] = array(
        "text" => Loc::getMessage('SUCCESS_UPDATE'),
        "type" => "success"
      );
    }else{
      $arResult["mess"][] = array(
        "text" => Loc::getMessage('ERROR_UPDATE'),
        "type" => "error"
      );
    }

  }
  if ($request->getPost("action") == "updateordercoefficientjpy"){
    $coefficient = (double)$request->getPost("coefficient");
    if($coefficient > 0){

      $res = CCurrencyRates::Update(1, array(
        'DATE_RATE' => date("d.m.Y"),
        'CURRENCY'  => "JPY",
        'RATE'      => $coefficient
      ));

      if($res){
        $arResult["mess"][] = array(
          "text" => Loc::getMessage('SUCCESS_UPDATE_JPY'),
          "type" => "success"
        );
      }else{
        $arResult["mess"][] = array(
          "text" => Loc::getMessage('ERROR_UPDATE_JPY'),
          "type" => "error"
        );
      }
    }else{
      $arResult["mess"][] = array(
        "text" => Loc::getMessage('ERROR_UPDATE_JPY'),
        "type" => "error"
      );
    }

  }
  if ($request->getPost("action") == "updatedealeruser"){
    $userID = intval($request->getPost("userID"));
    if ($userID > 0) {
      $user = new CUser;
      $aFields = array(
        'UF_PROXY_NUMBER'     => trim($request->getPost("proxyNumber")),
        'UF_PROXY_DATE_START' => trim($request->getPost("proxyDateStart")),
        'UF_PROXY_DATE_END'   => trim($request->getPost("proxyDateEnd")),
        'UF_DELIVERY_ADDRESS' => trim($request->getPost("address")),
      );
      $user->Update($userID, $aFields);

      if(empty($user->LAST_ERROR)){
        $arResult["mess"][] = array(
          "text" => Loc::getMessage('SUCCESS_UPDATE_EMPLOYEE'),
          "type" => "success"
        );
      }else{
        $arResult["mess"][] = array(
          "text" => $user->LAST_ERROR,
          "type" => "error"
        );
      }
    }
  }
}else{
  $arResult["mess"][] = array(
    "text" => Loc::getMessage('PERMISSION_ERROR'),
    "type" => "error"
  );
}

echo json_encode($arResult);
die();
