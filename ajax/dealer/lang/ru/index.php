<?
$MESS["PERMISSION_ERROR"] = "Ошибка доступа.";
$MESS["UPDATE_ERROR"] = "Ошибка изменения.";

$MESS["SUCCESS_LOCK"]   = "Дилер заблокирован.";
$MESS["SUCCESS_UNLOCK"] = "Дилер разблокирован.";

$MESS["SUCCESS_UPDATE"] = "Дилер успешно отредактирован.";
$MESS["ERROR_UPDATE"] = "Ошибка редактирования дилера.";

$MESS["ERROR_UPDATE_JPY"] = "Ошибка обновления курса.";
$MESS["SUCCESS_UPDATE_JPY"] = "Курс успешно изменен.";

$MESS["SUCCESS_UPDATE_EMPLOYEE"] = "Пользователь успешно отредактирован.";
