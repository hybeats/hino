<?
$MESS["CATALOG_ERROR2BASKET"]            = "Ошибка добавления в корзину";
$MESS["CATALOG_ELEMENT_NOT_FOUND"]       = "ЗЧ не найдена";
$MESS["CATALOG_ELEMENT_FULL_PRICE_LIST"] = "ЗЧ есть в полном прайс-листе. Перейдите в раздел <a href=\"/parts/?article=#ARTICLE#\" target=\"_blank\">\"Поиск ЗЧ\"</a>";

$MESS["BASKET_ELEMENT_NOT_FOUND"]  = "Запасная часть с данным артикулом не найдена в корзине";
$MESS["UNKNOWN_ERROR"]             = "Неизвестная ошибка";
$MESS["SUCCESS_ADD"]               = "В корзине: #ARTICLE# - #QUANTITY# шт.";
$MESS["INSUFFICIENT_AMOUNT"]       = "Есть замена. Перейдите в раздел <a href=\"/parts/?article=#ARTICLE#\" target=\"_blank\">\"Поиск ЗЧ\"</a>";

$MESS["SUCCESS_UPDATE"] = "Корзина успешно изменена.";
$MESS["SUCCESS_DELETE"] = "#ARTICLE# удален из заказа.";
$MESS["ERROR_DELETE"]   = "#ARTICLE# ошибка удаления.";
$MESS["SESSION_ERROR"]  = "Ошибка сессии";

$MESS["FILE_TYPE_ERROR"] = "Неверный формат файла. Загрузите файл в формате .txt";
$MESS["FILE_SIZE_ERROR"] = "Слишком большой файл. Более 700 байт.";
$MESS["FILE_ERROR"]      = "Ошибка загрузки файла. Попробуйте еще раз.";


$MESS["CATALOG_ELEMENT_NOT_FOUND_ARTICLE"]    = "#ARTICLE# - ЗЧ не найдена.";
$MESS["CATALOG_ELEMENT_FULL_PRICE_LIST_FILE"] = "#ARTICLE# - ЗЧ есть в полном прайс-листе.";
$MESS["SUCCESS_ADD_FILE"]                     = "в корзине - #QUANTITY# шт.";
$MESS["INSUFFICIENT_AMOUNT_FILE"]             = "есть замена.";
$MESS["SUCCESS_FILE"]                         = "#ARTICLE# - успешно загружен.";
$MESS["EMPTY_QUANTITY"]                       = "#ARTICLE# - количество равно 0.";
$MESS["PARTS_URL"]                            = "Перейдите в раздел <a href=\"/parts/?article=#ARTICLE#\" target=\"_blank\">\"Поиск ЗЧ\"</a>";

$MESS["DEALER_LINK_ERROR"] = "Пользователь не привязан к дилеру";
