<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header("Content-Type: application/x-javascript; charset=".LANG_CHARSET);
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

if (check_bitrix_sessid()){
  Bitrix\Main\Loader::includeModule("currency");
  Bitrix\Main\Loader::includeModule("sale");

  if ($_POST["action"] == "search"){

    $hp = new HinoProduct();
    $article = trim($_POST["vendor_code"]);
    $arResult["article_search"] = $article;

    //наличие в базе
    $xmlIdProduct = $hp->getXmlIdByArticle($article);

    if($xmlIdProduct){
      //получаем массив замен из 1С WebService
      $arReplacementSoap = $hp->getReplacementSoap(array($article));

      if(!empty($arReplacementSoap[$article])){
        //удаляем несуществующие запчасти
        //находим элементы последние в цепочке замен
        foreach ($arReplacementSoap[$article]["replace"] as &$arReplacementList) {
          $arLastElement[] = $arReplacementList[count($arReplacementList) - 1]["article"];

          foreach ($arReplacementList as $key => &$replacement) {
            if($replacement["xml_id"] == "false"){
              unset($arReplacementList[$key]);
              unset($arReplacementSoap[$article]["article"][ array_search($replacement["article"], $arReplacementSoap[$article]["article"]) ]);
            }
          }
        }

        $arReplacementInfo = $hp->getPartsInfo($arReplacementSoap[$article]["article"], "article", $arLastElement);

        //удаляем запчасти с количеством равным 0
        foreach ($arReplacementSoap[$article]["replace"] as &$arReplacementList) {
          foreach ($arReplacementList as $key => &$replacement) {
            if($arReplacementInfo[$replacement["article"]]["fields"]["presence"] <= 0 && !in_array($replacement["article"], $arLastElement)){
              unset($arReplacementList[$key]);
              unset($arReplacementInfo[$replacement["article"]]);
            }
          }
        }

        //удаляем одинаковые цепочеки
        $countReplacementList = count($arReplacementSoap[$article]["replace"]);

        for ($i = 0; $i < $countReplacementList; $i++) {
          $compare = false; //равен ли остальным елементам

          for ($j = $i + 1; $j < $countReplacementList; $j++) {
            $compare = HinoHelper::arrayRecursiveCompare(
              array_values($arReplacementSoap[$article]["replace"][$i]),
              array_values($arReplacementSoap[$article]["replace"][$j])
            );
            if($compare) break;
          }

          if(!$compare && !empty($arReplacementSoap[$article]["replace"][$i])){
            $arReplacementSoapFilter[] = $arReplacementSoap[$article]["replace"][$i];
          }
        }

        $arResult["result"] = !empty($arReplacementSoapFilter)? $arReplacementSoapFilter: array();
      }else{
        $arReplacementInfo = $hp->getPartsInfo(array($article), "article", array($article));
        $arResult["result"] = array( array( array("article" => $article)) );
      }

      $arResult["fields_display"] = array(
        "number", "format_name", "price", "price_mrc", "presence", "moscow", "artem", "belgium", "japan", "type", "grade", "code", "weight" //"size",
      );
      $arResult["resultPL"] = false;
      $arResult["replacement_info"] = $arReplacementInfo;
    }else{
      CModule::IncludeModule("hino.pricelist");
      $priceList = new Hino\PriceList();

      if($res = $priceList->searchFirsPart($article)){

        $size = implode("x", array(
          $res["UF_LENGTHCM"],
          $res["UF_WIDTHCM"],
          $res["UF_HEIGHTCM"]
        ));
        $arResult["fields_display"] = array(
          "number", "format_name", "price", "size", "weight"
        );

        $resRates = CCurrencyRates::GetByID(1);
        $coefficient = $resRates["RATE"];

        $arResult["replacement_info"][$article] = array(
          "id" => false,
          "fields" => array(
            "number"      => $res["UF_NEWESTPRT"],
            "format_name" => $res["UF_DESCR"],
            "price"       => ($res["UF_SUNITPR"]*1.64*1.18)/$coefficient,
            "size"        => $size,
            "weight"      => $res["UF_WEIGHT"]
          )
        );
        $arResult["result"] = array( array( array("article" => $article)) );
        $arResult["resultPL"] = true;
      }else{
        $arResult["resultPL"] = false;
        $arResult["result"] = array();
        $arResult["replacement_info"] = array();
      }
    }
    $dealer = HinoUsers::getDealer();
    $arResult['dealer'] = $dealer['ID']? true: false;
  }

  if ($_POST["action"] == "addbasket"){
    if (!HinoUsers::isDealer()) {
      $arResult["mess"][] = array(
        "text" => GetMessage('DEALER_LINK_ERROR'),
        "type" => "error"
      );
    } else {
      $hp = new HinoProduct();

      $article    = trim($_REQUEST["article"]);
      $idProduct = (int)$_REQUEST["part_id"];

      if($idProduct == 0){
        $infoPart = $hp->getElementInfoByArticle($article);
        $idProduct = $infoPart[$article]["FIELDS"]["ID"];
      }
      $deliveryID = (int)$_REQUEST["delivery"];
      $quantity   = (int)$_REQUEST["quantity"] > 0? (int)$_REQUEST["quantity"]: 1;

      if (!empty($idProduct)){
        global $USER;

        if(isset($_REQUEST["preorder"])){
          $preorder = (int)$_REQUEST["preorder"];
        }else{
          //получаем массив замен из 1С WebService
          $arReplacementSoap = $hp->getReplacementSoap(array($article));

          if($arReplacementSoap[$article]){
            //находим элементы последние в цепочке замен
            foreach ($arReplacementSoap[$article]["replace"] as &$arReplacementList) {
              $arLastElement[] = $arReplacementList[count($arReplacementList) - 1]["article"];
            }
            $preorder = intval(in_array($article, $arLastElement));
          }else{
            $preorder = 1;
          }
        }

        $fuser_id = \Bitrix\Sale\Fuser::getIdByUserId($USER->GetID());

        //получение текущей корзины
        $dbBasketItems = CSaleBasket::GetList(array(),array("FUSER_ID" => $fuser_id, "LID" => SITE_ID, "ORDER_ID" => false), false, false, array("ID", "PRODUCT_ID", "QUANTITY"));
        while ($arItems = $dbBasketItems->Fetch()){
          $arBasket[$arItems["PRODUCT_ID"]] = $arItems;
        }

        if(array_key_exists($idProduct, $arBasket)){
          $type = trim($_REQUEST["type_add"])? trim($_REQUEST["type_add"]): "update";

          $res = $hp->updateBasket($idProduct, $quantity, $fuser_id, $type, $deliveryID, $preorder);
        }else{
          $res = $hp->addBasket($idProduct, $quantity, $article, $fuser_id, $deliveryID, $preorder);
        }

        //обновление состояния корзины
        \Bitrix\Sale\BasketComponentHelper::updateFUserBasketQuantity($fuser_id);

        if(isset($res["result"])){
          $arResult["result"] = array(
          "id"       => $idProduct,
          "article"  => $article,
          "quantity" => $res["result"]
          );

          if($res["result"] < $quantity){
            if($res["result"] > 0){
              $popupText = GetMessage('SUCCESS_ADD', array("#ARTICLE#" => $article, "#QUANTITY#" => $res["result"]));
            }
            if($hp->checkNotEmptyReplace($arReplacementSoap[$article]['article'])){
              $popupText .= '<br/>'.GetMessage('INSUFFICIENT_AMOUNT', array("#ARTICLE#" => $article));
            }

            if($popupText){
              $arResult["popup"] = array("text" => $popupText);
            }else{
              $arResult["mess"][] = array(
              "text" => GetMessage('CATALOG_ELEMENT_NOT_FOUND'),
              "type" => "error"
              );
            }
          }else{
            $arResult["mess"][] = array(
            "text" => GetMessage('SUCCESS_ADD', array("#ARTICLE#" => $article, "#QUANTITY#" => $res["result"])),
            "type" => "success"
            );
          }
        }else{
          $errorMes = GetMessage($res["error"]);
          if($errorMes){
            $arResult["mess"][] = array(
            "text" => $errorMes,
            "type" => "error"
            );
          }else{
            $arResult["mess"][] = array(
            "text" => GetMessage('UNKNOWN_ERROR'),
            "type" => "error"
            );
          }
        }
      }else{
        CModule::IncludeModule("hino.pricelist");
        $priceList = new Hino\PriceList();

        if($res = $priceList->searchFirsPart($article)){
          $arResult["popup"] = array(
          "text" => GetMessage('CATALOG_ELEMENT_FULL_PRICE_LIST', array('#ARTICLE#' => $article)),
          );
        }else{
          $arResult["mess"][] = array(
          "text" => GetMessage('CATALOG_ELEMENT_NOT_FOUND'),
          "type" => "error"
          );
        }
      }
    }
  }

  if ($_POST["action"] == "removebasket"){
    if (!HinoUsers::isDealer()) {
      $arResult["mess"][] = array(
        "text" => GetMessage('DEALER_LINK_ERROR'),
        "type" => "error"
      );
    } else {
      $idProduct  = (int)$_REQUEST["part_id"];
      $deliveryID = (int)$_REQUEST["deliveryId"];
      $storeID    = (int)$_REQUEST["storeId"];
      $article    = trim($_REQUEST["article"]);

      if($idProduct > 0 && $deliveryID > 0){
        global $USER;
        $fuser_id = \Bitrix\Sale\Fuser::getIdByUserId($USER->GetID());

        $rp = new HinoReservationPart($idProduct, $deliveryID);

        $oldQuantityDelivery = $rp->getCountReservation();
        $rp->removePartStore($storeID);
        $newQuantityDelivery = $rp->getCountReservation();

        $diffQuantityDelivery = $oldQuantityDelivery - $newQuantityDelivery; //сколько удалил

        //получение текущего кол-ва
        $dbBasketItems = CSaleBasket::GetList(
          array(),
          array("FUSER_ID" => $fuser_id, "LID" => SITE_ID, "PRODUCT_ID" => $idProduct, "ORDER_ID" => false),
          false, false, array("ID", "PRODUCT_ID", "QUANTITY")
        );
        $arProduct = $dbBasketItems->Fetch();
        $newQuantity = $arProduct["QUANTITY"] - $diffQuantityDelivery;

        CSaleBasket::Update($arProduct["ID"], array("QUANTITY" => $newQuantity));

        //обновление состояния корзины
        \Bitrix\Sale\BasketComponentHelper::updateFUserBasketQuantity($fuser_id);

        $arResult["result"] = array(
          "id"       => $idProduct,
          "quantity" => $res["result"]
        );
        $arResult["mess"][] = array(
          "text" => GetMessage('SUCCESS_DELETE', array("#ARTICLE#" => $article)),
          "type" => "success"
        );
      }else{
        $arResult["mess"][] = array(
          "text" => GetMessage('ERROR_DELETE', array("#ARTICLE#" => $article)),
          "type" => "error"
        );
      }
    }
  }

  if ($_POST["action"] == "updatepart"){
    if (!HinoUsers::isDealer()) {
      $arResult["mess"][] = array(
        "text" => GetMessage('DEALER_LINK_ERROR'),
        "type" => "error"
      );
    } else {
      global $USER;
      $hp = new HinoProduct();

      $article       = trim($_REQUEST["article"]);
      $idProduct     = (int)$_REQUEST["part_id"];
      $idReservation = (int)$_REQUEST["reservation_id"];
      $deliveryID    = (int)$_REQUEST["delivery"];
      $quantity      = (int)$_REQUEST["quantity"];
      $fuser_id = \Bitrix\Sale\Fuser::getIdByUserId($USER->GetID());

      $basket = \Bitrix\Sale\Basket::loadItemsForFUser($fuser_id, Bitrix\Main\Context::getCurrent()->getSite());

      foreach ($basket as $basketItem) {
        if($basketItem->getProductId() == $idProduct){
          $itemCurrent = $basketItem;
          break;
        }
      }

      if (isset($itemCurrent) ) {
        $removeQuantity = HinoReservationPart::deletePartByID($idReservation);

        //удаляем старое кол-во из корзины и резерва
        $itemCurrent->setField('QUANTITY', $itemCurrent->getQuantity() - $removeQuantity);
        $basket->save();

        $rp = new HinoReservationPart($idProduct, $deliveryID);
        $allQuantity = $rp->getCountReservation();

        if($itemCurrent->getQuantity() == 0){
          $res = $hp->addBasket($idProduct, $quantity, $article, $fuser_id, $deliveryID, 1);
        }elseif($allQuantity == 0){
          $res = $hp->updateBasket($idProduct, $quantity, $fuser_id, "add", $deliveryID, 1);
        }else{
          $newQuantity = $allQuantity + $quantity;
          $res = $hp->updateBasket($idProduct, $newQuantity, $fuser_id, "update", $deliveryID, 1);
        }

        if(isset($res["result"])){
          $arResult["result"] = array(
            "id"       => $idProduct,
            "article"  => $article
          );
          $arResult["mess"][] = array(
            "text" => GetMessage('SUCCESS_UPDATE'),
            "type" => "success"
          );
        }else{
          $errorMes = GetMessage($res["error"]);
          if($errorMes){
            $arResult["mess"][] = array(
            "text" => $errorMes,
            "type" => "error"
            );
          }else{
            $arResult["mess"][] = array(
            "text" => GetMessage('UNKNOWN_ERROR'),
            "type" => "error"
            );
          }
        }
      } else {
        $arResult["mess"][] = array(
        "text" => GetMessage('BASKET_ELEMENT_NOT_FOUND'),
        "type" => "error"
        );
      }
    }
  }

  if ($_POST["action"] == "loadfile"){
    if($_FILES["file"]["size"] > 700){
      $arResult["mess"][] = array(
        "text" => GetMessage('FILE_SIZE_ERROR'),
        "type" => "error"
      );
    }
    if($_FILES["file"]["type"] != 'text/plain'){
      $arResult["mess"][] = array(
        "text" => GetMessage('FILE_TYPE_ERROR'),
        "type" => "error"
      );
    }
    if($_FILES["file"]["error"] > 0){
      AddMessage2Log('Ошибка загрузки файла. NAME - '.$_FILES["file"]["name"].'; Код - '.$_FILES["file"]["error"]);
      $arResult["mess"][] = array(
        "text" => GetMessage('FILE_ERROR'),
        "type" => "error"
      );
    }
    if(!HinoUsers::isDealer()){
      $arResult["mess"][] = array(
        "text" => GetMessage('DEALER_LINK_ERROR'),
        "type" => "error"
      );
    }

    if(count($arResult["mess"]) == 0){
      if($lines = file($_FILES["file"]["tmp_name"], FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)){
        global $USER;
        $fuser_id  = \Bitrix\Sale\Fuser::getIdByUserId($USER->GetID());
        $arArticle = array();
        $popUpText = array();

        //формируем массив артикулов и обрабатываем повторы
        foreach ($lines as $lineItem) {
          $arValue = explode(" ", $lineItem);
          if (array_key_exists($arValue[0], $arArticle)) {
            $arArticle[$arValue[0]]["QUANTITY"] += intval($arValue[1]);
          }else{
            $arArticle[$arValue[0]] = array(
              "QUANTITY" => intval($arValue[1])
            );
          }
        }

        $hp = new HinoProduct();
        $infoPart = $hp->getElementInfoByArticle(array_keys($arArticle));

        // добавляем part ID и удаляем неккоректные
        foreach ($arArticle as $article => &$value) {
          if(array_key_exists($article, $infoPart) && !empty($infoPart[$article])) {
            if($value["QUANTITY"] > 0){
              $value["PART_ID"] = $infoPart[$article]["FIELDS"]["ID"];
            }else{
              unset($arArticle[$article]);
              // $popUpText[] = GetMessage("EMPTY_QUANTITY", array( "#ARTICLE#" => $article));
            }
          }else{
            unset($arArticle[$article]);
            CModule::IncludeModule("hino.pricelist");
            $priceList = new Hino\PriceList();

            if($res = $priceList->searchFirsPart($article)){
              $popUpText[] = GetMessage("CATALOG_ELEMENT_FULL_PRICE_LIST_FILE", array( "#ARTICLE#" => $article))." ".GetMessage("PARTS_URL", array( "#ARTICLE#" => $article));
            }else{
              $popUpText[] = GetMessage("CATALOG_ELEMENT_NOT_FOUND_ARTICLE", array( "#ARTICLE#" => $article));
            }
          }
        }

        // получаем массив замен из 1С WebService
        $arReplacementSoap = $hp->getReplacementSoap(array_keys($arArticle));

        //получение текущей корзины
        $dbBasketItems = CSaleBasket::GetList(array(),array("FUSER_ID" => $fuser_id, "LID" => SITE_ID, "ORDER_ID" => false), false, false, array("ID", "PRODUCT_ID", "QUANTITY"));
        while ($arItems = $dbBasketItems->Fetch()){
          $arBasket[$arItems["PRODUCT_ID"]] = $arItems;
        }

        foreach ($arArticle as $article => &$arPart) {
          //находим элементы последние в цепочке замен
          if($arReplacementSoap[$article]){
            foreach ($arReplacementSoap[$article]["replace"] as &$arReplacementList) {
              $arLastElement[] = $arReplacementList[count($arReplacementList) - 1]["article"];
            }
            $arPart["PREORDER"] = intval(in_array($article, $arLastElement));
          }else{
            $arPart["PREORDER"] = 1;
          }

          //добавляем
          if(array_key_exists($arPart["PART_ID"], $arBasket)){
            $res = $hp->updateBasket($arPart["PART_ID"], $arPart["QUANTITY"], $fuser_id, "add", DELIVERY_DEFAULT, $arPart["PREORDER"]);
          }else{
            $res = $hp->addBasket($arPart["PART_ID"], $arPart["QUANTITY"], $article, $fuser_id, DELIVERY_DEFAULT, $arPart["PREORDER"]);
          }

          if($res["result"] < $arPart["QUANTITY"]){
            $tmpText = $article." - ";

            if($res["result"] > 0){
              $tmpText .= GetMessage("SUCCESS_ADD_FILE", array("#QUANTITY#" => $res["result"]));
            }

            $arReplacementSoap= $hp->getReplacementSoap(array($article));
            if($hp->checkNotEmptyReplace($arReplacementSoap[$article]['article'])){
              $tmpText .= GetMessage('INSUFFICIENT_AMOUNT_FILE')." ".GetMessage("PARTS_URL", array( "#ARTICLE#" => $article));
            }elseif ($res["result"] == 0) {
              $tmpText = GetMessage('CATALOG_ELEMENT_NOT_FOUND_ARTICLE', array("#ARTICLE#" => $article));
            }

            $popUpText[] = $tmpText;
          }else{
            $popUpText[] = GetMessage("SUCCESS_FILE", array("#ARTICLE#" => $article, "#QUANTITY#" => $res["result"]));
          }
        }

        //обновление состояния корзины
        \Bitrix\Sale\BasketComponentHelper::updateFUserBasketQuantity($fuser_id);

        $arResult["popup"] = array(
          "text" => implode("<br/>", $popUpText)
        );
      }else{
        AddMessage2Log('Ошибка чтения файла. NAME - '.$_FILES["file"]["name"]);
        $arResult["mess"][] = array(
          "text" => GetMessage('FILE_ERROR'),
          "type" => "error"
        );
      }
    }
  }

  $arResult["sessid"] = bitrix_sessid();
}else{
  $arResult["mess"][] = array(
    "text" => GetMessage('SESSION_ERROR'),
    "type" => "error"
  );
}

echo json_encode($arResult);
die();
