<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header("Content-Type: application/x-javascript; charset=".LANG_CHARSET);

use Bitrix\Main\Mail\Event;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (check_bitrix_sessid()){
  Bitrix\Main\Loader::includeModule("iblock");
  global $USER;
  $request = Application::getInstance()->getContext()->getRequest();

  if ($request->getPost("action") == "article_not_found"){

    $arSelect = Array("ID", "NAME");
    $arFilter = Array("IBLOCK_ID" => IBLOCK_DEALERS, "ACTIVE"=>"Y", "PROPERTY_EMPLOYEE" => $USER->GetID());
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

    $dealer = $res->GetNext();

    $personalManager = HinoUsers::getManager();

    $res = Event::send(array(
        "EVENT_NAME" => "ARTICLE_NOT_FOUND",
        "LID" => "s1",
        "C_FIELDS" => array(
          "EMAIL_TO"    => $personalManager["EMAIL"],
          "USER_NAME"   => $USER->GetFullName(),
          "USER_EMAIL"  => $USER->GetEmail(),
          "DEALER_NAME" => $dealer["NAME"],
          "ARTICLE"     => $request->getPost("article")
        ),
    ));
    if($res){
      $arResult["mess"][] = array(
        "text" => Loc::getMessage('SUCCESS_ARTICLE_NOT_FOUND'),
        "type" => "success"
      );
    }else{
      $arResult["mess"][] = array(
        "text" => Loc::getMessage('ERROR_ARTICLE_NOT_FOUND'),
        "type" => "error"
      );
    }
  }
}else{
  $arResult["mess"][] = array(
    "text" => Loc::getMessage('SESSION_ERROR'),
    "type" => "error"
  );
}

echo json_encode($arResult);
die();
